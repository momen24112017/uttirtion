<!DOCTYPE html>
<html lang="en">

<head>
    <title>u trition</title>
    <!-- Meta tag Keywords -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="UTF-8" />
    <meta name="keywords" />

    <!--// Meta tag Keywords -->

    <!-- Custom-Files -->
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/fontawesome-free-5.13.1-web/css/all.css">
    <link rel="stylesheet" href="css/style.css" type="text/css" media="all" />
    <!-- Style-CSS -->
    <!-- //Custom-Files -->

    <!-- Web-Fonts -->
    <link
        href="//fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i&amp;subset=latin-ext"
        rel="stylesheet">
    <link
        href="//fonts.googleapis.com/css?family=Barlow+Semi+Condensed:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">
    <!-- //Web-Fonts -->
    <script src="js/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
    <!--include angular-->
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.0/angular.min.js"></script>
    <script src="./js/wizzard.js"> </script>
    <script src="./js/common.js"> </script>
</head>
<style>

.stepwizard-step p {
    margin-top: 10px;
}

.stepwizard-row {
    display: table-row;
}

.stepwizard {
    display: table;
    width: 100%;
    position: relative;
}

.stepwizard-step button[disabled] {
    opacity: 1 !important;
    filter: alpha(opacity=100) !important;
}

.stepwizard-row:before {
    top: 14px;
    bottom: 0;
    position: absolute;
    content: " ";
    width: 100%;
    height: 1px;
    background-color: #ccc;
    z-order: 0;

}

.stepwizard-step {
    display: table-cell;
    text-align: center;
    position: relative;
}

.btn-circle {
  width: 30px;
  height: 30px;
  text-align: center;
  padding: 6px 0;
  font-size: 12px;
  line-height: 1.428571429;
  border-radius: 15px;
}
</style>
<body ng-controller="myCtrl" ng-app="myApp">
    <div class="container-fluid px-0">
        <!--navbar-->
        <?php include 'navbar.php';?>
        <!--//navbar-->
     <!-- banner -->
     <div class="bg-banner">
     <div class="row ">
           <!--<img src={{plan.banner_img}} alt="healthy-food" class="main-banner-2 img-responsive">-->
           <!--  <img src="images/banner-2-test6.jpg" alt="plan img" class=" main-banner-2 img-responsive">-->
       <div class="banner-2-overlay">
           <p class="banner-2-txt orange-txt">start your journey</p>
           <a href="#meal_plans">
                    </a>
       </div>
        </div>
    </div>
        <!-- //banner -->
        <!-- page details -->
        <div class="row no-gutters">
            <div class="px-0 col-sm-12">
                <div class="breadcrumb-agile bg-light py-2">
                    <ol class="breadcrumb bg-light m-0">
                        <li class="breadcrumb-item">
                            <a href="index.php">Home</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">journey_wizzard</li>
                    </ol>
                </div>
            </div>
        </div>
     
        <!-- //page details -->

        <!--wizzard-->
        
         <div class="row my-4 justify-content-center">   
             <div class="col-sm-9">
                 <div class="stepwizard">
                     <div class="stepwizard-row justify-content-center setup-panel">
                         
                             <div class="stepwizard-step">
                                 <a href="#step-1" type="button" class="btn btn-primary btn-circle">1</a>
                                 <p>Step 1</p>
                             </div>
                             <div class="stepwizard-step">
                                 <a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a>
                                 <p>Step 2</p>
                             </div>
                             <div class="stepwizard-step">
                                 <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">3</a>
                                 <p>Step 3</p>
                             </div>
                     </div>
                 </div>
             </div>
         </div>
             
             <div class="row justify-content-center">
             <div class="col-sm-9">
             <form role="form">
                 <div class="row justify-content-center setup-content" id="step-1">
                     <div class="col-md-6">
                         <h3> Step 1</h3>
                         <div class="form-group">
                             <label class="control-label" ng-model="arrQuestion[0].question">Weight</label>
                             <input  maxlength="100" ng-model="arrQuestion[0].answer" type="number" required="required" class="form-control" placeholder="Enter First Name"  />
                         </div>
                         <div class="form-group">
                             <label class="control-label" ng-model="arrQuestion[1].question">height</label>
                             <input maxlength="100" type="number" ng-model="arrQuestion[1].answer" required="required" class="form-control" placeholder="Enter Last Name" />
                         </div>
                         <button class="btn btn-primary nextBtn btn-lg pull-right" type="button"  >Next</button>
                     </div>
                 </div>
                 <div class="row justify-content-center setup-content" id="step-2">
                     
                     <div class="col-md-6">
                         <h3> Step 2</h3>
                         <div class="form-group">
                             <label class="control-label" ng-model="arrQuestion[2].question" value="Company Name">Company Name</label>
                             <input maxlength="200" type="text" ng-model="arrQuestion[2].answer" required="required"  class="form-control" placeholder="Enter Company Name" />
                         </div>
                         <div class="form-group">
                             <label class="control-label">Company Address</label>
                             <input maxlength="200" type="text" required="required" class="form-control"  placeholder="Enter Company Address"  />
                         </div>
                         <button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Next</button>
                     </div>
                     
                 </div>
                 <div class="row justify-content-center setup-content" id="step-3">
                     
                     <div class="col-md-6">
                         <h3> Step 3</h3>
                         <button class="btn btn-success my-4 btn-lg pull-right" type="button" ng-click="submitQuestionaire()">
                         <div class="spinner-border text-light" ng-if="spnFinish"  role="status">
                             <span class="sr-only">Loading...</span>
                         </div>    
                         Finish!</button>
                     </div>
                     
                 </div>
             </form>
             </div>
             </div
      
       


       
       <!--//wizard-->
  




        <!--footer-->
        <?php include 'footer.php';?>
        <!--//footer-->       
        <!-- move top icon -->
        <a href=" index.php#home" class="move-top text-center">
            <span class="fas fa-level-up-alt" aria-hidden="true"></span>
        </a>
        <!-- //move top icon -->

       
       </div>
    <script>
    $(document).ready(function () {

var navListItems = $('div.setup-panel div a'),
        allWells = $('.setup-content'),
        allNextBtn = $('.nextBtn');

allWells.hide();

navListItems.click(function (e) {
    e.preventDefault();
    var $target = $($(this).attr('href')),
            $item = $(this);

    if (!$item.hasClass('disabled')) {
        navListItems.removeClass('btn-primary').addClass('btn-default');
        $item.addClass('btn-primary');
        allWells.hide();
        $target.show();
        $target.find('input:eq(0)').focus();
    }
});

allNextBtn.click(function(){
    var curStep = $(this).closest(".setup-content"),
        curStepBtn = curStep.attr("id"),
        nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
        curInputs = curStep.find("input[type='text'],input[type='url']"),
        isValid = true;

    $(".form-group").removeClass("has-error");
    for(var i=0; i<curInputs.length; i++){
        if (!curInputs[i].validity.valid){
            isValid = false;
            $(curInputs[i]).closest(".form-group").addClass("has-error");
        }
    }

    if (isValid)
        nextStepWizard.removeAttr('disabled').trigger('click');
});

$('div.setup-panel div a.btn-primary').trigger('click');
});
</script>
  </body>

</html>