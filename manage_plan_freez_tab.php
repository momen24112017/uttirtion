<form method="post" name="form" ng-class="{true: 'error'}[form.$invalid]">
        <div class="row justify-content-center my-4 mx-0">
          <div class="col-sm-4">
             <div class="form-group">
              <label for="startfreeze">from</label>
              <input type="date" name="from" required=""
            ng-model="objFreezePlan.from" min="{{minDate}}" max="{{maxDate}}">>
             </div>
         </div>
         <div class="col-sm-4">
           <div class="form-group">
            <label for="endfreeze">to</label>
            <input type="date" name="to" required=""
            ng-model="objFreezePlan.to" min="{{minDate}}" max="{{maxDate}}">
           </div>
         </div>
       </div>
       <div class="row justify-content-center mx-0">
       <button type="submit" class="btn btn-primary" ng-click="freezePlan()" ng-disabled="!form.$valid">

        <div ng-if="FreezeSpn == false">Freeze</div>

        <div ng-if="FreezeSpn == true" class="text-center">
            <div class="spinner-border" role="status">
                <span class="sr-only">Loading...</span>
            </div>
        </div>

      </button>
       </div>      
</form>