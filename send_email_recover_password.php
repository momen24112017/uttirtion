<!DOCTYPE html>
<html lang="en">

<head>
    <title>u trition</title>
    <!-- Meta tag Keywords -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="UTF-8" />
    <meta name="keywords" />

    <!--// Meta tag Keywords -->

    <!-- Custom-Files -->
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/fontawesome-free-5.13.1-web/css/all.css">
    <link rel="stylesheet" href="css/style.css" type="text/css" media="all" />
    <!-- Style-CSS -->
    <!-- //Custom-Files -->

    <!-- Web-Fonts -->
    <link
        href="//fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i&amp;subset=latin-ext"
        rel="stylesheet">
    <link
        href="//fonts.googleapis.com/css?family=Barlow+Semi+Condensed:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">
        <link rel="icon" type="image/png" sizes="96x96" href="images/favicon-32x32.png">
    <!-- //Web-Fonts -->
    <script src="js/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
    <!--include angular-->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/angularjs-toaster/3.0.0/toaster.min.css" rel="stylesheet" />

<link
rel="stylesheet"
href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css"
/>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.0/angular.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.4.0/angular-animate.min.js" ></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angularjs-toaster/3.0.0/toaster.min.js"></script>
    <script src="./js/successfuly_confirmedreg.js"> </script>
    <script src="./js/common.js"> </script>
</head>
  <body ng-app="successfulyConfirmedApp" ng-controller="successfulyConfirmedregCtrl">
  <toaster-container></toaster-container>
     <div class="container-fluid px-0">
      <!--nav-->
         <nav class="navbar  navbar-expand-lg  navbar-light checkout-nav-bg">
            <a class="navbar-brand" href="index.php">
            <img src="images/logo_crop.jpg" alt="utrition logo" class="nav-logo"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
           <div class="collapse navbar-collapse" id="navbarNav">
             <ul class="navbar-nav ml-auto">
               <li class="nav-item">
                 <a class="nav-link" href="index.php">home</a>
               </li>
             </ul>
           </div>
         </nav>     
          <!--//nav-->
         <!--content-->
          <!--background-->
       <div class="content-recover-bg">  
         
         <div class="row no-gutters justify-content-center">
            <div class="col-sm-6">
                <div class="form-recover-bg">
                    <div class="row text-center text-capitalize green-txt d-flex justify-content-center " >
                         <h4 class="  mt-4 ">
                        You should soon recieve E-mail to<span class="orange-txt"> reset password.</span>
                        </h4>
                    </div>
                    <div class="row text-center  green-txt d-flex justify-content-center " >
                        <h5 class="  my-3 ">Please check your Junk/Spam folder in Case the confirmation email got delivered there.</h5>
                    </div>

                    <div class="row text-center text-capitalize green-txt d-flex justify-content-center " >
                         <h6 >Didn't recieve message.</h6>   
                    </div>
                    <div class="row mt-4 d-flex justify-content-center">
                        <h6 class=" resend-link">
                        <button class="btn btn-outline-info ml-2" ng-click="resendMailforResetPassword()">
                          
                          <div ng-if="ResetPasswordSpn == false">Resend again</div>
                          
                          <div ng-if="ResetPasswordSpn" class="text-center">
                            <div class="spinner-border" role="status">
                                <span class="sr-only">Loading...</span>
                            </div>
                          </div>
                        
                        </button>
                
                        </h6>    
                    </div>
                </div>    
            </div>   
         </div> 
      </div>
          <!--//content-->  
     </div>
  </body>
</html>
