<!DOCTYPE html>
<html lang="en">

<head>
    <title>u trition</title>
    <!-- Meta tag Keywords -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="UTF-8" />
    <meta name="keywords" />

    <!--// Meta tag Keywords -->

    <!-- Custom-Files -->
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/fontawesome-free-5.13.1-web/css/all.css">
    <link rel="stylesheet" href="css/style.css" type="text/css" media="all" />
    <!-- Style-CSS -->
    <!-- //Custom-Files -->

    <!-- Web-Fonts -->
    <link
        href="//fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i&amp;subset=latin-ext"
        rel="stylesheet">
    <link
        href="//fonts.googleapis.com/css?family=Barlow+Semi+Condensed:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">
    <!-- //Web-Fonts -->
    <script src="js/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
    <script>
        addEventListener("load", function () {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }
    </script>
    <!--// Meta tag Keywords -->

    <!-- Custom-Files -->
    <link rel="stylesheet" href="css/bootstrap.css">
    <!-- Bootstrap-Core-CSS -->
    <link rel="stylesheet" href="css/fontawesome-free-5.13.1-web/css/all.css">
    <link rel="stylesheet" href="css/style.css" type="text/css" media="all" />
    <!-- Style-CSS -->
   
    <!-- Font-Awesome-Icons-CSS -->
    <!-- //Custom-Files -->
        <!-- Web-Fonts -->
    <link
        href="//fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i&amp;subset=latin-ext"
        rel="stylesheet">
    <link
        href="//fonts.googleapis.com/css?family=Barlow+Semi+Condensed:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">
        <link rel="icon" type="image/png" sizes="96x96" href="images/favicon-32x32.png">
    <!-- //Web-Fonts -->
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.0/angular.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.4.0/angular-animate.min.js" ></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angularjs-toaster/3.0.0/toaster.min.js"></script>
    <script src="./js/plan_detail.js"> </script>
    <script src="./js/common.js"> </script>
    <link
  rel="stylesheet"
  href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/angularjs-toaster/3.0.0/toaster.min.css" rel="stylesheet" />
</head>

<body ng-controller="myPlanDetailsCtrl" ng-app="myPlanDetailsApp">
<toaster-container></toaster-container>
     <!--navbar-->
     <?php include 'navbar.php';?>
          <!--//navbar-->
          <!-- banner -->
    <div class="bg-banner">
         <div class="row ">
                 <!--<img src={{plan.banner_img}} alt="healthy-food" class="main-banner-2 img-responsive">-->
                 <!--  <img src="images/banner-2-test6.jpg" alt="plan img" class=" main-banner-2 img-responsive">-->
             <div class="banner-2-overlay">
                 <p class="banner-2-txt green-txt">{{plan.name}}<span class="orange-txt secfont"> Plan</span></p>
                 <a href="#meal_plans"></a>
             </div>
         </div>
    </div>
        <!-- //banner -->
        <!-- page details -->
    <div class="row no-gutters">
         <div class="px-0 col-sm-12">
             <div class="breadcrumb-agile bg-light py-2">
                 <ol class="breadcrumb bg-light m-0">
                     <li class="breadcrumb-item">
                         <a href="index.php">Home</a>
                     </li>
                     <li class="breadcrumb-item active" aria-current="page">
                         Review_detail
                     </li>
                 </ol>
             </div>
         </div>
    </div>
     
        <!-- //page details -->
         <!--review details-->
     <div class="row  my-5 ">
        <div class="col-sm-4 offset-sm-2">
         <h5 class="green-txt  mb-3">Start to serve you from    <span class="orange-txt">{{startDate}}</span>
           </h5>
           <label for="startingdate" class="orange-txt mr-2">Or choose your  start</label>
           <input type="date"  id="startingdate " ng-model="startDate"  min="{{minDate}}">   
           <!-- <input class="date12" type="text" name="input" placeholder="YYYY-MM-DD" required pattern="(?:19|20)\[0-9\]{2}-(?:(?:0\[1-9\]|1\[0-2\])/(?:0\[1-9\]|1\[0-9\]|2\[0-9\])|(?:(?!02)(?:0\[1-9\]|1\[0-2\])/(?:30))|(?:(?:0\[13578\]|1\[02\])-31))" title="Enter a date in this format YYYY/MM/DD"/> -->
        </div>
          

      
     </div>             
     
     <div class="row my-5 justify-content-center">
         <div class="col-sm-5">
            <div class="card cart-view-pro" >
                <ul class="list-group list-group-flush">
                  <li class="list-group-item cart-view-title green-txt">my cart</li>
                  <li class="list-group-item cart-view-li-style">
                    <div class="row">
                       <div class="col-sm-auto">  
                    <img src="{{plan.feature_img}}" alt="" class="img-responsive cart-view-pro-img">
                    </div>
                    <div class="col-sm-3">
                        <p class="cart-view-pro-name">{{plan.name}}</p>
                        
                       
                        
                    </div>
                    
                        <div class="col-sm-3 text-right">
                          <span class="cart-view-pro-name text-uppercase green-txt">{{plan.price}} AED</span>
                        </div>    
                    
                </li>

                  <li class="list-group-item cart-view-li-style">
                      <!--promo code-->
                      <p>
                        <a class="promo-code orange-txt" data-toggle="collapse" href="#collapsepromo" role="button" aria-expanded="false" aria-controls="collapsepromo">
                            <i class="fas mr-2 fa-tag"></i>enter promo code
                        </a>
                        
                      </p>
                      <div class="collapse" id="collapsepromo">
                        <div class="card card-body">
                          <form class="form-inline">
                            <div class="form-group mx-sm-3 mb-2">
                              <input type="text" ng-model="promocodeValue" class="form-control" id="inputpromocode" placeholder="Enter Promo Code">
                            </div>
                            <button type="button" ng-click="checkPromocode()" class="btn promo-btn mb-2">
                            <div  class="text-center">
                              <div ng-if="applySpn" class="spinner-border " role="status">
                                  <span class="sr-only">Loading...</span>
                              </div>
                            </div>

                             
                            <span ng-if="applyBtn" >apply</span></button>
                          </form>
                          <div style="color:red;">{{message}}</div>
                        </div>
                      </div>
                      <!--//promo code-->

                    </li>

                    

                </ul>
              </div>
       </div>

         <!--order summary-->
         <div class="col-sm-3">
          <div class="card cart-view-pro" >
            <ul class="list-group list-group-flush">
              <li class="list-group-item cart-view-title green-txt">order summary</li>
              <li class="list-group-item cart-view-li-style">
                <div class="row ">
               
                  <div class="col-sm-12">
                    <div class="ds-block">
                      <p class="cart-view-pro-name float-left">total</p>
                      <p class="cart-view-pro-name float-right text-uppercase">{{plan.price}} AED</p>
                    </div>  
                     
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-12 ">
                     <div class="ds-block">     
                         <p class="det-cart-pg  float-left">promocode</p>
                         <p class="det-cart-pg  float-right">{{promocode.value}}</p>                     
    </div>
                  </div>
                </div> 
                <div class="row">
                  <div class="col-sm-6">
                    <!-- <p class="det-cart-pg">dubai-UAE</p>     -->
                  </div> 
                </div> 
             
              </li>
    
              <li class="list-group-item cart-view-li-style">
                <div class="row ">
               
                  <div class="col-sm-12 blue-txt">
                    <div class="ds-block cart-view-title">
                      <p class=" float-left">total</p>
                      <p class=" float-right text-uppercase">{{plan.price - promocode.value}} aed</p>
                    </div>  
                     
                  </div>
                </div>
                <div class="row">
                   <div class="col-sm-12">
                     
                       <button type="button" ng-click="checkout()" class="btn shopping-n-btn w-100 mt-4 rounded-pill">
                 
                        <div ng-if="spanCheckoutbtn" class="spinner-border " role="status">
                            <span class="sr-only">Loading...</span>
                        </div>
                            
                         <i class="fas mr-2  fa-shopping-bag"></i>
                         check out
                        </button>
                     
                   </div>  
                </div>
              </li>
            </ul>
          </div>
         </div>
         <!--//order summary--> 

     </div>
     
         <!--//review details-->
        <!--footer-->
        <?php include 'footer.php';?>
        <!--//footer-->
        <!-- move top icon -->
        <a href="index.php#home" class="move-top text-center">
            <span class="fa fa-level-up" aria-hidden="true"></span>
        </a>
        <!-- //move top icon -->
</div>
</body>
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.jquery.com/ui/1.11.0/jquery-ui.js"></script>
<script>
  $(document).ready(function() {
    $(function() {
  var $dp1 = $(".date12");
  $dp1.datepicker({
  
      minDate:new Date().addHours(48),
  });

});
Date.prototype.addHours= function(h){
    this.setHours(this.getHours()+h);
    return this;
}
  });
</script>

</html>