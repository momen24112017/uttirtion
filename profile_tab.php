
<div class="row justify-content-center">
  <div class="col-sm-6">
    <div class="card">
      <h3 class=" pl-3 my-4 green-txt">
        <span class="text-uppercase ">personal information
        </span>
      </h3>
      <ul class="list-group list-group-flush">
        <li class="list-group-item">
          <form>
            <div class="form-group row">
              <label for="name-n-profile" class="col-sm-2 col-form-label">Name:</label>
              <div class="col-sm-10">
                <input type="text" ng-model="User.name" disabled class="form-control-plaintext" id="name-n-profile">
              </div>
            </div>

            <div class="form-group row">
              <label for="email-n-profile" class="col-sm-2 col-form-label">Email:</label>
              <div class="col-sm-10">
                <input type="email" ng-model="User.email" disabled class="form-control-plaintext" id="email-n-profile">
              </div>
            </div>

            <div class="form-group row">
              <label for="email-n-profile" class="col-sm-2 col-form-label">Phone:</label>
              <div class="col-sm-10">
                <input type="text" ng-model="User.phone" class="form-control" id="email-n-profile">
              </div>
            </div>
            <div class="form-group row">
              <label for="email-n-profile" class="col-sm-2 col-form-label">Password:</label>
              <div class="col-sm-10">
                <input type="password" ng-model="User.password" value="" class="form-control" id="email-n-profile">
              </div>
            </div>
        </li>

      </ul>
    </div>
  </div>
</div>

<div class="row justify-content-center">
  <div class="col-sm-6">
    <div class="card">
      <h3 class=" pl-3 my-4 green-txt">
        <span class="text-uppercase ">delivery information
        </span>
      </h3>
      <ul class="list-group list-group-flush">
        <li class="list-group-item">
          <p>Address</p>
          <form>
            <div class="form-group row" ng-repeat="obj in UserAddress track by $index">
       
              <div class="col-sm-2 col-form-label green-txt">
                <input type="text" class="form-control" id="address-home-n-profile" ng-model="obj.address_name">
              </div>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="address-home-n-profile" ng-model="obj.address">
              </div>
              <div class="col-sm-2">
                <a class="promo-code orange-txt float-right" data-toggle="collapse" href="#collapseupdateaddress"
                  role="button" aria-expanded="false" aria-controls="collapseupdateaddress">
                  <i ng-click="addAddress($index)" class="fas fa-plus"></i>
                  <i ng-if="$index>0" ng-click="removeAddress($index)" class="fas fa-minus"></i>
                </a>
              </div>
            </div>
            <button type="button" ng-click="updateUserProfile()" class="btn w-100 promo-btn mb-2">
              <div ng-if="spnupdate" class="spinner-border " role="status">
                <span class="sr-only">Loading...</span>
              </div>
              <span ng-if="!spnupdate">update</span>
            </button>
       
          </form>
        </li>
      </ul>
    </div>
  </div>
</div>

<div class="row justify-content-center">
  <div class="col-sm-6">
    <div class="card">
      <h3 class=" pl-3 my-4 green-txt">
        <span class="text-uppercase ">medical information
        </span>
      </h3>
      <ul class="list-group list-group-flush">
        <li class="list-group-item">
          <form>
            <div class="form-row">
              <div class="form-group col-md-6 checkboxex-style">
                <label>Do you have any medical condition:</label><br>
                <label>medical condition:</label><br>
                <p>{{medicalMessage}}</p>
                <i ng-if="arrUM[0]['a']['Anemia']=='yes'" class="far fa-check-circle"></i> 
          
                <label ng-if="arrUM[0]['a']['Anemia']!=null">Anemia {{arrUM[0]['a']['Diabetes']}}
                <br></label>
                <i ng-if="arrUM[0]['a']['Diabetes']=='yes'" class="far  fa-check-circle"></i> 

                 <label ng-if="arrUM[0]['a']['Diabetes']!=null">Diabetes <br></label>
                
                <i ng-if="arrUM[0]['a']['BloodPressure']=='yes'" class="far fa-check-circle"></i> 
             
                <label ng-if="arrUM[0]['a']['BloodPressure']!=null">Blood Pressure (hyper or Hypo) <br></label>
                
                <i ng-if="arrUM[0]['a']['Thyroid']" class="far fa-check-circle"></i> 
    
                <label ng-if="arrUM[0]['a']['Thyroid']!=null">Thyroid (hyper or hypo)
                <br></label>
                <i ng-if="arrUM[0]['a']['heart']"  class="far fa-check-circle"></i> 
         
                <label ng-if="arrUM[0]['a']['heart']!=null">Heart problems
                <br></label>
              
                <i ng-if="arrUM[0]['a']['Gastrointestinal']" class="far fa-check-circle"></i> 
         
                <label ng-if="arrUM[0]['a']['Gastrointestinal']!=null">Gastrointestinal Problems
                <br></label>
             
                <label ng-if="arrUM[0]['a']['otherCondition']!=null">other,please specify

                </label>
                <input ng-if="arrUM[0]['a']['otherCondition']!=null" type="text" value="{{arrUM[0]['a']['otherCondition']}}" class="form-control" disabled name="otherConditionProfile">

              </div>

            </div>


            <div class="form-row">
              <div class="form-group col-md-6 checkboxex-style">
                <label>Food causes Allergy:</label><br>
                <p>{{allergyMessage}}</p>
                <i  ng-if="arrUM[1]['a']['eggs']=='yes'" class="far fa-check-circle"></i>
              
                <label ng-if="arrUM[1]['a']['eggs']!=null">eggs<br></label>
                <i ng-if="arrUM[1]['a']['Dairy']=='yes'" class="far fa-check-circle"></i> 
              
                <label ng-if="arrUM[1]['a']['Dairy']!=null">Dairy
                <br></label>
                <i ng-if="arrUM[1]['a']['Gluten']=='yes'" class="far fa-check-circle"></i>
    
                <label ng-if="arrUM[1]['a']['Gluten']!=null">Gluten
                <br></label>
                <i  ng-if="arrUM[1]['a']['nuts']=='yes'" class="far fa-check-circle"></i> 
              
                <label ng-if="arrUM[1]['a']['nuts']!=null">nuts
                <br></label>
                <i class="far fa-check-circle" ng-if="arrUM[1]['a']['fish']=='yes'"></i> 
             
                <label ng-if="arrUM[1]['a']['fish']!=null">fish
                <br></label>
                <i class="far fa-check-circle" ng-if="arrUM[1]['a']['Seashell']=='yes'" ></i> 
               
                <label ng-if="arrUM[1]['a']['Seashell']!=null">Seashell
                <br></label>
             
                <label  ng-if="arrUM[1]['a']['otherAllergy']!=null"  for="allergy7">other,please specify

                </label>

                <input type="text" ng-if="arrUM[1]['a']['otherAllergy']!=null" value="{{arrUM[1]['a']['otherAllergy']}}" class="form-control" disabled name="otherAllergyProfile">

              </div>

            </div>




          </form>
        </li>

      </ul>
    </div>
  </div>
</div>
