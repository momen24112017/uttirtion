<!DOCTYPE html>
<html lang="en">

<head>
    <title>u trition</title>
    <!-- Meta tag Keywords -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="UTF-8" />
    <meta name="keywords" />

    <!--// Meta tag Keywords -->

    <!-- Custom-Files -->
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/fontawesome-free-5.13.1-web/css/all.css">
    <link rel="stylesheet" href="css/style.css" type="text/css" media="all" />
    <!-- Style-CSS -->
    <!-- //Custom-Files -->

    <!-- Web-Fonts -->
    <link
        href="//fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i&amp;subset=latin-ext"
        rel="stylesheet">
    <link
        href="//fonts.googleapis.com/css?family=Barlow+Semi+Condensed:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">
    <!-- //Web-Fonts -->
    <script src="js/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
    <!--include angular-->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/angularjs-toaster/3.0.0/toaster.min.css" rel="stylesheet" />
    <link rel="icon" type="image/png" sizes="96x96" href="images/favicon-32x32.png">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.0/angular.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.4.0/angular-animate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angularjs-toaster/3.0.0/toaster.min.js"></script>
    <script src="./js/login.js"> </script>
    <script src="./js/common.js"> </script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/angularjs-toaster/3.0.0/toaster.min.css" rel="stylesheet" />
</head>

<body ng-controller="myLCtrl" ng-app="myLApp">
    <toaster-container></toaster-container>
    <div class="container-fluid px-0">

        <!-- navbar -->

        <!-- top-header -->

        <nav id="home" class="navbar nav-txt-style hide-md navbar-expand-lg navbar-light header-bg">
            <span class=" navbar-text "><i class="fa orange-txt fa-map-marker mr-2"></i>UAE</span>
            <span class="navbar-text ml-3"><i class="fa orange-txt fa-phone mr-2 "></i> (+971) 526920873</span>
            <div class="ml-auto" id="navbarText">
                <a ng-if="User==undefined" href="log_reg.php"
                    class="  btn login-button-2 stand-btn mr-3 text-uppercase">
                    <span class="fa fa-sign-in  mr-2"></span>Login / register
                </a>
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" ng-if="User!=undefined" href="profile.php"
                            id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false">
                            <span class="navbar-text text-capitalize mr-2">
                                <span>
                                    <i class="fas fa-user mr-1"></i>
                                </span> Hello, {{User.name}}
                            </span>
                        </a>
                        <div class="dropdown-menu w-100 text-center" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="profile.php">My Profile</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" ng-click="logout()"><span ng-if="User!=undefined">logout</span></a>


                        </div>
                    </li>
                </ul>

                <!-- <span class="navbar-text text-capitalize mr-2">
                    follow us :
                </span>
                <span class="fa-cont">
                    <span class="s"> <i class="fab fa-twitter mr-2"></i></span>
                    <i class="fab fa-facebook-f mr-2"></i>
                </span> -->
            </div>
        </nav>
        <!-- //top-header -->
        <!--navbar-->
        <nav class="navbar navbar-expand-lg navbar-light nav-bg">
            <a class="navbar-brand" href="index.php">
                <img src="images/logo_crop.jpg" alt="utrition logo" class="nav-logo"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02"
                aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
                <ul class="navbar-nav ml-auto  mt-2 mt-lg-0">
                    <li class="nav-item "><a class="nav-link" href="index.php#meal_plans">meal plans</a></li>
                    <li class="nav-item "><a class="nav-link" href="index.php#about">About Us</a></li>
                    <li class="nav-item "><a class="nav-link" ng-if="User!=undefined" href="manage_plan.php">my meal
                            plan</a></li>
                    <li class="nav-item active "><a class="nav-link" href="gallery.php">gallery</a></li>
                    <li class="nav-item "><a class="nav-link" href="faq.php">faq's</a></li>
                    <li class="nav-item "><a class="nav-link" href="contact.php">contact us</a></li>
                    <li class="nav-item d-lg-none d-xl-none"><a class="nav-link" ng-if="User==undefined"
                            href="log_reg.html">logIn/Register</a>
                    </li>
                </ul>
            </div>
        </nav>
        <!--//navbar-->
        <!--//navbar-->
        <div class="row no-gutters">
            <div class="col-sm-12 ">
                <h1 class="main-title text-center  mb-4 green-txt">

                    <span class="text-uppercase ">Explore 100's<span class="orange-txt secfont"> delicious
                        </span>Meal</span>
                </h1>
            </div>

            <div class="stand-block">

            </div>
        </div>
        <div class="row no-gutters">
            <div class="col-sm-3 px-0">
                <img src="images/Photos/18.jpg" class="img-responsive meal-img-gal" alt="meal-img">
            </div>
            <div class="col-sm-3 px-0">
                <img src="images/Photos/17.jpg" class="img-responsive meal-img-gal" alt="meal-img">
            </div>
            <div class="col-sm-3 px-0">
                <img src="images/Photos/16.jpg" class="img-responsive meal-img-gal" alt="meal-img">
            </div>
            <div class="col-sm-3 px-0">
                <img src="images/Photos/14.jpg" class="img-responsive meal-img-gal" alt="meal-img">
            </div>
            <div class="col-sm-3 px-0">
                <img src="images/Photos/15.jpg" class="img-responsive meal-img-gal" alt="meal-img">
            </div>
            <div class="col-sm-3 px-0">
                <img src="images/Photos/13.jpg" class="img-responsive meal-img-gal" alt="meal-img">
            </div>
            <div class="col-sm-3 px-0">
                <img src="images/Photos/12.jpg" class="img-responsive meal-img-gal" alt="meal-img">
            </div>
            <div class="col-sm-3 px-0">
                <img src="images/Photos/11.jpg" class="img-responsive meal-img-gal" alt="meal-img">
            </div>
            <div class="col-sm-3 px-0">
                <img src="images/Photos/10.jpg" class="img-responsive meal-img-gal" alt="meal-img">
            </div>
            <div class="col-sm-3 px-0">
                <img src="images/Photos/9.jpg" class="img-responsive meal-img-gal" alt="meal-img">
            </div>
            <div class="col-sm-3 px-0">
                <img src="images/Photos/8.jpg" class="img-responsive meal-img-gal" alt="meal-img">
            </div>
            <div class="col-sm-3 px-0">
                <img src="images/Photos/7.jpg" class="img-responsive meal-img-gal" alt="meal-img">
            </div>
            <div class="col-sm-3 px-0">
                <img src="images/Photos/6.jpg" class="img-responsive meal-img-gal" alt="meal-img">
            </div>
            <div class="col-sm-3 px-0">
                <img src="images/Photos/5.jpg" class="img-responsive meal-img-gal" alt="meal-img">
            </div>
            <div class="col-sm-3 px-0">
                <img src="images/Photos/4.jpg" class="img-responsive meal-img-gal" alt="meal-img">
            </div>
            <div class="col-sm-3 px-0">
                <img src="images/Photos/3.jpg" class="img-responsive meal-img-gal" alt="meal-img">
            </div>
            <div class="col-sm-3 px-0">
                <img src="images/Photos/2.jpg" class="img-responsive meal-img-gal" alt="meal-img">
            </div>
            <div class="col-sm-3 px-0">
                <img src="images/Photos/19.jpg" class="img-responsive meal-img-gal" alt="meal-img">
            </div>
            <div class="col-sm-3 px-0">
                <img src="images/Photos/20.jpg" class="img-responsive meal-img-gal" alt="meal-img">
            </div>
            <div class="col-sm-3 px-0">
                <img src="images/Photos/21.jpg" class="img-responsive meal-img-gal" alt="meal-img">
            </div>
          
            <div class="col-sm-3 px-0">
                <img src="images/Photos/23.jpg" class="img-responsive meal-img-gal" alt="meal-img">
            </div>
            <div class="col-sm-3 px-0">
                <img src="images/Photos/24.jpg" class="img-responsive meal-img-gal" alt="meal-img">
            </div>
            <div class="col-sm-3 px-0">
                <img src="images/Photos/25.jpg" class="img-responsive meal-img-gal" alt="meal-img">
            </div>
            <div class="col-sm-3 px-0">
                <img src="images/Photos/26.jpg" class="img-responsive meal-img-gal" alt="meal-img">
            </div>
            <div class="col-sm-3 px-0">
                <img src="images/Photos/27.jpg" class="img-responsive meal-img-gal" alt="meal-img">
            </div>
            <div class="col-sm-3 px-0">
                <img src="images/Photos/28.jpg" class="img-responsive meal-img-gal" alt="meal-img">
            </div>
            <div class="col-sm-3 px-0">
                <img src="images/Photos/29.jpg" class="img-responsive meal-img-gal" alt="meal-img">
            </div>
            <div class="col-sm-3 px-0">
                <img src="images/Photos/30.jpg" class="img-responsive meal-img-gal" alt="meal-img">
            </div>
            <div class="col-sm-3 px-0">
                <img src="images/Photos/31.jpg" class="img-responsive meal-img-gal" alt="meal-img">
            </div>
            <div class="col-sm-3 px-0">
                <img src="images/Photos/32.jpg" class="img-responsive meal-img-gal" alt="meal-img">
            </div>
            <div class="col-sm-3 px-0">
                <img src="images/Photos/33.jpg" class="img-responsive meal-img-gal" alt="meal-img">
            </div>
            <div class="col-sm-3 px-0">
                <img src="images/Photos/34.jpg" class="img-responsive meal-img-gal" alt="meal-img">
            </div>
            <div class="col-sm-3 px-0">
                <img src="images/Photos/35.jpg" class="img-responsive meal-img-gal" alt="meal-img">
            </div>
            <div class="col-sm-3 px-0">
                <img src="images/Photos/36.jpg" class="img-responsive meal-img-gal" alt="meal-img">
            </div>
            <div class="col-sm-3 px-0">
                <img src="images/Photos/37.jpg" class="img-responsive meal-img-gal" alt="meal-img">
            </div>
            <div class="col-sm-3 px-0">
                <img src="images/Photos/38.jpg" class="img-responsive meal-img-gal" alt="meal-img">
            </div>
            <div class="col-sm-3 px-0">
                <img src="images/Photos/39.jpg" class="img-responsive meal-img-gal" alt="meal-img">
            </div>
            <div class="col-sm-3 px-0">
                <img src="images/Photos/40.jpg" class="img-responsive meal-img-gal" alt="meal-img">
            </div>
            <div class="col-sm-3 px-0">
                <img src="images/Photos/41.jpg" class="img-responsive meal-img-gal" alt="meal-img">
            </div>
            <div class="col-sm-3 px-0">
                <img src="images/Photos/42.jpg" class="img-responsive meal-img-gal" alt="meal-img">
            </div>
            <div class="col-sm-3 px-0">
                <img src="images/Photos/43.jpg" class="img-responsive meal-img-gal" alt="meal-img">
            </div>
            <div class="col-sm-3 px-0">
                <img src="images/Photos/44.jpg" class="img-responsive meal-img-gal" alt="meal-img">
            </div>
            <div class="col-sm-3 px-0">
                <img src="images/Photos/45.jpg" class="img-responsive meal-img-gal" alt="meal-img">
            </div>
            <div class="col-sm-3 px-0">
                <img src="images/Photos/46.jpg" class="img-responsive meal-img-gal" alt="meal-img">
            </div>
            <div class="col-sm-3 px-0">
                <img src="images/Photos/47.jpg" class="img-responsive meal-img-gal" alt="meal-img">
            </div>
            <div class="col-sm-3 px-0">
                <img src="images/Photos/48.jpg" class="img-responsive meal-img-gal" alt="meal-img">
            </div>
            <div class="col-sm-3 px-0">
                <img src="images/Photos/49.jpg" class="img-responsive meal-img-gal" alt="meal-img">
            </div>
            <div class="col-sm-3 px-0">
                <img src="images/Photos/50.jpg" class="img-responsive meal-img-gal" alt="meal-img">
            </div>
            <div class="col-sm-3 px-0">
                <img src="images/Photos/51.jpg" class="img-responsive meal-img-gal" alt="meal-img">
            </div>
            <div class="col-sm-3 px-0">
                <img src="images/Photos/52.jpg" class="img-responsive meal-img-gal" alt="meal-img">
            </div>
            <div class="col-sm-3 px-0">
                <img src="images/Photos/53.jpg" class="img-responsive meal-img-gal" alt="meal-img">
            </div>
            <div class="col-sm-3 px-0">
                <img src="images/Photos/54.jpg" class="img-responsive meal-img-gal" alt="meal-img">
            </div>
            <div class="col-sm-3 px-0">
                <img src="images/Photos/55.jpg" class="img-responsive meal-img-gal" alt="meal-img">
            </div>
            <div class="col-sm-3 px-0">
                <img src="images/Photos/56.jpg" class="img-responsive meal-img-gal" alt="meal-img">
            </div>
            <div class="col-sm-3 px-0">
                <img src="images/Photos/57.jpg" class="img-responsive meal-img-gal" alt="meal-img">
            </div>
            <div class="col-sm-3 px-0">
                <img src="images/Photos/58.jpg" class="img-responsive meal-img-gal" alt="meal-img">
            </div>
            <div class="col-sm-3 px-0">
                <img src="images/Photos/59.jpg" class="img-responsive meal-img-gal" alt="meal-img">
            </div>
            <div class="col-sm-3 px-0">
                <img src="images/Photos/60.jpg" class="img-responsive meal-img-gal" alt="meal-img">
            </div>
            <div class="col-sm-3 px-0">
                <img src="images/Photos/61.jpg" class="img-responsive meal-img-gal" alt="meal-img">
            </div>
            <div class="col-sm-3 px-0">
                <img src="images/Photos/62.jpg" class="img-responsive meal-img-gal" alt="meal-img">
            </div>
            <div class="col-sm-3 px-0">
                <img src="images/Photos/63.jpg" class="img-responsive meal-img-gal" alt="meal-img">
            </div>
            <div class="col-sm-3 px-0">
                <img src="images/Photos/64.jpg" class="img-responsive meal-img-gal" alt="meal-img">
            </div>
            <div class="col-sm-3 px-0">
                <img src="images/Photos/65.jpg" class="img-responsive meal-img-gal" alt="meal-img">
            </div>
            <div class="col-sm-3 px-0">
                <img src="images/Photos/66.jpg" class="img-responsive meal-img-gal" alt="meal-img">
            </div>
            <div class="col-sm-3 px-0">
                <img src="images/Photos/67.jpg" class="img-responsive meal-img-gal" alt="meal-img">
            </div>
            <div class="col-sm-3 px-0">
                <img src="images/Photos/68.jpg" class="img-responsive meal-img-gal" alt="meal-img">
            </div>
            <div class="col-sm-3 px-0">
                <img src="images/Photos/69.jpg" class="img-responsive meal-img-gal" alt="meal-img">
            </div>
            <div class="col-sm-3 px-0">
                <img src="images/Photos/70.jpg" class="img-responsive meal-img-gal" alt="meal-img">
            </div>
            <div class="col-sm-3 px-0">
                <img src="images/Photos/71.jpg" class="img-responsive meal-img-gal" alt="meal-img">
            </div>
            <div class="col-sm-3 px-0">
                <img src="images/Photos/72.jpg" class="img-responsive meal-img-gal" alt="meal-img">
            </div>
        </div>


        <!-- footer -->
        <?php include 'footer.php';?>
        <!-- //footer -->

    </div>
</body>

</html>