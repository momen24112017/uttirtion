<!--save btn-->
<form>
  <div class="row no-gutters justify-content-center">
    <div class="col-sm-9 text-right">
      <button type="button" ng-if="arrSubmitPlan.length!=0" class="btn text-capitalize   btn-accordion-save"
        ng-click="submit()">
        <div ng-if="saveMenu" class="spinner-border " role="status">
          <span class="sr-only">Loading...</span>
        </div>
        <span ng-if="!saveMenu">save</span>

      </button>
    </div>
  </div>
  <!--// save btn-->
  <div class="row py-5 no-gutters justify-content-center " ng-show="showLoadingSpinForSbMenu">

    <!--spinner-->
    <div class="spin-wrap green-txt">
      <div class="spinner-border " role="status">
      </div>
      <span class="pl-3">
        <h3>Loading...</h3>
      </span>
    </div>
  </div>
  <div class="row mt-2 no-gutters justify-content-center">
    <div class="col-sm-9">

      <nav>
        <div class="nav nav-tabs" id="nav-tabweeks" role="tablist">
          <a class="nav-item nav-link week-item " ng-repeat="(key,objWeek) in arrSubmitPlan track by $index"
            ng-class="{active: $index == 0}" id="nav-weekone-tab" data-toggle="tab" href="#{{key}}" role="tab"
            aria-controls="nav-weekone" aria-selected="true">{{key}}</a>

        </div>
      </nav>
    </div>
  </div>

  <div class="row mt-5 no-gutters justify-content-center">

    <div class="col-sm-9">
      <div class="tab-content" id="nav-tabweeksContent">

        <!--week 1-->

        <div class="row  no-gutters justify-content-center" ng-if="arrSubmitPlan.length==0"><b>Hello there is no plan to
            submit</b></div>
        <div class="tab-pane fade show" ng-repeat="(key,arrWeek) in arrSubmitPlan track by $index"
          ng-class="{active: $index == 0}" id="{{key}}" role="tabpanel" aria-labelledby="nav-weekone-tab">

          <div ng-repeat="week in arrWeek track by $index">
            <?php include 'week.php';?>
          </div>
        </div>

        <!--//week 1-->

      </div>
    </div>
  </div>