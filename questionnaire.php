<!DOCTYPE html>
<html lang="en">

<head>
    <title>u trition</title>
    <!-- Meta tag Keywords -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="UTF-8" />
    <meta name="keywords" />

    <!--// Meta tag Keywords -->

    <!-- Custom-Files -->
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/fontawesome-free-5.13.1-web/css/all.css">
    <link rel="stylesheet" href="css/style.css" type="text/css" media="all" />
    <link rel="icon" type="image/png" sizes="96x96" href="images/favicon-32x32.png">
    <!-- Style-CSS -->
    <!-- //Custom-Files -->

    <!-- Web-Fonts -->
    <link
        href="//fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i&amp;subset=latin-ext"
        rel="stylesheet">
    <link
        href="//fonts.googleapis.com/css?family=Barlow+Semi+Condensed:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">
    <!-- //Web-Fonts -->
    <script src="js/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
    <!--include angular-->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/angularjs-toaster/3.0.0/toaster.min.css" rel="stylesheet" />

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.0/angular.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.4.0/angular-animate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angularjs-toaster/3.0.0/toaster.min.js"></script>
    <script src="./js/login.js"> </script>
    <script src="./js/common.js"> </script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/angularjs-toaster/3.0.0/toaster.min.css" rel="stylesheet" />
</head>

<body ng-controller="myLCtrl" ng-app="myLApp">
    <toaster-container></toaster-container>
    <div class="container-fluid px-0">

        <!-- navbar -->
        <nav class="navbar  navbar-expand-lg  navbar-light checkout-nav-bg">
            <a class="navbar-brand" href="index.php">
                <img src="images/logo_crop.jpg" alt="utrition logo" class="nav-logo"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="index.php">home</a>
                    </li>
                </ul>
            </div>
        </nav>
        <!--//navbar-->
        <!--questionnaire-->
        <div class="questionnaire-bg">
            <div class="stand-block"></div>
            <div class="row no-gutters justify-content-center">
                <div class="col-sm-6">
                    <form class="form-recover-bg" method="post" name="form" ng-class="{true: 'error'}[form.$invalid]">
                        <!-- <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="firstnameQuestionnaire">first name</label>
                            <span style="color:red"  ng-show="form.first_name.$error.required">*</span>
                            <input type="text" name="first_name" ng-model="objQuestionnaire.first_name" class="form-control" id="firstnameQuestionnaire" required="">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="lastnameQuestionnaire">last name</label>
                            <span style="color:red"  ng-show="form.last_name.$error.required">*</span>
                            <input type="text" class="form-control" name="last_name" ng-model="objQuestionnaire.last_name" id="lastnameQuestionnaire" required="">
                        </div>
                    </div> -->
                       
                        <div class="stand-block"></div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="datebirthQuestionnaire">date of birth</label>
                                <span style="color:red" ng-show="form.datebirth.$error.required">*</span>
                                <input type="date" class="form-control" name="datebirth"
                                    ng-model="objQuestionnaire.datebirth" id="datebirthQuestionnaire" required="">
                            </div>

                            <div class="form-group col-md-6">
                                <label for="genderQuestionnaire">gender</label>
                                <span style="color:red" ng-show="form.gender.$error.required">*</span>
                                <select id="genderQuestionnaire" name="gender" ng-model="objQuestionnaire.gender"
                                    class="form-control" required="">
                                    <option value="false" selected disabled>Choose...</option>
                                    <option value="male">male</option>
                                    <option value="female">female</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="weightQuestionnaire">weight (Kg):</label>
                                <span style="color:red" ng-show="form.weight.$error.required">*</span>
                                <input type="number" class="form-control" name="weight"
                                    ng-model="objQuestionnaire.weight" id="weightQuestionnaire" required="">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="heightQuestionnaire">height (Cm):</label>
                                <span style="color:red" ng-show="form.height.$error.required">*</span>
                                <input type="number" class="form-control" name="height"
                                    ng-model="objQuestionnaire.height" id="heightQuestionnaire" required="">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="percenatgebodyfatQuestionnaire">Percenatge body Fat :</label>
                                <span style="color:red" ng-show="form.percenatgebodyfat.$error.required">*</span>
                                <input type="number" class="form-control" name="percenatgebodyfat"
                                    ng-model="objQuestionnaire.percenatgebodyfat" id="percenatgebodyfatQuestionnaire"
                                    required="">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="targetweightQuestionnaire">target weight (Kg):</label>
                                <span style="color:red" ng-show="form.targetweight.$error.required">*</span>
                                <input type="number" class="form-control" name="targetweight"
                                    ng-model="objQuestionnaire.targetweight" id="targetweightQuestionnaire" required="">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="mobileQuestionnaire">mobile</label>
                                <span style="color:red" ng-show="form.mobile.$error.required">*</span>
                                <input type="tel" class="form-control" name="mobile"
                                    ng-model="objQuestionnaire.mobile" id="mobileQuestionnaire" required="">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="emailQuestionnaire">e-mail</label>
                                <span style="color:red" ng-show="form.email.$error.required">*</span>
                                <input type="email" class="form-control" name="email" ng-model="objQuestionnaire.email"
                                    id="emailQuestionnaire" required="">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="clubQuestionnaire">club</label>
                                <span style="color:red" ng-show="form.club.$error.required">*</span>
                                <input type="text" class="form-control" name="club" ng-model="objQuestionnaire.club"
                                    id="clubQuestionnaire" required="">
                            </div>

                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6 checkboxex-style">
                                <label>Do you have any medical condition:</label><br>
                                <input class="form-check-input" type="checkbox" id="medical1"
                                    ng-model="objQuestionnaire.medicalCondition.Diabetes" name="Diabetes"
                                    ng-true-value="yes" ng-false-value="no">
                                <label for="medical1">Diabetes </label><br>
                                <input class="form-check-input" type="checkbox" id="medical2"
                                    ng-model="objQuestionnaire.medicalCondition.BloodPressure" name="BloodPressure"
                                    ng-true-value="yes" ng-false-value="no">
                                <label for="medical2">Blood Pressure (hyper or Hypo)
                                </label><br>
                                <input class="form-check-input" type="checkbox" id="medical3"
                                    ng-model="objQuestionnaire.medicalCondition.Thyroid" name="Thyroid"
                                    ng-true-value="yes" ng-false-value="no">
                                <label for="medical3">Thyroid (hyper or hypo)
                                </label><br>
                                <input class="form-check-input" type="checkbox" id="medical4"
                                    ng-model="objQuestionnaire.medicalCondition.heart" name="heart"
                                    ng-true-value="yes" ng-false-value="no">
                                <label for="medical4">Heart problems
                                </label><br>
                                <input class="form-check-input" type="checkbox" id="medical5"
                                    ng-model="objQuestionnaire.medicalCondition.Anemia" name="Anemia"
                                    ng-true-value="yes" ng-false-value="no">
                                <label for="medical5">Anemia
                                </label><br>
                                <input class="form-check-input" type="checkbox" id="medical6"
                                    ng-model="objQuestionnaire.medicalCondition.Gastrointestinal" name="Gastrointestinal"
                                    ng-true-value="yes" ng-false-value="no">
                                <label for="medical6">Gastrointestinal Problems
                                </label><br>
                                <!-- <input class="form-check-input" type="checkbox" id="medical7"
                                    ng-model="objQuestionnaire.medicalCondition.othermedical" name="medical7"
                                    ng-true-value="yes" ng-false-value="no"> -->
                                    <input class="form-check-input" type="checkbox" id="medical7" name="none">
                                <label for="medical7">None
                                    
                                </label><br>
                                    <label for="medical7">other,please specify

                                </label>
                                <input type="text" class="form-control"
                                    ng-model="objQuestionnaire.medicalCondition.otherCondition" name="otherCondition">

                            </div>

                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-6 checkboxex-style">
                                <label>Do you have any food Allergy:</label><br>
                                <input class="form-check-input" type="checkbox" id="allergy1" name="eggs"
                                    ng-model="objQuestionnaire.foodAllergy.eggs" ng-true-value="yes"
                                    ng-false-value="no">
                                <label for="allergy1">eggs</label><br>
                                <input class="form-check-input" type="checkbox" id="allergy2" name="Dairy"
                                    ng-model="objQuestionnaire.foodAllergy.Dairy" ng-true-value="yes"
                                    ng-false-value="no">
                                <label for="allergy2">Dairy
                                </label><br>
                                <input class="form-check-input" type="checkbox" id="allergy3" name="Gluten"
                                    ng-model="objQuestionnaire.foodAllergy.Gluten" ng-true-value="yes"
                                    ng-false-value="no">
                                <label for="allergy3">Gluten
                                </label><br>
                                <input class="form-check-input" type="checkbox" id="allergy4" name="nuts"
                                    ng-model="objQuestionnaire.foodAllergy.nuts" ng-true-value="yes"
                                    ng-false-value="no">
                                <label for="allergy4">nuts
                                </label><br>
                                <input class="form-check-input" type="checkbox" id="allergy5" name="fish"
                                    ng-model="objQuestionnaire.foodAllergy.fish" ng-true-value="yes"
                                    ng-false-value="no">
                                <label for="allergy5">fish
                                </label><br>
                                <input class="form-check-input" type="checkbox" id="allergy6" name="Seashell"
                                    ng-model="objQuestionnaire.foodAllergy.Seashell" ng-true-value="yes"
                                    ng-false-value="no">
                                <label for="allergy6">Seashell
                                    
                                </label><br>
                                <input class="form-check-input" type="checkbox" id="allergy7" name="none">
                                <label for="allergy7">None
                                    
                                </label><br>
                                <!-- <input class="form-check-input" type="checkbox" id="allergy7" name="allergy7" ng-model="objQuestionnaire.foodAllergy.allergy7" ng-true-value="otherallergy" ng-false-value=""> -->
                                <label for="allergy7">other,please specify

                                </label>
                                <input type="text" class="form-control"
                                    ng-model="objQuestionnaire.foodAllergy.otherAllergy" name="otherAllergy">

                            </div>

                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="dislikesQuestionnaire" class="col-form-label">dislikes:
                                </label>
                                <textarea class="form-control" name="message" 
                                    rows="5" id="dislikesQuestionnaire"></textarea>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="doctoradviseQuestionnaire" class="col-form-label">Did the doctor advice you
                                    to follow any kind of diet?
                                    Please specify what diet and why:
                                </label>
                                <textarea class="form-control" name="message" ng-model="objQuestionnaire.message"
                                    rows="5" id="doctoradviseQuestionnaire"></textarea>
                            </div>
                        </div>
                       
                        <div class="form-group">
                            <div class="form-check">
                                <input class="form-check-input" name="agreeTermsofService" ng-model="objQuestionnaire.agreeTermsofService"
                                    type="checkbox" id="gridCheck" required="">
                                <label class="form-check-label text-capitalize" for="gridCheck">
                                    i have read and agree to the terms of service.
                                </label>
                                <span style="color:red" ng-show="form.targetweight.$error.required">*</span>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary" ng-click="userQuestionnaire()"
                            ng-disabled="!form.$valid">

                            <div ng-if="questionnaireSpn == false">send</div>

                            <div ng-if="questionnaireSpn == true" class="text-center">
                                <div class="spinner-border" role="status">
                                    <span class="sr-only">Loading...</span>
                                </div>
                            </div>

                        </button>
                    </form>
                </div>
            </div>
            <!--//questionnaire-->



            <!-- footer -->
            <?php include 'footer.php';?>
            <!-- //footer -->
            <!-- move top icon  -->
            <a href="#home" class="move-top text-center">
                <span class="fas fa-level-up-alt" aria-hidden="true"></span>
            </a>

            <!-- //move top icon -->

        </div>
    </div>
</body>

</html>