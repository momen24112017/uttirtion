


<!-- top-header -->

   <nav id="home" class="navbar nav-txt-style hide-md navbar-expand-lg navbar-light header-bg">
            <span class=" navbar-text "><i class="fa orange-txt fa-map-marker mr-2"></i>UAE</span>
            <span class="navbar-text ml-3"><i class="fa orange-txt fa-phone mr-2 "></i> (+971) 526920873</span>
            <div class="ml-auto" id="navbarText">
                <a ng-if="User==undefined" href="log_reg.php" class="  btn login-button-2 stand-btn mr-3 text-uppercase">
                    <span class="fa fa-sign-in  mr-2"></span>Login / register
                </a>
                <ul class="navbar-nav mr-auto">   
                <li class="nav-item dropdown">
                <a  class="nav-link dropdown-toggle" ng-if="User!=undefined" href="profile.php" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >  
                     <span  class="navbar-text text-capitalize mr-2">
                         <span>
                                 <i class="fas fa-user mr-1"></i>
                         </span>     Hello, {{User.name}}
                     </span>
                </a>
                <div class="dropdown-menu w-100 text-center" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="profile.php">My Profile</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item"ng-click="logout()"><span  ng-if="User!=undefined">logout</span></a>
      
         
        </div>
                </li>
                </ul>
               
                <!-- <span class="navbar-text text-capitalize mr-2">
                    follow us :
                </span>
                <span class="fa-cont">
                    <span class="s"> <i class="fab fa-twitter mr-2"></i></span>
                    <i class="fab fa-facebook-f mr-2"></i>
                </span> -->
            </div>
        </nav>
        <!-- //top-header -->

        <!--navbar-->
        <nav class="navbar navbar-expand-lg navbar-light nav-bg">
            <a class="navbar-brand" href="index.php">
                <img src="images/logo_crop.jpg" alt="utrition logo" class="nav-logo"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02"
                aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
                <ul class="navbar-nav ml-auto  mt-2 mt-lg-0">
                    <li class="nav-item active"><a class="nav-link" href="index.php#meal_plans">meal plans</a></li>
                    <li class="nav-item "><a class="nav-link" href="index.php#about">About Us</a></li>
                    <li class="nav-item "><a class="nav-link" ng-if="User!=undefined" href="manage_plan.php">my meal plan</a></li>
                    <li class="nav-item "><a class="nav-link" href="gallery.php">gallery</a></li>
                    <li class="nav-item "><a class="nav-link" href="faq.php">faq's</a></li>
                    <li class="nav-item "><a class="nav-link" href="contact.php">contact us</a></li>
                    <li class="nav-item d-lg-none d-xl-none"><a class="nav-link" ng-if="User==undefined" href="log_reg.php">logIn/Register</a>
                    </li>
                </ul>
            </div>
        </nav>
        <!--//navbar-->

