 <!--footer-->
 <div class="footer-bg">
    <div class="row mt-5 py-5 no-gutters justify-content-center">
       <div class="col-sm-2 d-flex justify-content-center">
       <ul class="navbar-nav footer-txt   intro-sm-txt">
         <li class="nav-link" >quick links</li>
         <li><a class="nav-link" href="index.php#meal_plans">meal plans</a></li>
         <li class="nav-item "><a class="nav-link" href="index.php#about">About Us</a></li>
         <li class="nav-item "><a class="nav-link" href="manage_plan.php">My meal plan</a></li>
         <li class="nav-item "><a class="nav-link" href="faq.html">faq's</a></li>
         <li class="nav-item d-lg-none d-xl-none"><a class="nav-link" href="log_reg.php">logIn/Register</a>
       </ul>
    </div>

    <div class="col-sm-3 d-flex justify-content-center">
        <ul class="navbar-nav footer-txt intro-sm-txt">
            <li  class="nav-link" >contact  us</li>
            <li class="nav-item sc-footer "> <a class="nav-link" > <i class="fa  fa-phone mr-2 ">
                 
                  </i> (+971) 526920873 </a></li>
            <li class="nav-item "><a class="nav-link" href="https://www.instagram.com/utritionlife.ae/?hl=en">
                <i class="fab fa-instagram mr-2"></i>Instagram  </a></li>
   
                <!-- <li class="nav-item "><a class="nav-link" href="manage_plan.php">
                    <i class="fab fa-facebook-f mr-2"></i>facebook </a></li> -->

            </ul>
    </div>

    <div class="col-sm-2  d-flex justify-content-center">
        <ul class="navbar-nav footer-txt intro-sm-txt ">
            <li class="nav-link" >quick start </li>
            <li><a class="nav-link" href="index.php#meal_plans">meal plans</a></li>
            <li><a ng-if="User==undefined" href="log_reg.php" class="nav-link  btn login-button-2 px-1 stand-btn ">
                <span  class="fa fa-sign-in  mr-2"></span>Login / register</a>
            </li>
            <li><a  href="contact.php" class="nav-link mt-2 btn login-button-2 px-1 stand-btn ">
                <span  class="fa fa-sign-in  mr-2"></span>request call back</a>
            </li>
        </ul>
    </div>

</div>
    </div>
        <!--//footer-->
       <!-- copyright -->
        <div class="cpy-right text-center py-3">
            <p class="white-txt footer-txt ">© 2020 U trition. All rights reserved.
            </p>
        </div>
        <!-- //copyright -->