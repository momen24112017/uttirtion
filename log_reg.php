<!DOCTYPE html>
<html lang="en">

<head>
    <title>u trition</title>
    <!-- Meta tag Keywords -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="UTF-8" />
    <meta name="keywords" />

    <!--// Meta tag Keywords -->

    <!-- Custom-Files -->
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/fontawesome-free-5.13.1-web/css/all.css">
    <link rel="stylesheet" href="css/style.css" type="text/css" media="all" />
    <link rel="icon" type="image/png" sizes="96x96" href="images/favicon-32x32.png">
    <!-- Style-CSS -->
    <!-- //Custom-Files -->

    <!-- Web-Fonts -->
    <link
        href="//fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i&amp;subset=latin-ext"
        rel="stylesheet">
    <link
        href="//fonts.googleapis.com/css?family=Barlow+Semi+Condensed:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">
    <!-- //Web-Fonts -->
    <script src="js/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
    <!--include angular-->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/angularjs-toaster/3.0.0/toaster.min.css" rel="stylesheet" />

<link
rel="stylesheet"
href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css"
/>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.0/angular.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.4.0/angular-animate.min.js" ></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angularjs-toaster/3.0.0/toaster.min.js"></script>
    <script src="./js/login.js"> </script>
    <script src="./js/common.js"> </script>
    <link
  rel="stylesheet"
  href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/angularjs-toaster/3.0.0/toaster.min.css" rel="stylesheet" />
</head>

<body ng-controller="myLCtrl" ng-app="myLApp">
<toaster-container></toaster-container>
    <div class="container-fluid px-0">
    
       <!-- navbar -->
       <nav class="navbar  navbar-expand-lg  navbar-light checkout-nav-bg">
            <a class="navbar-brand" href="index.php">
            <img src="images/logo_crop.jpg" alt="utrition logo" class="nav-logo"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
           <div class="collapse navbar-collapse" id="navbarNav">
             <ul class="navbar-nav ml-auto">
               <li class="nav-item">
                 <a class="nav-link" href="index.php">home</a>
               </li>
             </ul>
           </div>
         </nav>     
    <!--//navbar-->
     
        <!--log in tabs-->
        <div class="row  mt-5 no-gutters justify-content-center">
            <div class="col-sm-6">

                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item log-item" role="presentation">
                        <a class="nav-link active" id="log-tab" data-toggle="tab" href="#log" role="tab"
                            aria-controls="home" aria-selected="true">log in</a>
                    </li>
                    <li class="nav-item log-item" role="presentation">
                        <a class="nav-link" id="reg-tab" data-toggle="tab" href="#reg" role="tab"
                            aria-controls="profile" aria-selected="false">register</a>
                    </li>

                </ul>
            </div>
        </div>

<div class="row no-gutters  justify-content-center">
            <div class="col-sm-6">
                <div class="tab-content log-content" id="myTabContent">
                    <!--log tab-->
                    <div class="tab-pane fade show active" id="log" role="tabpanel" aria-labelledby="log-tab">
                        <div class="login-body mt-5">
                            <form action="#" method="post" name="form">
                                <div class="form-group" ng-class="{true: 'error'}[submitted && form.email.$invalid]">
                                    <label>Your Email
                                    <span style="color:red" ng-show="submitted && form.email.$error.required">*</span>
                                    </label>
                                    <input type="text" ng-model="objLogin.email" class="form-control" name="email" placeholder="" required="">
                                </div>
                                <div class="form-group">
                                    <label class="mb-2">Password
                                    <span style="color:red" ng-show="submitted && form.password.$error.required">*</span>
                                    </label>
                                    <input type="password" class="form-control" ng-model="objLogin.password" name="password" placeholder=""
                                        required="">
                                </div>
                                <button type="button" data-ng-click="login()"  class="btn white-txt w-100 btn-on-car submit mt-3 mb-4">
                                <div ng-if="logspn" class="text-center">
                                    <div class="spinner-border" role="status">
                                        <span class="sr-only">Loading...</span>
                                    </div>
                                </div>
                                <span ng-if="logbtn">Login</span></button>
                                <p class="forgot-w3ls text-center mb-3">
                                    <a href="recover_account.php">Forgot your password?</a>
                                </p>
                            </form>
                        </div>
                    </div>
                    <!--//log tab-->
                    <!--reg tab-->
                    <div class="tab-pane fade" id="reg" role="tabpanel" aria-labelledby="reg-tab">
                        <div class="login-body mt-5">
                            <form action="#" method="post" name="form" ng-class="{true: 'error'}[form.email.$invalid]">

                                <div class="form-group ">
                                    <label for="inputtitle">Title
                                        <span style="color:red" ng-show="form.title.$error.required">*</span>
                                    </label>
                                    <select id="inputtitle" ng-model="objRegister.title" name="title" required="" class="form-control">
                                        <option  value="mr" >Mr.</option>
                                        <option  value="mrs">Mrs.</option>
                                        <option  value="miss">Miss.</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="name">Your Name
                                    <span style="color:red" ng-show="form.name.$error.required">*</span>
                                    </label>
                                    <input type="text" class="form-control" ng-model="objRegister.name" id="name" name="name" placeholder="Name"
                                        required="">
                                </div>
                                <div class="form-group">
                                    <label for="email">Email
                                    <span style="color:red" ng-show="form.email.$error.required">*</span>
                                    </label>
                                    <input type="email" ng-model="objRegister.email" class="form-control" id="email" name="email" placeholder="Email"
                                        required="">
                                </div>
                                <p class="text-danger text-capitalize" ng-if="errorCode ==4016">{{errorMessage}}</p>
                                <div class="form-group">
                                    <label for="password1" class="mb-2">Password
                                    <span style="color:red" ng-show="form.password.$error.required">*</span>
                                    </label>
                                    <input type="password" ng-model="objRegister.password" class="form-control" name="password" id="password1"
                                        placeholder="Password" required="">
                                </div>
                                
                                <div class="form-group">
                                    <label for="date">Age
                                    <span style="color:red" ng-show="form.age.$error.required">*</span>
                                    </label>
                                    <input type="number" ng-model="objRegister.age" class="form-control" name="age" id="date" placeholder="Age"
                                        required="">
                                </div>
                                <div class="form-group">
                                    <label for="date">Phone
                                    <span style="color:red" ng-show="form.phone.$error.required">*</span>
                                    </label>
                                    <input type="text" ng-model="objRegister.phone" class="form-control" name="phone" id="date" placeholder="Phone"
                                        required="">
                                </div>
                                <div class="form-group">
                                    <label for="date">Address
                                    <span style="color:red" ng-show="form.address.$error.required">*</span>
                                    </label>
                                    <input type="text" ng-model="objRegister.address" class="form-control" name="address" id="date" placeholder="Address"
                                        required="">
                                </div>
                                <p class="text-center">
                                    By clicking Register, I agree to your terms
                                </p>

                                <button type="button" ng-click="register()" class="btn white-txt w-100 btn-on-car submit mt-3 mb-4">
                                    <div ng-if="regspn" class="text-center">
                                        <div class="spinner-border" role="status">
                                            <span class="sr-only">Loading...</span>
                                        </div>
                                    </div>

                                <span ng-if="regbtn">Register</span></button>
                               
                            </form>
                        </div>
                    </div>
                    <!--//reg tab-->
                </div>

            </div>
        </div>
        <!--//log in tabs-->


        <!-- footer -->
        <?php include 'footer.php';?>
        <!-- //footer -->
        <!-- move top icon -->
        <a href="#home" class="move-top text-center">
            <span class="fas fa-level-up-alt" aria-hidden="true"></span>
        </a>
        <!-- //move top icon -->

    </div>
</body>

</html>