app.service('SrvPlan',['$http',function($http){
    var service = {
        ListPlan:function() {
            return $http.get('https://uttrition-test.azurewebsites.net/public/api/listPlan').catch(function(response){

            });

        }
    }

    return service;
}])

app.service('SrvResendMail',['$http',function($http){
    var service = {
        ResendMail:function(id) {
            return $http.get('https://uttrition-test.azurewebsites.net/public/api/resendMail/'+id).catch(function(response){
            });

        }
    }

    return service;
}])

app.service('SrvResendMailforResetPassword',['$http',function($http){
    var service = {
        ResendMailforResetPassword:function(email) {
            return $http.post('https://uttrition-test.azurewebsites.net/public/api/resendMailforResetPassword/'+email).catch(function(response){
            });

        }
    }

    return service;
}])

app.service('SrvPromoCode',['$http',function($http){
    var service = {
        CheckPromocode:function(promocode) {
            return $http.get('https://uttrition-test.azurewebsites.net/public/api/checkPromocode/'+promocode).catch(function(response){

            });
                          
        }
        
    }
    return service;

}])

app.service('SrvCheckout',['$http',function($http){
    var service = {
        Checkout:function(obj) {
            return $http.post('https://uttrition-test.azurewebsites.net/public/api/buyPlan',obj).success(function(response){
                
            });
                          
        }
        
    }
    return service;

}])


app.service('SrvUser',['$http',function($http){
    var service = {
        Register:function(obj) {
            return $http.post('https://uttrition-test.azurewebsites.net/public/api/register',obj).catch(function(response){

            });           
        },
        Login:function(obj){
            return $http.post('https://uttrition-test.azurewebsites.net/public/api/login',obj).catch(function(response){

            });
        },
        ListUserData:function(user_id){
            return $http.get('https://uttrition-test.azurewebsites.net/public/api/getUserData/'+user_id).catch(function(response){

            });
        },
        SubmitMenu:function(arr,user_id){
            return $http.post('https://uttrition-test.azurewebsites.net/public/api/storeUserPlan/'+user_id,{data:arr}).catch(function(response){

            });
        },
        UpdateUserProfile:function(obj,user_id){
            return $http.post('https://uttrition-test.azurewebsites.net/public/api/editUserData/'+user_id,obj).catch(function(response){

            });
        },
        UpdateIsFirstTime:function(user_id){
            return $http.get('https://uttrition-test.azurewebsites.net/public/api/updateFirstTime/'+user_id).catch(function(response){

            });
        },
        ResetPassword:function(obj, email) {
            return $http.post('https://uttrition-test.azurewebsites.net/public/api/resetPassword/'+email,obj).catch(function(response){

            });                 
        },
        ContactUs:function(obj) {
            return $http.post('https://uttrition-test.azurewebsites.net/public/api/contactUs',obj).catch(function(response){

            });                 
        },
        UserQuestionnaire:function(obj, id) {
            return $http.post('https://uttrition-test.azurewebsites.net/public/api/userQuestionnaire/'+id,obj).catch(function(response){

            });                 
        },
        FreezePlan:function(obj) {
            return $http.post('https://uttrition-test.azurewebsites.net/public/api/freezePlan',obj).catch(function(response){

            });                 
        },
        ShowUserProfile:function() {
            return $http.get('https://uttrition-test.azurewebsites.net/public/api/showUserProfile').catch(function(response){

            });                 
        },

        // ViewProfile:function() {
        //     return $http.get('http://localhost/uttiration_backend/public/api/admin/normalUserProfile/'+user_id).catch(function(response){

        //     });                 
        // },
    
    }

    return service;
}])


app.service('SrvCheckEmail',['$http',function($http){
    var service = {
        CheckEmail:function( email) {
            return $http.get('https://uttrition-test.azurewebsites.net/public/api/checkEmail/'+email).catch(function(response){
            }); 
        }
    }
    return service;
}])