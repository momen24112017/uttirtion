var app = angular.module('myApp',['toaster', 'ngAnimate']);
app.controller('myCtrl',function($scope,$filter,SrvPlan,SrvUser,toaster) {
    $scope.arrPlan = [];
    $scope.User = '';
    $scope.UserProfile = '';
    $scope.UserAddress = [];
    $scope.FreezePlan = [];
    $scope.PlanAss = [];
    $scope.arrUM = [];
    $scope.arrSubmitPlan = [];
    $scope.showLoadingSpinForSbMenu =false;
    $scope.arrSubmitMenu = [];
    $scope.collect = [];
    $scope.objSubmitMenu = {
        "day": "",
        "date": "",
      
        "arrMeal": []
    }
    $scope.arrMenuUser = [];
    $scope.showLoadingSpinForPlan =false;
    $scope.planDetails = "";
    $scope.spnupdate = false;
    $scope.saveMenu = false;

    $scope.objFreezePlan = {from:"",to:"",user_id:""};
    $scope.FreezeSpn = false;
    
    $scope.maxDate = '';
    $scope.minDate = '';
    $scope.currentDate = $filter('date')(new Date(),'yyyy-MM-dd');

    $scope.init = function (){
        $scope.User = JSON.parse(localStorage.getItem('user'));
        $scope.listPlan();
        $scope.listUserData();
        var date  = new Date().addHours(48);
        $scope.minDate = date.yyyymmdd();
        var date  = new Date().addDays(47);
        $scope.maxDate = date.yyyymmdd();
    }

    Date.prototype.addHours= function(h){
        this.setHours(this.getHours()+h);
        return this;
    }
        
    Date.prototype.addDays= function(d){
        this.setDate(this.getDate()+d);
        return this;
    }

    Date.prototype.yyyymmdd = function() {         

        var yyyy = this.getFullYear().toString();                                    
        var mm = (this.getMonth()+1).toString(); // getMonth() is zero-based         
        var dd  = this.getDate().toString();             

        return  yyyy+ '-' + (mm[1]?mm:"0"+mm[0]) + '-' +(dd[1]?dd:"0"+dd[0]) ;
    }

    Date.prototype.ddmmyyyy = function() {         

        var yyyy = this.getFullYear().toString();                                    
        var mm = (this.getMonth()+1).toString(); // getMonth() is zero-based         
        var dd  = this.getDate().toString();             

        return  (dd[1]?dd:"0"+dd[0])+ '-' + (mm[1]?mm:"0"+mm[0]) + '-' + yyyy;
    }

    $scope.pushArray = function(day,date,key,key1,meal,address_id){
        $scope.x = angular.copy($scope.objSubmitMenu);
        var push = true;
        var arrKey = 0;
    
        var addMeal = true;
        angular.forEach($scope.arrSubmitMenu,function(value,key){
            if(value.date == date){
                if(key1=="breakfast"){
            value.arrMeal.push({breakfast:meal});
            }else if(key1=="lunch"){
                value.arrMeal.push({lunch:meal});
            }else if(key1=="snack"){
                value.arrMeal.push({snack:meal});
            }else if(key1=="dinner"){
                value.arrMeal.push({dinner:meal});
            }
                // return;
                push = false;
                arrKey = key;
                addMeal = false;
            }else{
                
            }
        })
   
        $scope.x.date = date;
        $scope.x.day = day;
        $scope.x.address_id = address_id;
        if(addMeal){

            if(key1=="breakfast"){
                $scope.x.arrMeal.push({breakfast:meal});
            }else if(key1=="lunch"){
                $scope.x.arrMeal.push({lunch:meal});
            }else if(key1=="snack"){
                $scope.x.arrMeal.push({snack:meal});
            }else if(key1=="dinner"){
                $scope.x.arrMeal.push({dinner:meal});
            }
        }

        // if($scope.arrSubmitMenu[key] == undefined){
        //     $scope.arrSubmitMenu[key] = [];
        // }
        
        if(push ==true){
            $scope.arrSubmitMenu.push($scope.x);
            // $scope.collect.push($scope.arrSubmitMenu);
        }
        
        console.log($scope.arrSubmitMenu);
    }

    $scope.pushArrayAddress = function(index,address_id){
        // console.log(key);
        $scope.y = angular.copy($scope.objSubmitMenu);
        $scope.arrSubmitMenu[index].address_id = 0;
        $scope.arrSubmitMenu[index].address_id = address_id;
        console.log(index);
    }

    $scope.submit = function(){
        $scope.saveMenu = true;
        console.log($scope.arrSubmitMenu)
        SrvUser.SubmitMenu($scope.arrSubmitMenu,$scope.User.id).then(function(response){
            if(response.data.status.code == 200){
                $scope.saveMenu = false;
                $scope.listUserData();
                toaster.pop({
                    type: 'Success',
                    title: 'Menu Submit',
                    body: 'Menu Submit successfully!',
                    timeout: 3000
                });
                window.location.href="manage_plan.php"
                // alert("your menu submit successfully");             
            }else{
                $scope.saveMenu = false;
                toaster.pop({
                    type: 'error',
                    title: 'Menu Submit',
                    body: 'failed toMenu Submit try again!',
                    timeout: 3000
                });
            }
        })
    }
    
    $scope.logout = function(){
        localStorage.removeItem('user');
        localStorage.removeItem('user_id');
        localStorage.removeItem('email');
        window.location.href='index.php';
    }

    $scope.updateUserProfile = function(){
        $scope.spnupdate = true;
        $scope.User.arrAddress = [];
        $scope.User.arrAddress = $scope.UserAddress;
        SrvUser.UpdateUserProfile($scope.User,$scope.User.id).then(function(response){
            if(response.data.status.code == 200){
                $scope.spnupdate = false;
                toaster.pop({
                    type: 'Success',
                    title: 'Profile',
                    body: 'profile update successfully!',
                    timeout: 3000
                });
            }else{
                $scope.spnupdate = false;
                toaster.pop({
                    type: 'error',
                    title: 'Profile',
                    body: 'failed to update profile try again!',
                    timeout: 3000
                });
            }
        })
    }

    $scope.listUserData = function(){
        $scope.showLoadingSpinForSbMenu = true;
        SrvUser.ListUserData($scope.User.id).then(function(response){
            $scope.showLoadingSpinForSbMenu = false;
            $scope.UserProfile = response.data.user;
            $scope.UserAddress = response.data.user_address;
            $scope.FreezePlan = response.data.freeze_plan;
            if($scope.currentDate < $scope.FreezePlan.from){
                $scope.FreezePlan = null;
            }
            if($scope.UserAddress.length==0){
                $scope.addAddress();
            }
            $scope.PlanAss = response.data.plan_ass;
            $scope.arrUM = response.data.user_q;
            if($scope.arrUM[0]['a']['Anemia'] == null && $scope.arrUM[0]['a']['Diabetes']==null && $scope.arrUM[0]['a']['BloodPressure']==null
                && $scope.arrUM[0]['a']['Thyroid'] == null && $scope.arrUM[0]['a']['heart'] == null && $scope.arrUM[0]['a']['Gastrointestinal'] == null
                && $scope.arrUM[0]['a']['otherCondition'] == null){
                $scope.medicalMessage = "User doesn't have any medical condition";
            }
            if($scope.arrUM[0]['a']['eggs'] == null && $scope.arrUM[0]['a']['Dairy']==null && $scope.arrUM[0]['a']['Gluten']==null
                && $scope.arrUM[0]['a']['nuts'] == null && $scope.arrUM[0]['a']['fish'] == null && $scope.arrUM[0]['a']['Seashell'] == null
                && $scope.arrUM[0]['a']['otherAllergy'] == null){
                $scope.allergyMessage = "User doesn't have any food causes allergy";
            }
            angular.forEach($scope.PlanAss, function(value, key){
                if(angular.isUndefined(value.menu_submit)){
                    $scope.arrMenuUser = value.menuUser;
                }else{
                    $scope.arrSubmitPlan = value.menu_submit;
                }
            })
        })
    }

    $scope.renewPlan = function(){
        localStorage.setItem('planDetail',$scope.planDetails);
        window.location.href="review_details.php";
    }

    $scope.addAddress = function(){
        $scope.UserAddress.push({'address_name':'','address':''});
    }

    $scope.removeAddress = function(index){
        $scope.UserAddress.splice(index,1)
    }

    $scope.listPlan = function() {
        $scope.showLoadingSpinForPlan = true;
        SrvPlan.ListPlan().then(function(response){
            $scope.showLoadingSpinForPlan = false;
            $scope.arrPlan = response.data.data;
        })
    }

    $scope.getPlanDetail=function(plan){
        localStorage.setItem('planDetail',JSON.stringify(plan))
        window.location.href='plan_detail.php';
    }

    $scope.freezePlan = function(){
        $scope.FreezeSpn = true;
        $scope.objFreezePlan.user_id = localStorage.getItem('user_id');
        if($scope.objFreezePlan.from >= $scope.objFreezePlan.to){
            console.log("Error");
            $scope.FreezeSpn = false;
            toaster.pop({
                type: 'error',
                title: 'Set Dates Correctly',
                // body:  response.data.status.message,
                timeout: 3000
            });
            return;
        }
        SrvUser.FreezePlan($scope.objFreezePlan).then(function(response){
            if(response.data.status.code == 200){
                console.log("Success");
                $scope.FreezeSpn = false;
                toaster.pop({
                    type: 'Success',
                    title: 'Plan Freeze',
                    // body:  response.data.status.message,
                    timeout: 3000
                });
                window.location.href="manage_plan.php"
            }
        })
    }

    $scope.init();
})