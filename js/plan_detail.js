var app = angular.module('myPlanDetailsApp', ['toaster', 'ngAnimate']);
app.controller('myPlanDetailsCtrl',function($scope,SrvPromoCode,SrvCheckout,toaster){
    $scope.plan = "";
    $scope.promocode=0;
    $scope.DiscountNumber = 0;
    $scope.promocodeValue = "";
    $scope.applyBtn = true;
    $scope.applySpn = false;
    $scope.spanCheckoutbtn = false;
    // $scope.HourNow = Date.getHours();
    $scope.startDate = '';
    $scope.minDate = '';
    $scope.promocode.id = 0;
    
    $scope.init = function (){
        $scope.plan=JSON.parse(localStorage.getItem('planDetail'))
        $scope.User = JSON.parse(localStorage.getItem('user'));

        var date  = new Date().addHours(48);
        $scope.minDate = date.yyyymmdd();
        $scope.startDate = date.ddmmyyyy();
    }

    Date.prototype.addHours= function(h){
        this.setHours(this.getHours()+h);
        return this;
    }
        
    Date.prototype.addDays= function(d){
        this.setDate(this.getDate()+d);
        return this;
    }

    Date.prototype.yyyymmdd = function() {         

        var yyyy = this.getFullYear().toString();                                    
        var mm = (this.getMonth()+1).toString(); // getMonth() is zero-based         
        var dd  = this.getDate().toString();             

        return  yyyy+ '-' + (mm[1]?mm:"0"+mm[0]) + '-' +(dd[1]?dd:"0"+dd[0]) ;
    }

    Date.prototype.ddmmyyyy = function() {         

        var yyyy = this.getFullYear().toString();                                    
        var mm = (this.getMonth()+1).toString(); // getMonth() is zero-based         
        var dd  = this.getDate().toString();             

        return  (dd[1]?dd:"0"+dd[0])+ '-' + (mm[1]?mm:"0"+mm[0]) + '-' + yyyy;
    }

    // $scope.addDays = function(date, days) {
    //     var result = new Date(date);
    //     result.setDate(result.getDate() + days);
    //     return result;
    //   }

    $scope.checkPromocode = function(){
        $scope.applyBtn = false;
        $scope.applySpn = true;
        SrvPromoCode.CheckPromocode($scope.promocodeValue).then(function(response){
            if($scope.promocodeValue == ""){
                $scope.applyBtn = true;
                $scope.applySpn = false;
                $scope.message = "Enter Promo code";
            }
            if(response.data.status.code == 200){
                $scope.applyBtn = true;
                $scope.applySpn = false;
                toaster.pop({
                    type: 'Success',
                    title: 'Success',
                    body: "Promo code is Valid",
                    timeout: 3000
                });
                $scope.message = "";
                $scope.promocode = response.data.promocode;
            }else{
                $scope.applyBtn = true;
                $scope.applySpn = false;
                $scope.message = "Invalid Promo code";
                $scope.promocode=0;
            }
        })
    }

    $scope.checkout = function(){
        $scope.spanCheckoutbtn = true;
        var to  = new Date();
        var toDate = to.addDays(30);
        var total = $scope.plan.price;
        if($scope.promocode.value){
            var total = $scope.plan.price - $scope.promocode.value;
        }
        $scope.objcheckout = {"user_id":$scope.User.id,"plan_id":$scope.plan.id,
        "promocode_id":$scope.promocode.id,"from":$scope.startDate,"price":total,to:toDate};
        SrvCheckout.Checkout($scope.objcheckout).then(function(response){
            if(response.data.status.code == 5016){
                $scope.spanCheckoutbtn = false;
                toaster.pop({
                    type: 'error',
                    title: 'Check Out',
                    body: response.data.status.message,
                    timeout: 3000
                });
                return;
            }
            $scope.spanCheckoutbtn = false;
            if(response.data.status.code == 200){
                window.location.href='success_purchase.php';
            }else{
                
            }
        })
    }

    $scope.logout = function(){
        localStorage.removeItem('user');
        localStorage.removeItem('user_id');
        localStorage.removeItem('email');
        window.location.href='index.php';
    }
    
    $scope.init();

})