var app = angular.module('myLApp', ['toaster', 'ngAnimate']);
app.controller('myLCtrl',function($scope,SrvUser,SrvCheckEmail,toaster) {
    
    $scope.User = '';
    $scope.objRegister = {name:"",email:"",password:"",age:"",phone:"",title:"mr",address:""};
    $scope.objLogin = {email:"",password:""};
    $scope.objPassword = {password:"",password_c:""};
    $scope.objContactUs = {first_name:"",last_name:"",email:"",phone:"",message:"",weight:"",goal:"",target_weight:""};
    $scope.objQuestionnaire = {first_name:"",last_name:"",datebirth:"",gender:"",weight:"",height:"",percenatgebodyfat:"",targetweight:"",email:"",club:"",agreeTermsofService:"",
        medicalCondition:{Diabetes:"",BloodPressure:"",Thyroid:"",heart:"",Anemia:"",Gastrointestinal:"",otherCondition:""},
        foodAllergy:{eggs:"",Dairy:"",Gluten:"",nuts:"",fish:"",Seashell:"",otherAllergy:""},
        message:""};
    $scope.email = "";
    $scope.logbtn=true;
    $scope.regbtn=true;
    $scope.logspn=false;
    $scope.regspn=false;
    $scope.contactUsSpn=false;
    $scope.questionnaireSpn=false;
    $scope.resetSpn = false;
    $scope.resetbtn=true;
    $scope.errorMessage ='';
    $scope.errorCode = 0;
    $scope.init = function (){
        $scope.User = JSON.parse(localStorage.getItem('user'));
    }


    $scope.register = function (){  
        $scope.regbtn=false;
        $scope.regspn = true;
        SrvUser.Register($scope.objRegister).then(function(response){
            $scope.regbtn=true;
            $scope.regspn = false;
            if(response.data.status.code == 5018){
                toaster.pop({
                    type: 'error',
                    title: 'Register',
                    body:  response.data.status.message,
                    timeout: 3000
                });
            }
            if(response.data.status.code == 200){
                toaster.pop({
                    type: 'Success',
                    title: 'Register',
                    body: 'You have registered successfully!',
                    timeout: 3000
                });
                localStorage.setItem('user_id',response.data.user_id);
                window.location.href="successfuly_confirmedreg.php"
            }
            if(response.data.status.code == 4016){
                $scope.regbtn=true;
                $scope.regspn = false; 
                $scope.errorCode = 4016;
                $scope.errorMessage = response.data.status.message;
                toaster.pop({
                    type: 'error',
                    title: 'Register',
                    body: response.data.status.message,
                    timeout: 3000
                });
            }
        })
    }
    
    $scope.login= function(){
        $scope.logbtn=false;
        $scope.logspn=true;

        SrvUser.Login($scope.objLogin).then(function(response){
            $scope.result = false;
            $scope.logbtn=true;
            $scope.logspn=false;
            if(response.data.status.code==200){
                $scope.objLogin = {email:"",password:""};
                toaster.pop({
                    type: 'Success',
                    title: 'Log in',
                    body: 'You have logged successfully !.',
                    timeout: 3000
                });
                localStorage.setItem('user',JSON.stringify(response.data.data));
                localStorage.setItem('user_id',JSON.stringify(response.data.data.id));
                localStorage.setItem('email',JSON.stringify(response.data.data.email));
                if(response.data.data.first_time == 0){
                    window.location.href="successful_login.php";
                }else{
                    window.location.href="index.php";
                }
                $scope.objLogin = {email:"",password:""};  
            } 
            else{
                $scope.objLogin = {email:"",password:""};
                toaster.pop({
                    type: 'error',
                    title: 'Log in',
                    body: 'Email or password are incorrect',
                    timeout: 3000
                });
                $scope.objLogin = {email:"",password:""};
            }
        })        
    }

    $scope.checkEmail= function(){
        $scope.resetSpn =true;
        SrvCheckEmail.CheckEmail($scope.email).then(function(response){     
            if(response.data.status.code==5017){
                $scope.resetSpn = false;
                toaster.pop({
                    type: 'error',
                    title: 'Reset Password',
                    body: 'Email not found',
                    timeout: 3000
                });
            }
            else{
                localStorage.setItem('email',$scope.email);
                $scope.resetSpn = false;
                window.location.href="send_email_recover_password.php";
            }
        })
    }

    $scope.ResetPassword= function(){
        // $scope.resetbtn=false;
        $scope.resetSpn = true;
        if($scope.objPassword.password == "" ||$scope.objPassword.password_c ==""){
            $scope.errorMessage = "Please fill the inputs";
            $scope.resetSpn = false;
            return;
        }
        if($scope.objPassword.password != $scope.objPassword.password_c){
            $scope.errorMessage = "password not match";
            $scope.resetSpn = false;
            return;
        }
        // $scope.resetSpn = true;
        // $scope.resetbtn=false;
        SrvUser.ResetPassword($scope.objPassword, localStorage.getItem('email')).then(function(response){
            if (response.data.status.code == 5013){
                $scope.errorMessage = response.data.status.message;
                // $scope.resetbtn=true;
                $scope.resetSpn = false; 
            }
            if(response.data.status.code == 5018){
                $scope.errorMessage = response.data.status.message;
                // $scope.resetbtn=true;
                $scope.resetSpn = false; 
            }
            if(response.data.status.code == 200){
                window.location.href="log_reg.php";
            }
        })
    }

    $scope.contactUs= function(){
        $scope.contactUsSpn = true;
        SrvUser.ContactUs($scope.objContactUs).then(function(response){
            if(response.data.status.code==200){
                $scope.contactUsSpn = false;
                toaster.pop({
                    type: 'Success',
                    title: 'Success',
                    body: 'Message sent successfully',
                    timeout: 3000
                });
            }
        })
    }

    $scope.userQuestionnaire= function(){
        $scope.questionnaireSpn = true;

        SrvUser.UserQuestionnaire($scope.objQuestionnaire,localStorage.getItem('user_id')).then(function(response){
            if(response.data.status.code==200){
                $scope.questionnaireSpn = false;
                toaster.pop({
                    type: 'Success',
                    title: 'Success',
                    body: 'Message sent successfully',
                    timeout: 3000
                });
            }
            window.location.href="index.php";
        })
    }

    $scope.init();
}) 
