var app = angular.module('myApp', ['toaster', 'ngAnimate']);
app.controller('myCtrl',function($scope,SrvPlan,toaster) {
    $scope.arrPlan = [];
    $scope.User = '';

    $scope.init = function (){
        $scope.User = JSON.parse(localStorage.getItem('user'));
        $scope.listPlan();
    }

    $scope.listPlan = function() {
        SrvPlan.ListPlan().then(function(response){
            $scope.arrPlan = response.data.data;
        })
    }

    $scope.getPlanDetail=function(plan){
        localStorage.setItem('planDetail',JSON.stringify(plan))
        window.location.href='plan_detail.php';
    }

    $scope.logout = function(){
        localStorage.removeItem('user');
        localStorage.removeItem('user_id');
        localStorage.removeItem('email');
        window.location.href='index.php';
    }

    $scope.init();
})