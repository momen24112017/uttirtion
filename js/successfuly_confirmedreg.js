var app = angular.module('successfulyConfirmedApp',['toaster', 'ngAnimate']);

app.controller('successfulyConfirmedregCtrl',function($scope,SrvResendMail,SrvResendMailforResetPassword,toaster) {
    
    $scope.resendMailSpn=false;
    $scope.ResetPasswordSpn=false;
    
    $scope.init = function (){

    }

    $scope.resendMail = function(){
        $scope.resendMailSpn=true;
        SrvResendMail.ResendMail(localStorage.getItem('user_id')).then(function(response){   
            if(response.data.status.code == 200){
                toaster.pop({
                    type: 'Success',
                    title: 'Email Send',
                    body: 'Email Send successfully !.',
                    timeout: 3000
                }); 
                $scope.resendMailSpn=false;
            }     
        })
    }

    $scope.resendMailforResetPassword = function(){
        $scope.ResetPasswordSpn=true;
        SrvResendMailforResetPassword.ResendMailforResetPassword(localStorage.getItem('email')).then(function(response){        
            if(response.data.status.code == 200){
                toaster.pop({
                    type: 'Success',
                    title: 'Email Send',
                    body: 'Email Send successfully !.',
                    timeout: 3000
                }); 
                $scope.ResetPasswordSpn=false;
            } 
        })
    }


    $scope.init();

})