var app = angular.module('myApp',['toaster', 'ngAnimate']);
app.controller('myCtrl',function($scope,SrvUser,$location,toaster) {

    $scope.User = '';
    $scope.UserAddress = [];
    $scope.FreezePlan = [];
    $scope.PlanAss = [];
    $scope.arrUM = [];
    $scope.arrSubmitPlan = [];
    $scope.showLoadingSpinForSbMenu =false;
    $scope.arrSubmitMenu = [];
    $scope.collect = [];
    $scope.objSubmitMenu = {
        "day": "",
        "date": "",
        "arrMeal": []
    }
    $scope.arrMenuUser = [];
    
    
    $scope.arrUser = [];
    $scope.viewSpn=false;
    $scope.userId = 0;

    $scope.init = function (){
        $scope.userId = $location.absUrl().split('id=')[1];
        $scope.listUserData($scope.userId);
        $scope.showUserProfile();
    }

    $scope.pushArray = function(day,date,key,key1,meal,address_id){
        $scope.x = angular.copy($scope.objSubmitMenu);
        var push = true;
        var arrKey = 0;
    
        var addMeal = true;
        angular.forEach($scope.arrSubmitMenu,function(value,key){
            if(value.date == date){
                if(key1=="breakfast"){
            value.arrMeal.push({breakfast:meal});
            }else if(key1=="lunch"){
                value.arrMeal.push({lunch:meal});
            }else if(key1=="snack"){
                value.arrMeal.push({snack:meal});
            }else if(key1=="dinner"){
                value.arrMeal.push({dinner:meal});
            }
                // return;
                push = false;
                arrKey = key;
                addMeal = false;
            }else{
                
            }
        })
   
        $scope.x.date = date;
        $scope.x.day = day;
        $scope.x.address_id = address_id;
        if(addMeal){

            if(key1=="breakfast"){
                $scope.x.arrMeal.push({breakfast:meal});
            }else if(key1=="lunch"){
                $scope.x.arrMeal.push({lunch:meal});
            }else if(key1=="snack"){
                $scope.x.arrMeal.push({snack:meal});
            }else if(key1=="dinner"){
                $scope.x.arrMeal.push({dinner:meal});
            }
        }

        // if($scope.arrSubmitMenu[key] == undefined){
        //     $scope.arrSubmitMenu[key] = [];
        // }
        
        if(push ==true){
            $scope.arrSubmitMenu.push($scope.x);
            // $scope.collect.push($scope.arrSubmitMenu);
        }
        
        console.log($scope.arrSubmitMenu);
    }

    $scope.pushArrayAddress = function(index,address_id){
        $scope.y = angular.copy($scope.objSubmitMenu);
        $scope.arrSubmitMenu[index].address_id = 0;
        $scope.arrSubmitMenu[index].address_id = address_id;
        console.log(index);
    }
    
    $scope.logout = function(){
        localStorage.removeItem('user');
        localStorage.removeItem('user_id');
        localStorage.removeItem('email');
        window.location.href='index.php';
    }

    $scope.listUserData = function(user_id){
        $scope.showLoadingSpinForSbMenu = true;
        SrvUser.ListUserData(user_id).then(function(response){
            $scope.showLoadingSpinForSbMenu = false;
            $scope.User = response.data.user;
            $scope.UserAddress = response.data.user_address;
            $scope.FreezePlan = response.data.freeze_plan;
            if($scope.UserAddress.length==0){
                $scope.addAddress();
            }
            $scope.PlanAss = response.data.plan_ass;
            $scope.arrUM = response.data.user_q;
            if($scope.arrUM[0]['a']['Anemia'] == null && $scope.arrUM[0]['a']['Diabetes']==null && $scope.arrUM[0]['a']['BloodPressure']==null
                && $scope.arrUM[0]['a']['Thyroid'] == null && $scope.arrUM[0]['a']['heart'] == null && $scope.arrUM[0]['a']['Gastrointestinal'] == null
                && $scope.arrUM[0]['a']['otherCondition'] == null){
                $scope.medicalMessage = "User doesn't have any medical condition";
            }
            if($scope.arrUM[0]['a']['eggs'] == null && $scope.arrUM[0]['a']['Dairy']==null && $scope.arrUM[0]['a']['Gluten']==null
                && $scope.arrUM[0]['a']['nuts'] == null && $scope.arrUM[0]['a']['fish'] == null && $scope.arrUM[0]['a']['Seashell'] == null
                && $scope.arrUM[0]['a']['otherAllergy'] == null){
                $scope.allergyMessage = "User doesn't have any food causes allergy";
            }
            angular.forEach($scope.PlanAss, function(value, key){
                if(angular.isUndefined(value.menu_submit)){
                    $scope.arrMenuUser = value.menuUser;
                }else{
                    $scope.arrSubmitPlan = value.menu_submit;
                }
            })
        })
    }

    $scope.showUserProfile= function(){
        SrvUser.ShowUserProfile().then(function(response){
            if(response.data.status.code == 200){
                $scope.arrUser = response.data.data;
            }
        })
    }

    $scope.viewUserProfile= function(){
        $scope.viewSpn = true;
        if($scope.userId == 0){
            $scope.errorMessage ='Hello there is no profile selected to view';
            $scope.viewSpn = false;
            return;
        }
        $scope.viewSpn = false;
        $scope.errorMessage ='';
        localStorage.setItem('profile_id',$scope.userId);
        window.location.href="admin-preview_user_profile.php";
    }

    // $scope.viewProfile= function(){
    //     SrvUser.ViewProfile().then(function(response){
    //         if(response.data.status.code == 200){
    //             $scope.userId = response.data.data;
    //             localStorage.setItem('profile_id',$scope.userId);
    //             window.location.href="admin-preview_user_profile.php";
    //         }
    //     })
    // }

    $scope.init();
})