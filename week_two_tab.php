

      
      <!--accordion days -->
       
      <div id="accordion-w2">
        <div class="card card-accordion">
          <div class="card-header d-flex" id="w2-one">
            <h5 class="mr-auto mb-0">
              <button class="btn  day-tab text-uppercase  btn-link" data-toggle="collapse" data-target="#collapse-w2-one" aria-expanded="true" aria-controls="collapseOne">
                saturday
              </button>
            </h5>
            <h5 class="ml-auto">
              <!--edit modal-->
<!-- Button trigger modal -->
<button type="button" class="btn text-capitalize  stand-btn btn-accordion-edit" data-toggle="modal" data-target="#exampleModal">
edit <i class="fas fa-edit"></i></button>
</button>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
<button type="button" class="close" data-dismiss="modal" aria-label="Close">
<span aria-hidden="true">&times;</span>
</button>
</div>
<div class="modal-body">
<div class="form-group">

<label for="timeedit_w2">drop off</label>
                <input type="text" class="form-control mb-2" id="timeedit_w2" disabled placeholder="Example input placeholder">

<label for="addressedit_w2">address</label>
<input type="text" class="form-control mb-2" id="addressedit_w2" disabled placeholder="Example input placeholder">

</div>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
<button type="button" class="btn btn-on-car">Save changes</button>
</div>
</div>
</div>
</div>

              <!--//edit modal-->
            
          </div>
      
          <div id="collapse-w2-one" class="collapse show" aria-labelledby="w2-one" data-parent="#accordion-w2">
            <div class="card-body">
       <!--w2-d1-->
       <div id="accordion-w2-d1">
        <div class="card text-uppercase">
          <div class="card-header d-flex" id="heading-w2-d1-breakfast">
          <h6 class="mr-auto green-txt">
           breakfast
          </h6>  
            <h5 class="ml-auto">
                <button class="btn py-0 btn-link collapsed" data-toggle="collapse" data-target="#collapse-w2-d1-breakfast" aria-expanded="false" aria-controls="collapseTwo">
                    more details
                    <i class="far fa-plus-square"></i>
              </button>
            </h5>
          </div>
          <div id="collapse-w2-d1-breakfast" class="collapse" aria-labelledby="heading-w2-d1-breakfast" data-parent="#accordion-w2-d1">
            <div class="card-body">
          
                <div class="row">
                  <!--<div class="col-sm-10">
                    <div class="form-group">
                     <textarea id="w3review" name="w3review" disabled class=" txtarea-edit">
                     </textarea>
                  </div>
                 </div>
                -->        
            <div class="col-sm-11">
               <p> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid.
            </p>

              </div>
                <div class="col-sm-1">
                <button type="button" class="btn  btn-accordion-edit">edit</button>                           
              </div>
            
            
            </div>
          </div>
          </div>
        </div>

        <div class="card text-uppercase">
          <div class="card-header d-flex" id="heading-w2-d1-lunch">
          <h6 class="mr-auto green-txt">
           lunch
          </h6>  
            <h5 class="ml-auto">
                <button class="btn py-0 btn-link collapsed" data-toggle="collapse" data-target="#collapse-w2-d1-lunch" aria-expanded="false" aria-controls="collapseTwo">
                    more details
                    <i class="far fa-plus-square"></i>
              </button>
            </h5>
          </div>
          <div id="collapse-w2-d1-lunch" class="collapse" aria-labelledby="heading-w2-d1-lunch" data-parent="#accordion-w2-d1">
            <div class="card-body">
            
              <div class="row">
                <div class="col-sm-10">
                  <div class="form-group">
                   <textarea id="w3review" name="w3review" disabled class=" txtarea-edit">
                   </textarea>
                </div>
               </div>
                <div class="col-sm-1">
                 <button type="button" class="btn  btn-accordion-edit">edit</button>                           
               </div>
              </div>

              Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
            </div>
          </div>
        </div>
        
        <div class="card text-uppercase">
          <div class="card-header d-flex" id="heading-w2-d1-snack">
            <h6 class="mr-auto green-txt">
              snack
            </h6>
            
            <h5 class="mb-l- auto">
              <button class="btn py-0 btn-link collapsed" data-toggle="collapse" data-target="#collapse-w2-d1-snack" aria-expanded="false" aria-controls="collapseThree">
                more details
                <i class="far fa-plus-square"></i>
              </button>
            </h5>
          </div>
          <div id="collapse-w2-d1-snack" class="collapse" aria-labelledby="heading-w2-d1-snack" data-parent="#accordion-w2-d1">
            <div class="card-body">
            
              <div class="row">
                <div class="col-sm-10">
                  <div class="form-group">
                   <textarea id="w3review" name="w3review" disabled class=" txtarea-edit">
                   </textarea>
                </div>
               </div>
                <div class="col-sm-1">
                 <button type="button" class="btn  btn-accordion-edit">edit</button>                           
               </div>
              </div>

              Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
            </div>
          </div>
        </div>

        <div class="card text-uppercase">
            <div class="card-header d-flex" id="heading-w2-d1-dinner">
            <h6 class="mr-auto green-txt">
             dinner
            </h6>  
              <h5 class="ml-auto">
                  <button class="btn py-0 btn-link collapsed" data-toggle="collapse" data-target="#collapse-w2-d1-dinner" aria-expanded="false" aria-controls="collapseFour">
                      more details
                      <i class="far fa-plus-square"></i>
                </button>
              </h5>
            </div>
            <div id="collapse-w2-d1-dinner" class="collapse" aria-labelledby="heading-w2-d1-dinner" data-parent="#accordion-w2-d1">
              <div class="card-body">
              
                <div class="row">
                  <div class="col-sm-10">
                    <div class="form-group">
                     <textarea id="w3review" name="w3review" disabled class=" txtarea-edit">
                     </textarea>
                  </div>
                 </div>
                  <div class="col-sm-1">
                   <button type="button" class="btn  btn-accordion-edit">edit</button>                           
                 </div>
                </div>

                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
           
              </div>
            </div>
          </div>
        
      </div>
       <!--//w2-d1-->
            </div>
          </div>
        </div>

        <div class="card card-accordion">
          <div class="card-header" id="w2-Two">
            <h5 class="mb-0">
              <button class="btn day-tab text-uppercase btn-link collapsed" data-toggle="collapse" data-target="#collapse-w2-Two" aria-expanded="false" aria-controls="collapseThree">
                sunday
              </button>
            </h5>
          </div>
          <div id="collapse-w2-Two" class="collapse" aria-labelledby="w2-Two" data-parent="#accordion-w2">
            <div class="card-body">
           <!--week2-d2-->

           <div id="accordion-w2-d2">
            <div class="card text-uppercase">
              <div class="card-header d-flex" id="heading-w2-d2-breakfast">
              <h6 class="mr-auto">
               breakfast
              </h6>  
                <h5 class="ml-auto">
                    <button class="btn py-0 btn-link collapsed" data-toggle="collapse" data-target="#collapse-w2-d2-breakfast" aria-expanded="false" aria-controls="collapseTwo">
                        more details
                        <i class="far fa-plus-square"></i>
                  </button>
                </h5>
              </div>
              <div id="collapse-w2-d2-breakfast" class="collapse" aria-labelledby="heading-w2-d2-breakfast" data-parent="#accordion-w2-d2">
                <div class="card-body">
                  Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                </div>
              </div>
            </div>

            <div class="card text-uppercase">
              <div class="card-header d-flex" id="heading-w2-d2-lunch">
              <h6 class="mr-auto">
               lunch
              </h6>  
                <h5 class="ml-auto">
                    <button class="btn py-0 btn-link collapsed" data-toggle="collapse" data-target="#collapse-w2-d2-lunch" aria-expanded="false" aria-controls="collapse-w2-d2-lunch">
                        more details
                        <i class="far fa-plus-square"></i>
                  </button>
                </h5>
              </div>
              <div id="collapse-w2-d2-lunch" class="collapse" aria-labelledby="heading-w2-d2-lunch" data-parent="#accordion-w2-d2">
                <div class="card-body">
                  Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                </div>
              </div>
            </div>
            
            <div class="card text-uppercase">
              <div class="card-header d-flex" id="heading-w2-d2-snack">
                <h6 class="mr-auto">
                  snack
                </h6>
                
                <h5 class="mb-l- auto">
                  <button class="btn py-0 btn-link collapsed" data-toggle="collapse" data-target="#collapse-w2-d2-snack" aria-expanded="false" aria-controls="collapseThree">
                    more details
                    <i class="far fa-plus-square"></i>
                  </button>
                </h5>
              </div>
              <div id="collapse-w2-d2-snack" class="collapse" aria-labelledby="heading-w2-d2-snack" data-parent="#accordion-w2-d2">
                <div class="card-body">
                  Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                </div>
              </div>
            </div>

            <div class="card text-uppercase">
                <div class="card-header d-flex" id="heading-w2-d2-dinner">
                <h6 class="mr-auto">
                 dinner
                </h6>  
                  <h5 class="ml-auto">
                      <button class="btn py-0 btn-link collapsed" data-toggle="collapse" data-target="#collapse-w2-d2-dinner" aria-expanded="false" aria-controls="collapseFour">
                          more details
                          <i class="far fa-plus-square"></i>
                    </button>
                  </h5>
                </div>
                <div id="collapse-w2-d2-dinner" class="collapse" aria-labelledby="heading-w2-d2-dinner" data-parent="#accordion-w2-d2">
                  <div class="card-body">
                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                  </div>
                </div>
              </div>
            
          </div>

           <!--//week2-d2-->
              


            </div>
          </div>
        </div>

        <div class="card card-accordion">
          <div class="card-header" id="w2-Three">
            <h5 class="mb-0">
              <button class="btn day-tab btn-link text-uppercase  ollapsed" data-toggle="collapse" data-target="#collapse-w2-Three" aria-expanded="false" aria-controls="collapseThree">
                monday
              </button>
            </h5>
          </div>
          <div id="collapse-w2-Three" class="collapse" aria-labelledby="w2-Three" data-parent="#accordion-w2">
            <div class="card-body">
           <!--week2-d3-->

           <div id="accordion-w2-d3">
            <div class="card text-uppercase">
              <div class="card-header d-flex" id="heading-w2-d3-breakfast">
              <h6 class="mr-auto">
               breakfast
              </h6>  
                <h5 class="ml-auto">
                    <button class="btn py-0 btn-link collapsed" data-toggle="collapse" data-target="#collapse-w2-d3-breakfast" aria-expanded="false" aria-controls="collapseTwo">
                        more details
                        <i class="far fa-plus-square"></i>
                  </button>
                </h5>
              </div>
              <div id="collapse-w2-d3-breakfast" class="collapse" aria-labelledby="heading-w2-d3-breakfast" data-parent="#accordion-w2-d3">
                <div class="card-body">
                  Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                </div>
              </div>
            </div>

            <div class="card text-uppercase">
              <div class="card-header d-flex" id="heading-w2-d3-lunch">
              <h6 class="mr-auto">
               lunch
              </h6>  
                <h5 class="ml-auto">
                    <button class="btn py-0 btn-link collapsed" data-toggle="collapse" data-target="#collapse-w2-d3-lunch" aria-expanded="false" aria-controls="collapse-w2-d3-lunch">
                        more details
                        <i class="far fa-plus-square"></i>
                  </button>
                </h5>
              </div>
              <div id="collapse-w2-d3-lunch" class="collapse" aria-labelledby="heading-w2-d3-lunch" data-parent="#accordion-w2-d3">
                <div class="card-body">
                  Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                </div>
              </div>
            </div>
            
            <div class="card text-uppercase">
              <div class="card-header d-flex" id="heading-w2-d3-snack">
                <h6 class="mr-auto">
                  snack
                </h6>
                
                <h5 class="mb-l- auto">
                  <button class="btn py-0 btn-link collapsed" data-toggle="collapse" data-target="#collapse-w2-d3-snack" aria-expanded="false" aria-controls="collapseThree">
                    more details
                    <i class="far fa-plus-square"></i>
                  </button>
                </h5>
              </div>
              <div id="collapse-w2-d3-snack" class="collapse" aria-labelledby="heading-w2-d3-snack" data-parent="#accordion-w2-d3">
                <div class="card-body">
                  Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                </div>
              </div>
            </div>

            <div class="card text-uppercase">
                <div class="card-header d-flex" id="heading-w2-d3-dinner">
                <h6 class="mr-auto">
                 dinner
                </h6>  
                  <h5 class="ml-auto">
                      <button class="btn py-0 btn-link collapsed" data-toggle="collapse" data-target="#collapse-w2-d3-dinner" aria-expanded="false" aria-controls="collapseFour">
                          more details
                          <i class="far fa-plus-square"></i>
                    </button>
                  </h5>
                </div>
                <div id="collapse-w2-d3-dinner" class="collapse" aria-labelledby="heading-w2-d3-dinner" data-parent="#accordion-w2-d3">
                  <div class="card-body">
                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                  </div>
                </div>
              </div>
            
          </div>

           <!--//week2-d3-->
              


            </div>
          </div>
        </div>

        <div class="card card-accordion">
          <div class="card-header" id="w2-four">
            <h5 class="mb-0">
              <button class="btn day-tab text-uppercase btn-link collapsed" data-toggle="collapse" data-target="#collapse-w2-four" aria-expanded="false" aria-controls="collapseThree">
                tuseday
              </button>
            </h5>
          </div>
          <div id="collapse-w2-four" class="collapse" aria-labelledby="w2-four" data-parent="#accordion-w2">
            <div class="card-body">
            
              <!--w2-d4-->
              <div id="accordion-w2-d4">
                <div class="card text-uppercase">
                  <div class="card-header d-flex" id="heading-w2-d4-breakfast">
                  <h6 class="mr-auto">
                   breakfast
                  </h6>  
                    <h5 class="ml-auto">
                        <button class="btn py-0 btn-link collapsed" data-toggle="collapse" data-target="#collapse-w2-d4-breakfast" aria-expanded="false" aria-controls="collapseTwo">
                            more details
                            <i class="far fa-plus-square"></i>
                      </button>
                    </h5>
                  </div>
                  <div id="collapse-w2-d4-breakfast" class="collapse" aria-labelledby="heading-w2-d4-breakfast" data-parent="#accordion-w2-d4">
                    <div class="card-body">
                      Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                    </div>
                  </div>
                </div>

                <div class="card text-uppercase">
                  <div class="card-header d-flex" id="heading-w2-d1-lunch">
                  <h6 class="mr-auto">
                   lunch
                  </h6>  
                    <h5 class="ml-auto">
                        <button class="btn py-0 btn-link collapsed" data-toggle="collapse" data-target="#collapse-w2-d4-lunch" aria-expanded="false" aria-controls="collapseTwo">
                            more details
                            <i class="far fa-plus-square"></i>
                      </button>
                    </h5>
                  </div>
                  <div id="collapse-w2-d4-lunch" class="collapse" aria-labelledby="heading-w2-d4-lunch" data-parent="#accordion-w2-d4">
                    <div class="card-body">
                      Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                    </div>
                  </div>
                </div>
                
                <div class="card text-uppercase">
                  <div class="card-header d-flex" id="heading-w2-d4-snack">
                    <h6 class="mr-auto">
                      snack
                    </h6>
                    
                    <h5 class="mb-l- auto">
                      <button class="btn py-0 btn-link collapsed" data-toggle="collapse" data-target="#collapse-w2-d4-snack" aria-expanded="false" aria-controls="collapseThree">
                        more details
                        <i class="far fa-plus-square"></i>
                      </button>
                    </h5>
                  </div>
                  <div id="collapse-w2-d4-snack" class="collapse" aria-labelledby="heading-w2-d4-snack" data-parent="#accordion-w2-d4">
                    <div class="card-body">
                      Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                    </div>
                  </div>
                </div>

                <div class="card text-uppercase">
                    <div class="card-header d-flex" id="heading-w2-d4-dinner">
                    <h6 class="mr-auto">
                     dinner
                    </h6>  
                      <h5 class="ml-auto">
                          <button class="btn py-0 btn-link collapsed" data-toggle="collapse" data-target="#collapse-w2-d4-dinner" aria-expanded="false" aria-controls="collapseFour">
                              more details
                              <i class="far fa-plus-square"></i>
                        </button>
                      </h5>
                    </div>
                    <div id="collapse-w2-d4-dinner" class="collapse" aria-labelledby="heading-w2-d4-dinner" data-parent="#accordion-w2-d4">
                      <div class="card-body">
                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                      </div>
                    </div>
                  </div>
                
              </div>
              <!--//w2-d4-->

            </div>
          </div>
        </div>

        <div class="card">
          <div class="card-header" id="w2-five">
            <h5 class="mb-0">
              <button class="btn day-tab text-uppercase  btn-link collapsed" data-toggle="collapse" data-target="#collapse-w2-five" aria-expanded="false" aria-controls="collapseThree">
                wednesday
              </button>
            </h5>
          </div>
          <div id="collapse-w2-five" class="collapse" aria-labelledby="w2-five" data-parent="#accordion-w2">
            <div class="card-body">
           <!--w2-d5-->
           
      <div id="accordion-w2-d5">
        <div class="card text-uppercase">
          <div class="card-header d-flex" id="heading-w2-d5-breakfast">
          <h6 class="mr-auto">
           breakfast
          </h6>  
            <h5 class="ml-auto">
                <button class="btn py-0 btn-link collapsed" data-toggle="collapse" data-target="#collapse-w2-d5-breakfast" aria-expanded="false" aria-controls="collapseTwo">
                    more details
                    <i class="far fa-plus-square"></i>
              </button>
            </h5>
          </div>
          <div id="collapse-w2-d5-breakfast" class="collapse" aria-labelledby="heading-w2-d5-breakfast" data-parent="#accordion-w2-d5">
            <div class="card-body">
              Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
            </div>
          </div>
        </div>

        <div class="card text-uppercase">
          <div class="card-header d-flex" id="heading-w2-d5-lunch">
          <h6 class="mr-auto">
           lunch
          </h6>  
            <h5 class="ml-auto">
                <button class="btn py-0 btn-link collapsed" data-toggle="collapse" data-target="#collapse-w2-d5-lunch" aria-expanded="false" aria-controls="collapseTwo">
                    more details
                    <i class="far fa-plus-square"></i>
              </button>
            </h5>
          </div>
          <div id="collapse-w2-d5-lunch" class="collapse" aria-labelledby="heading-w2-d5-lunch" data-parent="#accordion-w2-d5">
            <div class="card-body">
              Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
            </div>
          </div>
        </div>
        
        <div class="card text-uppercase">
          <div class="card-header d-flex" id="heading-w2-d5-snack">
            <h6 class="mr-auto">
              snack
            </h6>
            
            <h5 class="mb-l- auto">
              <button class="btn py-0 btn-link collapsed" data-toggle="collapse" data-target="#collapse-w2-d5-snack" aria-expanded="false" aria-controls="collapseThree">
                more details
                <i class="far fa-plus-square"></i>
              </button>
            </h5>
          </div>
          <div id="collapse-w2-d5-snack" class="collapse" aria-labelledby="heading-w2-d5-snack" data-parent="#accordion-w2-d5">
            <div class="card-body">
              Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
            </div>
          </div>
        </div>

        <div class="card text-uppercase">
            <div class="card-header d-flex" id="heading-w2-d5-dinner">
            <h6 class="mr-auto">
             dinner
            </h6>  
              <h5 class="ml-auto">
                  <button class="btn py-0 btn-link collapsed" data-toggle="collapse" data-target="#collapse-w2-d5-dinner" aria-expanded="false" aria-controls="collapseFour">
                      more details
                      <i class="far fa-plus-square"></i>
                </button>
              </h5>
            </div>
            <div id="collapse-w2-d5-dinner" class="collapse" aria-labelledby="heading-w2-d5-dinner" data-parent="#accordion-w2-d5">
              <div class="card-body">
                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
              </div>
            </div>
          </div>
        
      </div>
           <!--//w2-d5-->
            </div>
          </div>
        </div>

        <div class="card card-accordion">
          <div class="card-header" id="w2-six">
            <h5 class="mb-0">
              <button class="btn day-tab text-uppercase  btn-link collapsed" data-toggle="collapse" data-target="#collapse-w2-six" aria-expanded="false" aria-controls="collapseThree">
                thursday
              </button>
            </h5>
          </div>
          <div id="collapse-w2-six" class="collapse" aria-labelledby="w2-six" data-parent="#accordion-w2">
            <div class="card-body">
              <!--w2-d6-->
              <div id="accordion-w2-d6">
                <div class="card text-uppercase">
                  <div class="card-header d-flex" id="heading-w2-d6-breakfast">
                  <h6 class="mr-auto">
                   breakfast
                  </h6>  
                    <h5 class="ml-auto">
                        <button class="btn py-0 btn-link collapsed" data-toggle="collapse" data-target="#collapse-w2-d6-breakfast" aria-expanded="false" aria-controls="collapseTwo">
                            more details
                            <i class="far fa-plus-square"></i>
                      </button>
                    </h5>
                  </div>
                  <div id="collapse-w2-d6-breakfast" class="collapse" aria-labelledby="heading-w2-d6-breakfast" data-parent="#accordion-w2-d6">
                    <div class="card-body">
                      Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                    </div>
                  </div>
                </div>

                <div class="card text-uppercase">
                  <div class="card-header d-flex" id="heading-w2-d6-lunch">
                  <h6 class="mr-auto">
                   lunch
                  </h6>  
                    <h5 class="ml-auto">
                        <button class="btn py-0 btn-link collapsed" data-toggle="collapse" data-target="#collapse-w2-d6-lunch" aria-expanded="false" aria-controls="collapseTwo">
                            more details
                            <i class="far fa-plus-square"></i>
                      </button>
                    </h5>
                  </div>
                  <div id="collapse-w2-d6-lunch" class="collapse" aria-labelledby="heading-w2-d6-lunch" data-parent="#accordion-w2-d6">
                    <div class="card-body">
                      Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                    </div>
                  </div>
                </div>
                
                <div class="card text-uppercase">
                  <div class="card-header d-flex" id="heading-w2-d6-snack">
                    <h6 class="mr-auto">
                      snack
                    </h6>
                    
                    <h5 class="mb-l- auto">
                      <button class="btn py-0 btn-link collapsed" data-toggle="collapse" data-target="#collapse-w2-d6-snack" aria-expanded="false" aria-controls="collapseThree">
                        more details
                        <i class="far fa-plus-square"></i>
                      </button>
                    </h5>
                  </div>
                  <div id="collapse-w2-d6-snack" class="collapse" aria-labelledby="heading-w2-d6-snack" data-parent="#accordion-w2-d6">
                    <div class="card-body">
                      Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                    </div>
                  </div>
                </div>

                <div class="card text-uppercase">
                    <div class="card-header d-flex" id="heading-w2-d6-dinner">
                    <h6 class="mr-auto">
                     dinner
                    </h6>  
                      <h5 class="ml-auto">
                          <button class="btn py-0 btn-link collapsed" data-toggle="collapse" data-target="#collapse-w2-d6-dinner" aria-expanded="false" aria-controls="collapseFour">
                              more details
                              <i class="far fa-plus-square"></i>
                        </button>
                      </h5>
                    </div>
                    <div id="collapse-w2-d6-dinner" class="collapse" aria-labelledby="heading-w2-d6-dinner" data-parent="#accordion-w2-d6">
                      <div class="card-body">
                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                      </div>
                    </div>
                  </div>
                
              </div>
              <!--//w2-d6-->
            </div>
          </div>
        </div>
        
        <div class="card card-accordion">
          <div class="card-header" id="w2-seven">
            <h5 class="mb-0">
              <button class="btn day-tab text-uppercase  btn-link collapsed" data-toggle="collapse" data-target="#collapse-w2-seven" aria-expanded="false" aria-controls="collapseThree">
                friday
              </button>
            </h5>
          </div>
          <div id="collapse-w2-seven" class="collapse" aria-labelledby="w2-seven" data-parent="#accordion-w2">
            <div class="card-body">
              <!--w2-d7-->
              <div id="accordion-w2-d7">
                <div class="card text-uppercase">
                  <div class="card-header d-flex" id="heading-w2-d7-breakfast">
                  <h6 class="mr-auto">
                   breakfast
                  </h6>  
                    <h5 class="ml-auto">
                        <button class="btn py-0 btn-link collapsed" data-toggle="collapse" data-target="#collapse-w2-d7-breakfast" aria-expanded="false" aria-controls="collapseTwo">
                            more details
                            <i class="far fa-plus-square"></i>
                      </button>
                    </h5>
                  </div>
                  <div id="collapse-w2-d7-breakfast" class="collapse" aria-labelledby="heading-w2-d7-breakfast" data-parent="#accordion-w2-d7">
                    <div class="card-body">
                      Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                    </div>
                  </div>
                </div>

                <div class="card text-uppercase">
                  <div class="card-header d-flex" id="heading-w2-d7-lunch">
                  <h6 class="mr-auto">
                   lunch
                  </h6>  
                    <h5 class="ml-auto">
                        <button class="btn py-0 btn-link collapsed" data-toggle="collapse" data-target="#collapse-w2-d7-lunch" aria-expanded="false" aria-controls="collapseTwo">
                            more details
                            <i class="far fa-plus-square"></i>
                      </button>
                    </h5>
                  </div>
                  <div id="collapse-w2-d7-lunch" class="collapse" aria-labelledby="heading-w2-d7-lunch" data-parent="#accordion-w2-d7">
                    <div class="card-body">
                      Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                    </div>
                  </div>
                </div>
                
                <div class="card text-uppercase">
                  <div class="card-header d-flex" id="heading-w2-d7-snack">
                    <h6 class="mr-auto">
                      snack
                    </h6>
                    
                    <h5 class="mb-l- auto">
                      <button class="btn py-0 btn-link collapsed" data-toggle="collapse" data-target="#collapse-w2-d7-snack" aria-expanded="false" aria-controls="collapseThree">
                        more details
                        <i class="far fa-plus-square"></i>
                      </button>
                    </h5>
                  </div>
                  <div id="collapse-w2-d7-snack" class="collapse" aria-labelledby="heading-w2-d7-snack" data-parent="#accordion-w2-d7">
                    <div class="card-body">
                      Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                    </div>
                  </div>
                </div>

                <div class="card text-uppercase">
                    <div class="card-header d-flex" id="heading-w2-d7-dinner">
                    <h6 class="mr-auto">
                     dinner
                    </h6>  
                      <h5 class="ml-auto">
                          <button class="btn py-0 btn-link collapsed" data-toggle="collapse" data-target="#collapse-w2-d7-dinner" aria-expanded="false" aria-controls="collapseFour">
                              more details
                              <i class="far fa-plus-square"></i>
                        </button>
                      </h5>
                    </div>
                    <div id="collapse-w2-d7-dinner" class="collapse" aria-labelledby="heading-w2-d7-dinner" data-parent="#accordion-w2-d7">
                      <div class="card-body">
                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                      </div>
                    </div>
                  </div>
                
              </div>
              <!--//w2-d7-->
            </div>
          </div>
        </div>

      </div>
      
      <!--//accordion days -->
  