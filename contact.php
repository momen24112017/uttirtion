<!DOCTYPE html>
<html lang="en">

<head>
    <title>u trition</title>
    <!-- Meta tag Keywords -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="UTF-8" />
    <meta name="keywords" />

    <!--// Meta tag Keywords -->

    <!-- Custom-Files -->
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/fontawesome-free-5.13.1-web/css/all.css">
    <link rel="stylesheet" href="css/style.css" type="text/css" media="all" />
    <!-- Style-CSS -->
    <!-- //Custom-Files -->

    <!-- Web-Fonts -->
    <link
        href="//fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i&amp;subset=latin-ext"
        rel="stylesheet">
    <link
        href="//fonts.googleapis.com/css?family=Barlow+Semi+Condensed:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">
    <!-- //Web-Fonts -->
    <script src="js/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
    <!--include angular-->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/angularjs-toaster/3.0.0/toaster.min.css" rel="stylesheet" />
    <link rel="icon" type="image/png" sizes="96x96" href="images/favicon-32x32.png">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.0/angular.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.4.0/angular-animate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angularjs-toaster/3.0.0/toaster.min.js"></script>
    <script src="./js/login.js"> </script>
    <script src="./js/common.js"> </script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/angularjs-toaster/3.0.0/toaster.min.css" rel="stylesheet" />
</head>

<body ng-controller="myLCtrl" ng-app="myLApp">
    <toaster-container></toaster-container>
    <div class="container-fluid px-0">

        <!-- navbar -->
        
<!-- top-header -->

   <nav id="home" class="navbar nav-txt-style hide-md navbar-expand-lg navbar-light header-bg">
            <span class=" navbar-text "><i class="fa orange-txt fa-map-marker mr-2"></i>UAE</span>
            <span class="navbar-text ml-3"><i class="fa orange-txt fa-phone mr-2 "></i> (+971) 526920873</span>
            <div class="ml-auto" id="navbarText">
                <a ng-if="User==undefined" href="log_reg.php" class="  btn login-button-2 stand-btn mr-3 text-uppercase">
                    <span class="fa fa-sign-in  mr-2"></span>Login / register
                </a>
                <ul class="navbar-nav mr-auto">   
                <li class="nav-item dropdown">
                <a  class="nav-link dropdown-toggle" ng-if="User!=undefined" href="profile.php" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >  
                     <span  class="navbar-text text-capitalize mr-2">
                         <span>
                                 <i class="fas fa-user mr-1"></i>
                         </span>     Hello, {{User.name}}
                     </span>
                </a>
                <div class="dropdown-menu w-100 text-center" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="profile.php">My Profile</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item"ng-click="logout()"><span  ng-if="User!=undefined">logout</span></a>
      
         
        </div>
                </li>
                </ul>
               
                <!-- <span class="navbar-text text-capitalize mr-2">
                    follow us :
                </span>
                <span class="fa-cont">
                    <span class="s"> <i class="fab fa-twitter mr-2"></i></span>
                    <i class="fab fa-facebook-f mr-2"></i>
                </span> -->
            </div>
        </nav>
        <!-- //top-header -->
        <!--navbar-->
        <nav class="navbar navbar-expand-lg navbar-light nav-bg">
            <a class="navbar-brand" href="index.php">
                <img src="images/logo_crop.jpg" alt="utrition logo" class="nav-logo"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02"
                aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
                <ul class="navbar-nav ml-auto  mt-2 mt-lg-0">
                    <li class="nav-item "><a class="nav-link" href="index.php#meal_plans">meal plans</a></li>
                    <li class="nav-item "><a class="nav-link" href="index.php#about">About Us</a></li>
                    <li class="nav-item "><a class="nav-link" ng-if="User!=undefined" href="manage_plan.php">my meal
                            plan</a></li>
                    <li class="nav-item "><a class="nav-link" href="gallery.php">gallery</a></li>        
                    <li class="nav-item "><a class="nav-link" href="faq.php">faq's</a></li>
                    <li class="nav-item active"><a class="nav-link" href="contact.php">contact us</a></li>
                    <li class="nav-item d-lg-none d-xl-none"><a class="nav-link" ng-if="User==undefined"
                            href="log_reg.html">logIn/Register</a>
                    </li>
                </ul>
            </div>
        </nav>
        <!--//navbar-->
        <!--//navbar-->
        <div class="row no-gutters">
            <div class="col-sm-12 ">
                <h1 class="main-title text-center  mb-4 green-txt">
               
                    <span class="text-uppercase ">let's Evolve<span class="orange-txt secfont"> a Healthier version
                        </span> of you</span>
                </h1>
            </div>
            <div class="col-sm-12 ">
                <p class="text-center  ">
                    Please fill your details in the form below, and we will be contacting you soon.
                </p>
            </div>
            <div class="stand-block">

            </div>
        </div>
        <div class="row no-gutters">
            <div class="col-sm-5">
                <h3 class=" text-center  my-4 green-txt">
                    <span class="text-uppercase ">working hours
                    </span>
                </h3>

                <h3 class=" text-center darkgray-txt  my-4 ">
                    <span class="text-uppercase ">9am - 8pm, Saturday to Thursday
                    </span>
                </h3>
                <!-- <p class="text-center">branch name</p> -->
                <p class="text-center">Tel:(+971) 526920873</p>
                <p class="text-center">E-mail:info@utritionlife.com</p>

            </div>
            <div class="col-sm-6">
                <h3 class=" text-center  my-4 green-txt">
                    <span class="text-uppercase ">contact form
                    </span>
                </h3>
                <form method="post" name="form" ng-class="{true: 'error'}[form.$invalid]">
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="firstnameContact">first name</label>
                            <span style="color:red" ng-show="form.first_name.$error.required">*</span>
                            <input type="text" class="form-control" ng-model="objContactUs.first_name"
                                id="firstnameContact" name="first_name" required="">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="lastnameContact">last name</label>
                            <span style="color:red" ng-show="form.last_name.$error.required">*</span>
                            <input type="text" class="form-control" id="lastnameContact"
                                ng-model="objContactUs.last_name" name="last_name" required="">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="emailContact">e-mail</label>
                            <span style="color:red" ng-show="form.email.$error.required">*</span>
                            <input type="email" class="form-control" id="emailContact" ng-model="objContactUs.email"
                                name="email" required="">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="phoneContact">phone</label>
                            <span style="color:red" ng-show="form.phone.$error.required">*</span>
                            <input type="number" class="form-control" id="phoneContact" ng-model="objContactUs.phone"
                                name="phone" required="">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="messageContact" class="col-form-label">Message: </label>
                            <span style="color:red" ng-show="form.message.$error.required">*</span>
                            <textarea class="form-control" name="message" rows="5" ng-model="objContactUs.message"
                                id="messageContact" required=""></textarea>
                        </div>
                    </div>
                    <!-- <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="weightContact">weight :</label>
                            <span style="color:red" ng-show="form.weight.$error.required">*</span>
                            <input type="number" name="weight" class="form-control" ng-model="objContactUs.weight"
                                id="weightContact" required="">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="goalContact">goal :</label>
                            <span style="color:red" ng-show="form.goal.$error.required">*</span>
                            <select id="goalContact" name="goal" ng-model="objContactUs.goal" class="form-control"
                                required="">
                                <option value="false" selected  disabled>please Choose...</option>
                                <option value="mentain weight">mentain weight</option>
                                <option value="weight loss">weight loss</option>
                                <option value="gain mass">gain mass</option>
                            </select>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="tergetweightContact">terget weight :</label>
                            <span style="color:red" ng-show="form.target_weight.$error.required">*</span>
                            <input type="number" name="target_weight" class="form-control"
                                ng-model="objContactUs.target_weight" id="tergetweightContact" required="">
                        </div> 
                    </div>-->
                    <div class="form-row">
                        <div class="form-group text-right col-md-12">
                            <button type="submit" ng-click="contactUs()" class="btn  btn-on-car"
                                ng-disabled="!form.$valid">

                                <div ng-if="contactUsSpn==false">submit</div>

                                <div ng-if="contactUsSpn" class="text-center">
                                    <div class="spinner-border" role="status">
                                        <span class="sr-only">Loading...</span>
                                    </div>
                                </div>

                            </button>
                        </div>
                    </div>


                </form>
            </div>
        </div>


        <!-- footer -->
        <?php include 'footer.php';?>
        <!-- //footer -->

    </div>
</body>

</html>