<!DOCTYPE html>
<html lang="en">

<head>
    <title>U trition</title>
    <!-- Meta tag Keywords -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="UTF-8" />
    <meta name="keywords" />
    <script>
        addEventListener("load", function () {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }
    </script>
    <!--// Meta tag Keywords -->

    <!-- Custom-Files -->
    <link rel="stylesheet" href="css/bootstrap.css">
    <!-- Bootstrap-Core-CSS -->
    <link rel="stylesheet" href="css/fontawesome-free-5.13.1-web/css/all.css">
    <link rel="stylesheet" href="css/style.css" type="text/css" media="all" />
    <!-- Style-CSS -->

    <!-- Font-Awesome-Icons-CSS -->
    <!-- //Custom-Files -->
    <!-- Web-Fonts -->
    <link
        href="//fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i&amp;subset=latin-ext"
        rel="stylesheet">
    <link
        href="//fonts.googleapis.com/css?family=Barlow+Semi+Condensed:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">
    <!-- //Web-Fonts -->
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.0/angular.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.4.0/angular-animate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angularjs-toaster/3.0.0/toaster.min.js"></script>
    <script src="./js/plan_detail.js"> </script>
    <script src="js/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>

    <script src="./js/common.js"> </script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/angularjs-toaster/3.0.0/toaster.min.css" rel="stylesheet" />
    <link rel="icon" type="image/png" sizes="96x96" href="images/favicon-32x32.png">
</head>

<body ng-controller="myPlanDetailsCtrl" ng-app="myPlanDetailsApp">
    <toaster-container></toaster-container>
    <!--navbar-->
    <?php include 'navbar.php';?>
    <!--//navbar-->
    <!-- banner -->
    <div class="bg-banner" style="background-image:url({{plan.banner_img}})">
        <!--<div class="bg-banner">-->
        <div class="row ">

            <!--     <img src={{plan.banner_img}} class="main-banner-2 img-responsive">-->
            <!--  <img src="images/banner-2-test6.jpg" alt="plan img" class=" main-banner-2 img-responsive">-->
            <div class="banner-2-overlay">
                <p class="banner-2-txt orange-txt">{{plan.name}}</p>
                <a href="review_details.php" ng-if="User!=undefined">
                    <button type="button" href="review_details.php" class="btn  white-txt top-btn mt-4 btn-on-car">Buy
                        Now<span class="fa fa-caret-right ml-1" aria-hidden="true"></span></button>

                </a>
                <a href="log_reg.php" ng-if="User==undefined">
                    <button type="button" class="btn  white-txt top-btn mt-4 btn-on-car">login to buy a plan<span
                            class="fa fa-caret-right ml-1" aria-hidden="true"></span></button>
                </a>
            </div>
        </div>
    </div>
    <!-- //banner -->
    <!-- page details -->
    <div class="row no-gutters">
        <div class="px-0 col-sm-12">
            <div class="breadcrumb-agile bg-light py-2">
                <ol class="breadcrumb bg-light m-0">
                    <li class="breadcrumb-item">
                        <a href="index.php">Home</a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">plan_detail</li>
                </ol>
            </div>
        </div>
    </div>

    <!-- //page details -->
    <div class="stand-block">

    </div>
    <!-- plan_detail -->
    <div class="row no-gutters justify-content-center ">
        <div class="col-sm-3">
            <div class="index-main-p pl-3 ">
                <h5 class="green-txt mb-4 text-capitalize"><span>{{ plan.name }}</span></h5>
                <p class="mb-3 ">
                <h3 class="orange-txt"><sub class="green-txt pr-1">from</sub>{{plan.price_per_day}} <span>AED
                    </span><sub>/ Day</sub></h3>
                <!-- <p class="my-3">{{plan.long_desc}}</p> -->
                <p class="my-2">{{plan.short_desc}}</p>

                <!--   <p class="mt-3 ">
                <h5 class="darkgray-txt">AED <span>{{plan.price}} </span><sub>/ month</sub></h5>
                </p>
    -->

                <!--modal-->
                <!-- Button trigger modal -->
                <button type="button" class="btn btn-primary mt-3" data-toggle="modal" data-target="#exampleModal">
                    Menu Sample
                </button>

                <!-- Modal -->
                <div class="modal  fade" id="exampleModal" tabindex="-1" role="dialog"
                    aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-style-plan-det mx-auto" role="document">
                        <div class="modal-content  modal-content-style-plan-det ">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">{{ plan.name }}</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <img src="{{ plan.menu_sample }}" class="img-responsive img-modal-menu" alt="">
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

                            </div>
                        </div>
                    </div>
                </div>
                <!--//modal-->

            </div>
        </div>

        <div class="col-sm-4  ">
            <div class="pl-3">
                <h5 class="green-txt mb-4 text-capitalize">{{ plan.name }}</h5>
                <p>{{ plan.long_desc }}</p>
                <!--    <img src="{{ plan.paln_table }}" class="my-2 table-gluten-plan-img" alt="" >-->
            </div>


        </div>

        <div class="col-sm-4">
            <div class="index-main-p side-overlay   pl-xl-3">
                <h5 class="green-txt mb-4 text-capitalize">Why you need to<span class="secfont orange-txt">
                        follow</span> this Diet
                </h5>
             
                <ul class="darkgray-txt">
                    <div class="side-bg-new">
                        <li ng-repeat="item in plan.long_feature">{{ item }}</li>
                    </div>
                </ul>
            </div>
        </div>
    </div>

    <div class="row no-gutters">
        <div class="col-sm-4 offset-sm-3">
            <!--table of gluten free-->
            <div class="tbl-bg table-responsive" ng-if="plan.paln_table!=null">
                <table class="table table-border  darkgray-txt my-3">
                    <thead>
                        <tr>
                            <th scope="col"></th>
                            <th scope="col">Gluten Allergy</th>
                            <th scope="col">Gluten Intolerance </th>

                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th scope="row">Definition</th>
                            <td>Genetic, autoimmune disorder, when gluten is ingested it triggers damage to the
                                small
                                intestine </td>
                            <td>Intolerance, feel uncomfortable without causing damage to the small intestine. </td>

                        </tr>
                        <tr>
                            <th scope="row">Gastrointestinal Symptoms </th>
                            <td>Diarrhea, bloating and abdominal pain</td>
                            <td>Diarrhea, constipation, bloating and abdominal pain.</td>

                        </tr>
                        <tr>
                            <th scope="row">General Symptoms</th>
                            <td>weight loss, joints and bond pain, numbness in legs and hands, mouth ulcers and
                                tooth
                                enamel
                                loss, hair loss.</td>
                            <td>Headache, low energy, skin problems, depression, mood swings, difficulty to lose
                                weight,
                                brain for, Hyperactivity (kids).</td>

                        </tr>
                        <tr>
                            <th scope="row">Clinical findings </th>
                            <td>Deficiencies of : Iron, Vitamin D, calcium, B12, Zinc, and copper.
                                <hr>Positive antibodies Test especially Immunoglobulin E (IgE)
                            </td>
                            <td>If goes untreated on the long term, it may cause deficiencies, in Iron, Vitamin D,
                                Zn
                                and
                                B12.
                                <hr>Positive antibodies Test only for
                                Immunoglobulin G (IgG)</td>

                        </tr>
                        <tr>
                            <th scope="row">Abnormal intestine Biopsy </th>
                            <td>Yes.</td>
                            <td>No.</td>

                        </tr>
                        <tr>
                            <th scope="row">Treatment </th>
                            <td>Strictly a life time Gluten Free Diet. </td>
                            <td>Gluten free diet should be followed for at least 6 month to allow healing of the
                                Gut.
                            </td>

                        </tr>
                        <tr>
                            <th scope="row">Onset of the Symptoms </th>
                            <td>Can start within 2 hours after ingesting food with Gluten.</td>
                            <td>Symptoms can start from 2 hours up to 72 hours.</td>

                        </tr>
                        <tr>
                            <th scope="row">Prevalence </th>
                            <td>Affects only 1% of the general population, but more than 75% they go undiagnosed .
                            </td>
                            <td>Affects more than 45% of the population.</td>

                        </tr>
                    </tbody>
                </table>
            </div>
            <!--//table of gluten free-->
        </div>
    </div>

    <!--
        <div class="row justify-content-center mt-4">
            <div class="col-md-8 mb-2">
                <div class="gallery-demo">
                    <a href="#gal1">
                        <img src="http://localhost/uttortion_backend/storage/app/public/{{plan.banner_img}}" alt=" "
                            class="img-fluid" />
                        <h4 class="p-mask">complete and balanced plan <span>${{plan.price}}</span></h4>
                        <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam sed mauris sem.
                            Orci
                            varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
                            Quisque tincidunt malesuada nulla, vitae blandit nisl sagittis quis. Nam non
                            accumsan
                            nisi, elementum rhoncus diam. Suspendisse nec massa in lectus tincidunt mattis.
                        </p>
                    </a>
                </div>
            </div>
        </div> -->


    <!--footer-->
    <?php include 'footer.php';?>
    <!--//footer-->
    <!-- move top icon -->
    <a href="index.php#home" class="move-top text-center">
        <span class="fa fa-level-up" aria-hidden="true"></span>
    </a>
    <!-- //move top icon -->
    </div>
</body>

</html>