     
      <!--accordion days -->
       
      <div id="accordion-{{key}}">
        <div class="card card-accordion">
          <div class="card-header d-flex" id="heading-w1-one">
            <h5 class="mr-auto mb-0">
              <button class="btn  day-tab text-uppercase  btn-link" data-toggle="collapse" data-target="#collapse-{{week.day.trim()}}" aria-expanded="true" aria-controls="w1-one">
                {{week.day}}  {{week.date}}
              </button>
            </h5>
            <h5 class="ml-auto">
              <!--edit modal-->
<!-- Button trigger modal -->
<!-- <button type="button" class="btn text-capitalize  stand-btn btn-accordion-edit" data-toggle="modal" data-target="#exampleModal">
edit <i class="fas fa-edit"></i></button>
</button> -->

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   <div class="modal-dialog">
     <div class="modal-content">
       <div class="modal-header">
         <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
         <span aria-hidden="true">&times;</span>
         </button>
       </div>
       <div class="modal-body">
         <div class="form-group">

           <label for="timeedit_w1">drop off</label>
           <input type="text" class="form-control mb-2" id="timeedit_w1" disabled placeholder="Example input placeholder">

           <label for="addressedit_w1">address</label>
           <input type="text" class="form-control mb-2" id="addressedit_w1" disabled placeholder="Example input placeholder">

         </div>
       </div>
      <div class="modal-footer">
         <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
         <button type="button" class="btn btn-on-car">Save changes</button>
      </div>
     </div>
     </div>
   </div>

              <!--//edit modal-->
            
</div>
      
          <div id="collapse-{{week.day.trim()}}" class="collapse show" aria-labelledby="heading-w1-one"  data-parent="#accordion-{{key}}">
            <div class="card-body">
       
       <div id="accordion-{{week.day.trim()}}" ng-repeat="(key1,obj) in week.arrMeal">
        <div class="card text-uppercase">
          <div class="card-header d-flex" id="heading-w1-d1-{{key}}">
          
          <h6 class="mr-auto green-txt">
          <button class="btn  day-tab text-uppercase  btn-link" type="button" data-toggle="collapse" data-target="#collapse-{{key}}-{{week.day.trim()}}-{{key1}}">
           {{key1}}
           </button>
          </h6>  
          
            
          </div>
          <div class="collapse" id="collapse-{{key}}-{{week.day.trim()}}-{{key1}}" >
            <div class="card-body">
          
                <div class="row">
                         
            <div class="col-sm-11">
               
               <!--radio botton choices-->
   
               <div class="form-check" ng-repeat="meal in obj track by $index">

                   <input id="{{$index}}" class="form-check-input" type="radio" ng-click="pushArray(week.day,week.date,key,key1,meal)" name="radio-{{key}}-{{key1}}" id="radio-w1-d1-br-1" value="{{meal}}">
                   <label class="form-check-label" for="radio-{{key}}-{{key1}}">
                     {{meal}}
                   </label> 
               </div>
             
                  <!--//radio botton choices-->     
            </div>
             <!-- <div class="col-sm-1">
                <button type="button" class="btn  btn-accordion-edit">edit</button>                           
              </div> -->
            
            
            </div>
          </div>
          </div>
        </div>

     
       <!--//w1-d1-->
            </div>
          </div>
        </div>


       

      </div>
      
    <!--//accordion days -->
  