<!DOCTYPE html>
<html lang="en">

<head>
    <title>u trition</title>
    <!-- Meta tag Keywords -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="UTF-8" />
    <meta name="keywords" />

    <!--// Meta tag Keywords -->

    <!-- Custom-Files -->
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/fontawesome-free-5.13.1-web/css/all.css">
    <link rel="stylesheet" href="css/style.css" type="text/css" media="all" />
    <!-- Style-CSS -->
    <!-- //Custom-Files -->

    <!-- Web-Fonts -->
    <link
        href="//fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i&amp;subset=latin-ext"
        rel="stylesheet">
    <link
        href="//fonts.googleapis.com/css?family=Barlow+Semi+Condensed:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">
    <!-- //Web-Fonts -->
    <script src="js/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
    <!--include angular-->
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.0/angular.min.js"></script>
    <script src="./js/index.js"> </script>
    <script src="./js/common.js"> </script>
</head>
  <body>
     <div class="container-fluid px-0">
      <!--nav-->
         <nav class="navbar  navbar-expand-lg  navbar-light checkout-nav-bg">
            <a class="navbar-brand" href="index.php">
            <img src="images/logo_crop.jpg" alt="utrition logo" class="nav-logo"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
           <div class="collapse navbar-collapse" id="navbarNav">
             <ul class="navbar-nav ml-auto">
               <li class="nav-item">
                 <a class="nav-link" href="index.php">home</a>
               </li>
             </ul>
           </div>
         </nav>     
          <!--//nav-->
         <!--content-->
          <!--background-->
       <div class="content-recover-bg">  
         
         <div class="row no-gutters justify-content-center">
            <div class="col-sm-6">
               <form class="form-recover-bg">
               <h3 class="orange-txt text-center py-3 text-uppercase">recover password</h3> 
                 <div class="form-group">
                   <label for="recoverpassword">Password</label>
                   <input type="password" id="recoverpassword" class="form-control" placeholder="password">
                 </div> 
                 <div class="form-group d-flex justify-content-end">
                   <button type="submit" class="btn stand-btn ">save</button>
                 </div>
               </form>    
            </div>   
          </div> 
       </div>
          <!--//content-->  
     </div>
  </body>
</html>
