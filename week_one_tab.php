     
      <!--accordion days -->
       
      <div id="accordion-w1">
        <div class="card card-accordion">
          <div class="card-header d-flex" id="heading-w1-one">
            <h5 class="mr-auto mb-0">
              <button class="btn  day-tab text-uppercase  btn-link" data-toggle="collapse" data-target="#collapse-{{week.day.trim()}}" aria-expanded="true" aria-controls="w1-one">
               {{week.day}}  {{week.date}}
              </button>
            </h5>
            <h5 class="ml-auto">
              <!--edit modal-->
<!-- Button trigger modal -->
<!-- <button type="button" class="btn text-capitalize  stand-btn btn-accordion-edit" data-toggle="modal" data-target="#exampleModal">
edit <i class="fas fa-edit"></i></button>
</button> -->

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   <div class="modal-dialog">
     <div class="modal-content">
       <div class="modal-header">
         <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
         <span aria-hidden="true">&times;</span>
         </button>
       </div>
       <div class="modal-body">
         <div class="form-group">

           <label for="timeedit_w1">drop off</label>
           <input type="text" class="form-control mb-2" id="timeedit_w1" disabled placeholder="Example input placeholder">

           <label for="addressedit_w1">address</label>
           <input type="text" class="form-control mb-2" id="addressedit_w1" disabled placeholder="Example input placeholder">

         </div>
       </div>
      <div class="modal-footer">
         <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
         <button type="button" class="btn btn-on-car">Save changes</button>
      </div>
     </div>
     </div>
   </div>

              <!--//edit modal-->
            
</div>
      
          <div id="collapse-{{week.day.trim()}}" class="collapse " ng-class="{show: $index == 0}" aria-labelledby="heading-w1-one" data-parent="#accordion-{{key}}-{{week.day}}">
            <div class="card-body">
               <!--drop off  and adress-->
            <!-- <form>
              <div class="form-row align-items-end">
                <div class="form-group col-md-4"> 
                   <label for="day-dropoff">drop off</label>
                   <select id="day-dropoff" class="form-control">
                     <option selected>3:00pm</option>
                     <option>3:00pm</option>
                   </select>
                </div>
                <div class="form-group col-md-4"> 
                   <label for="address-dropoff">drop off address</label>
                   <select id="address-dropoff" class="form-control">
                     <option selected>first address</option>
                     <option>second address</option>
                   </select>
                </div> 
                <div class="form-group col-md-2"> 
                <button type="button" class="btn text-capitalize   stand-btn btn-accordion-edit" data-toggle="modal" data-target="#exampleModal">
                Update  <i class="fas fa-edit"></i></button>
                
                </button>
                </div>
              </div>
            </form>    -->
         <!--//drop off-->
       <!--w1-d1-->
       <div id="accordion-{{key}}-{{week.day}}">
        <div class="card text-uppercase">
          <div class="card-header d-flex" id="heading-{{key}}-{{week.day}}-breakfast">
          <h6 class="mr-auto green-txt">
           breakfast
          </h6>  
            <h5 class="ml-auto">
                <button class="btn py-0 btn-link collapsed" data-toggle="collapse" data-target="#collapse-{{key}}-{{week.day}}-breakfast" aria-expanded="false" aria-controls="collapseTwo">
                    more details
                    <i class="far fa-plus-square"></i>
              </button>
            </h5>
          </div>
          <div id="collapse-{{key}}-{{week.day}}-breakfast" class="collapse" aria-labelledby="heading-{{key}}-{{week.day}}-breakfast" data-parent="#accordion-{{key}}-{{week.day}}">
            <div class="card-body">
          
                <div class="row">
                  <!--<div class="col-sm-10">
                    <div class="form-group">
                     <textarea id="w3review" name="w3review" disabled class=" txtarea-edit">
                     </textarea>
                  </div>
                 </div>
                -->        
            <div class="col-sm-11">
                 {{week.arrMeal[0].breakfast}}
             
             
            </div>
             <!-- <div class="col-sm-1">
                <button type="button" class="btn  btn-accordion-edit">edit</button>                           
              </div> -->
            
            
            </div>
          </div>
          </div>
        </div>

        <div class="card text-uppercase">
          <div class="card-header d-flex" id="heading-w1-d1-lunch">
          <h6 class="mr-auto green-txt">
           lunch
          </h6>  
            <h5 class="ml-auto">
                <button class="btn py-0 btn-link collapsed" data-toggle="collapse" data-target="#collapse-{{key}}-{{week.day}}-lunch" aria-expanded="false" aria-controls="collapseTwo">
                    more details
                    <i class="far fa-plus-square"></i>
              </button>
            </h5>
          </div>
          <div id="collapse-{{key}}-{{week.day}}-lunch" class="collapse" aria-labelledby="heading-{{key}}-{{week.day}}-lunch" data-parent="#accordion-{{key}}-{{week.day}}">
            <div class="card-body">
            
              

            {{week.arrMeal[2].lunch}}
            </div>
          </div>
        </div>
        
        <div class="card text-uppercase">
          <div class="card-header d-flex" id="heading-w1-d1-snack">
            <h6 class="mr-auto green-txt">
              snack
            </h6>
            
            <h5 class="mb-l- auto">
              <button class="btn py-0 btn-link collapsed" data-toggle="collapse" data-target="#collapse-{{key}}-{{week.day}}-snack" aria-expanded="false" aria-controls="collapseThree">
                more details
                <i class="far fa-plus-square"></i>
              </button>
            </h5>
          </div>
          <div id="collapse-{{key}}-{{week.day}}-snack" class="collapse" aria-labelledby="heading-w1-d1-snack" data-parent="#accordion-{{key}}-{{week.day}}">
            <div class="card-body">
            
              <div class="row">
                <div class="col-sm-10">
                  <div class="form-group">
                   <!-- <textarea id="w3review" name="w3review" disabled class=" txtarea-edit">
                   </textarea> -->
                </div>
               </div>
                <!-- <div class="col-sm-1">
                 <button type="button" class="btn  btn-accordion-edit">edit</button>                           
               </div> -->
              </div>

              {{week.arrMeal[3].snack}}
            </div>
          </div>
        </div>

        <div class="card text-uppercase">
            <div class="card-header d-flex" id="heading-w1-d1-dinner">
            <h6 class="mr-auto green-txt">
             dinner
            </h6>  
              <h5 class="ml-auto">
                  <button class="btn py-0 btn-link collapsed" data-toggle="collapse" data-target="#collapse-{{key}}-{{week.day}}-dinner" aria-expanded="false" aria-controls="collapseFour">
                      more details
                      <i class="far fa-plus-square"></i>
                </button>
              </h5>
            </div>
            <div id="collapse-{{key}}-{{week.day}}-dinner" class="collapse" aria-labelledby="heading-{{key}}-{{week.day}}-dinner" data-parent="#accordion-{{key}}-{{week.day}}">
              <div class="card-body">
              
                <div class="row">
                  <div class="col-sm-10">
                    <div class="form-group">
                     <!-- <textarea id="w3review" name="w3review" disabled class=" txtarea-edit">
                     </textarea> -->
                  </div>
                 </div>
                  <!-- <div class="col-sm-1">
                   <button type="button" class="btn  btn-accordion-edit">edit</button>                           
                 </div> -->
                </div>

                {{week.arrMeal[1].dinner}}
           
              </div>
            </div>
          </div>
        
      </div>
       <!--//w1-d1-->
            </div>
          </div>
        </div>

       

       </div>
      
    <!--//accordion days -->
  