<!DOCTYPE html>
<html lang="en">

<head>
  <title>u trition</title>
  <!-- Meta tag Keywords -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta charset="UTF-8" />
  <meta name="keywords" />

  <!--// Meta tag Keywords -->

  <!-- Custom-Files -->
  <link rel="stylesheet" href="css/bootstrap.css">
  <link rel="stylesheet" href="css/fontawesome-free-5.13.1-web/css/all.css">
  <link rel="stylesheet" href="css/style.css" type="text/css" media="all" />
  <link rel="icon" type="image/png" sizes="96x96" href="images/favicon-32x32.png">
  <!-- Style-CSS -->
  <!-- //Custom-Files -->

  <!-- Web-Fonts -->
  <link href="//fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i&amp;subset=latin-ext"
    rel="stylesheet">
  <link
    href="//fonts.googleapis.com/css?family=Barlow+Semi+Condensed:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
    rel="stylesheet">
  <!-- //Web-Fonts -->
  <script src="js/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
    integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
    crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
    integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
    crossorigin="anonymous"></script>
  <!--include angular-->
  <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.0/angular.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.4.0/angular-animate.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/angularjs-toaster/3.0.0/toaster.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/angularjs-toaster/3.0.0/toaster.min.css" rel="stylesheet" />
  <script src="./js/index.js"> </script>
  <script src="./js/common.js"> </script>
</head>

<body ng-controller="myCtrl" ng-app="myApp">
  <div class="container-fluid px-0">
    <!--navbar-->

    <!-- top-header -->

    <nav id="home" class="navbar nav-txt-style hide-md navbar-expand-lg navbar-light header-bg">
      <span class=" navbar-text "><i class="fa orange-txt fa-map-marker mr-2"></i>UAE</span>
      <span class="navbar-text ml-3"><i class="fa orange-txt fa-phone mr-2 "></i> (+971) 526920873</span>
      <div class="ml-auto" id="navbarText">
        <a ng-if="User==undefined" href="log_reg.php" class="  btn login-button-2 stand-btn mr-3 text-uppercase">
          <span class="fa fa-sign-in  mr-2"></span>Login / register</a>
        <span ng-if="User!=undefined" class="navbar-text text-capitalize mr-2">
          Hello, {{User.email}}
        </span>
        <span ng-if="User!=undefined"><a ng-click="logout()">logout</a></span>
        <!-- <span class="navbar-text text-capitalize mr-2">
                    follow us :
                </span>
                <span class="fa-cont">
                    <span class="s"> <i class="fab fa-twitter mr-2"></i></span>
                    <i class="fab fa-facebook-f mr-2"></i>
                </span> -->
      </div>
    </nav>
    <!-- //top-header -->

    <!--navbar-->
    <nav class="navbar navbar-expand-lg navbar-light nav-bg">
      <a class="navbar-brand" href="index.php">
        <img src="images/logo_crop.jpg" alt="utrition logo" class="nav-logo"></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02"
        aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
        <ul class="navbar-nav ml-auto  mt-2 mt-lg-0">
          <li class="nav-item "><a class="nav-link" href="index.php#meal_plans">meal plans</a></li>
          <li class="nav-item "><a class="nav-link" href="index.php#about">About Us</a></li>
          <li class="nav-item "><a class="nav-link" ng-if="User!=undefined" href="manage_plan.php">my meal plan</a></li>
          <li class="nav-item "><a class="nav-link" href="gallery.php">gallery</a></li>
          <li class="nav-item active"><a class="nav-link" href="faq.php">faq's</a></li>
          <li class="nav-item "><a class="nav-link" href="contact.php">contact</a></li>
          <li class="nav-item d-lg-none d-xl-none"><a class="nav-link" ng-if="User==undefined"
              href="log_reg.html">logIn/Register</a>
          </li>
        </ul>
      </div>
    </nav>
    <!--//navbar-->

    <!--//navbar-->
    <!--banner-->
    <div class="bg-banner-faq">
      <div class="row  p-0">

        <!--<img src="images/inner-bg.jpg" alt="healthy-food" class="main-banner-2 img-responsive">-->
        <!-- <img src="images/banner-2-test6.jpg" alt="plan img" class=" main-banner-2 img-responsive">-->
        <div class="overlay-on-img-manage">
          <p class="topText green-txt">FAQ's</p>
        </div>
      </div>

    </div>

    <!--//banner-->
    <!--crumb-->
    <div class="row no-gutters">
      <div class=" px-0 col-sm-12">
        <div class="breadcrumb-agile bg-light py-2">
          <ol class="breadcrumb bg-light m-0">
            <li class="breadcrumb-item">
              <a href="index.html">Home</a>
            </li>
            <li class="breadcrumb-item active" aria-current="page">Faq's</li>
          </ol>
        </div>
      </div>
    </div>
    <!--//crumb-->
    <div class="row mt-5 no-gutters justify-content-center">
      <div class="col-sm-10 ">
        <h4 class="orange-txt mb-2 text-uppercase">signing-in</h4>
        <div class="accordion" id="accordionfaq-first">
          <div class="card">
            <div class="card-header header-faq" id="headingOne-first">
              <h2 class="mb-0">
                <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse"
                  data-target="#collapseOne-first" aria-expanded="false" aria-controls="collapseOne">
                  Why do I need to sign up?
                </button>
              </h2>
            </div>

            <div id="collapseOne-first" class="collapse " aria-labelledby="headingOne-first"
              data-parent="#accordionfaq-first">
              <div class="card-body">
                <p> We need your details to serve you better & for below </p>
                <ul>
                  <li>
                    <p>For Suggesting the Right Meal Plan that really caters to you based on your health conditions,
                      Intolerances and to help you to achieve your goals.
                    </p>
                  </li>
                  <li>
                    <p>We also need your contact details to organize delivery of your meals at a time & place that suits
                      you

                    </p>
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <div class="card">
            <div class="card-header " id="headingTwo-first">
              <h2 class="mb-0">
                <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse"
                  data-target="#collapseTwo-first" aria-expanded="false" aria-controls="collapseTwo">
                  Where do I sign up? 
                </button>
              </h2>
            </div>
            <div id="collapseTwo-first" class="collapse" aria-labelledby="headingTwo-first"
              data-parent="#accordionfaq-first">
              <div class="card-body">
                <p>You can sign up through below options :</p>
                <ul>
                  <li>
                    <p>At any of the Golds Gym in Dubai
                    </p>
                  </li>
                  <li>
                    <p>Through our website; Utritionlife.com

                    </p>
                  </li>
                  <li>
                    <p>You can also call +971 526920873 for support from a member of the UTRITION team.​</p>
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <div class="card">
            <div class="card-header header-faq" id="headingThree-first">
              <h2 class="mb-0">
                <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse"
                  data-target="#collapseThree-first" aria-expanded="false" aria-controls="collapseThree">
                  When can I start? 
                </button>
              </h2>
            </div>
            <div id="collapseThree-first" class="collapse" aria-labelledby="headingThree-first"
              data-parent="#accordionfaq-first">
              <div class="card-body">
                <p>After filling the registration form and on successful payment we can start your plan in as little as
                  two business days. ​</p>

              </div>
            </div>
          </div>

          <div class="card">
            <div class="card-header " id="headingFour-first">
              <h2 class="mb-0">
                <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse"
                  data-target="#collapseFour-first" aria-expanded="false" aria-controls="collapseFour-first">
                  What is the duration of each plan? 
                </button>
              </h2>
            </div>
            <div id="collapseFour-first" class="collapse" aria-labelledby="headingFour-first"
              data-parent="#accordionfaq-first">
              <div class="card-body">
                <p>When you sign up with Utrition, you can choose from the following options of duration and meal
                  delivery frequency:</p> ​
                <ul>
                  <li>
                    <p>20 days (5 days a week delivery)</p>
                  </li>
                  <li>
                    <p>To enjoy and save more, you can also sign up for 40 Days or 60 Days.</p>
                  </li>

                </ul>
                ​
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>
 
  <div class="row no-gutters justify-content-center ">
    <div class="col-sm-10  ">
      <h4 class="orange-txt mb-2  mt-5 text-uppercase">delivery</h4>

      <div class="accordion" id="accordionfaq-two">
        <div class="card">
          <div class="card-header header-faq" id="headingOne-second">
            <h2 class="mb-0">
              <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse"
                data-target="#collapseOne-second" aria-expanded="false" aria-controls="collapseOne-second">
                Which areas do you deliver to? 
              </button>
            </h2>
          </div>

          <div id="collapseOne-second" class="collapse " aria-labelledby="headingOne-second"
            data-parent="#accordionfaq-two">
            <div class="card-body">
              <p>We deliver 6 days a week except Friday throughout Dubai with no cost and based on client requirement we
                can deliver on Fridays, but it will be charged extra based on location & timing.​</p>
            </div>
          </div>
        </div>
        <div class="card">
          <div class="card-header" id="headingTwo-second">
            <h2 class="mb-0">
              <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse"
                data-target="#collapseTwo-second" aria-expanded="false" aria-controls="collapseTwo-second">
                What time do you deliver the food? *
              </button>
            </h2>
          </div>
          <div id="collapseTwo-second" class="collapse" aria-labelledby="headingTwo-second"
            data-parent="#accordionfaq-two">
            <div class="card-body">
              <p>Deliveries are daily. Our morning slot is between 4am and 8am and our evening slot is between 4pm and
                10pm. Delivery schedules depend on your specific location but in the event that you’re not available to
                receive it, we can leave it on your doorstep or deliver to an alternative location.​ </p>
            </div>
          </div>
        </div>
        <div class="card">
          <div class="card-header header-faq" id="headingThree-second">
            <h2 class="mb-0">
              <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse"
                data-target="#collapseThree-second" aria-expanded="false" aria-controls="collapseThree-second">
                Can it be delivered to my office? 
              </button>
            </h2>
          </div>
          <div id="collapseThree-second" class="collapse" aria-labelledby="headingThree-second"
            data-parent="#accordionfaq-two">
            <div class="card-body">
              <p>Yes, we can during the morning delivery slot or at evening delivery slot based on your choice and
                availability of delivery slot.</p>
            </div>
          </div>
        </div>




      </div>
    </div>
  </div>


   <div class="row no-gutters justify-content-center ">
    <div class="col-sm-10  ">
      <h4 class="orange-txt mb-2  mt-5 text-uppercase">Consultation</h4>

      <div class="accordion" id="accordionfaq-three">
        <div class="card">
          <div class="card-header header-faq" id="headingOne-third">
            <h2 class="mb-0">
              <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse"
                data-target="#collapseOne-third" aria-expanded="false" aria-controls="collapseOne-third">
                When and how often should I have a weigh-in?
              </button>
            </h2>
          </div>

          <div id="collapseOne-third" class="collapse " aria-labelledby="headingOne-third"
            data-parent="#accordionfaq-three">
            <div class="card-body">
              <p>We recommend you weigh yourself before starting the meal plan program and once a week at a consistent time (preferably in the morning) to avoid any normal daily weight fluctuations that commonly occur.​​</p>
            </div>
          </div>
        </div>
        <div class="card">
          <div class="card-header" id="headingTwo-third">
            <h2 class="mb-0">
              <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse"
                data-target="#collapseTwo-third" aria-expanded="false" aria-controls="collapseTwo-third">
                Why Should I Consult with a dietitian? 
              </button>
            </h2>
          </div>
          <div id="collapseTwo-third" class="collapse" aria-labelledby="headingTwo-third"
            data-parent="#accordionfaq-three">
            <div class="card-body">
              <p>It is recommended to meet with our dietitian, in order to learn about your food preferences, eating habits and lifestyle. We will also provide the In-Body analysis which provides us with a complete review of your current weight, body composition and metabolism. This analysis helps our Utrition dietitian provide you with a fair assessment of what your goals should be and how your diet should be structured in order to achieve your goals.​</p>
            </div>
          </div>
        </div>
        <div class="card">
          <div class="card-header header-faq" id="headingThree-third">
            <h2 class="mb-0">
              <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse"
                data-target="#collapseThree-third" aria-expanded="false" aria-controls="collapseThree-third">
                What if I don’t want to see a dietitian? 
              </button>
            </h2>
          </div>
          <div id="collapseThree-third" class="collapse" aria-labelledby="headingThree-third"
            data-parent="#accordionfaq-three">
            <div class="card-body">
              <p>If you don’t wish to meet with our dietitian, simply select one of our meal plans and continue with the sign-up process. You will be asked for your food intolerances, food allergies, like and dislikes when completing the sign-up form. You can always call us to clarify any concerns you may have. </p>
            </div>
          </div>
        </div>




      </div>
    </div>
  </div>

  <div class="row no-gutters justify-content-center ">
    <div class="col-sm-10  ">
      <h4 class="orange-txt mb-2  mt-5 text-uppercase">Meal Plans</h4>

      <div class="accordion" id="accordionfaq-four">
        <div class="card">
          <div class="card-header header-faq" id="headingOne-fourth">
            <h2 class="mb-0">
              <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse"
                data-target="#collapseOne-fourth" aria-expanded="false" aria-controls="collapseOne-fourth">
                What meal plans does Utrition offer? 
              </button>
            </h2>
          </div>

          <div id="collapseOne-fourth" class="collapse" aria-labelledby="headingOne-fourth"
            data-parent="#accordionfaq-four">
            <div class="card-body">
              <p>We offer three different meal plans, all our meals are organic, gluten-free, balanced and designed for specific goals including weight loss, weight and nutrition maintenance and muscle gain and recovery. Our Utrition nutritionists are on hand to ensure you select the right plan for you based on your optimal daily calorie intake, age, height, current weight, gender, food intolerances, food allergies, and daily activity level.</p>
            </div>
          </div>
        </div>
        <div class="card">
          <div class="card-header" id="headingTwo-fourth">
            <h2 class="mb-0">
              <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse"
                data-target="#collapseTwo-fourth" aria-expanded="false" aria-controls="collapseTwo-fourth">
                How much weight can I expect to lose? 
              </button>
            </h2>
          </div>
          <div id="collapseTwo-fourth" class="collapse" aria-labelledby="headingTwo-fourth"
            data-parent="#accordionfaq-four">
            <div class="card-body">
              <p>Losing weight depends on various factors including your age, daily activity, exercise, medical conditions and weight before starting our meal plans. If followed as per the recommend advise form our dietitian, you may lose anything from 4 to 9 kilos per month. Your results all depend on you and your commitment, we are here to help you active that. </p>
            </div>
          </div>
        </div>
        <div class="card">
          <div class="card-header header-faq" id="headingThree-fourth">
            <h2 class="mb-0">
              <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse"
                data-target="#collapseThree-fourth" aria-expanded="false" aria-controls="collapseThree-fourth">
                Do you offer plans less then one month?  
              </button>
            </h2>
          </div>
          <div id="collapseThree-fourth" class="collapse" aria-labelledby="headingThree-fourth"
            data-parent="#accordionfaq-four">
            <div class="card-body">
              <p>In order to change and form healthy habits, Utritrion organic plans are carefully calculated and balanced for a minimum 4-week period. If you wish to try our organic meals for one week, please contact our sales team and they can assist you on a one-week plan. Prices will vary and results are less achievable for any plans less then 4-weeks.  ​</p>
            </div>
          </div>
        </div>

        <div class="card">
          <div class="card-header header-faq" id="headingFour-fourth">
            <h2 class="mb-0">
              <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse"
                data-target="#collapseFour-fourth" aria-expanded="false" aria-controls="collapseFour-fourth">
                What if I do not want 3 meals per day?
              </button>
            </h2>
          </div>
          <div id="collapseFour-fourth" class="collapse" aria-labelledby="headingFour-fourth"
            data-parent="#accordionfaq-four">
            <div class="card-body">
              <p>Utrition menus are carefully designed to ensure you enjoy the optimum quantity of carbs, protein, fat, calories, nutrients and vitamins throughout the day. If you would like to exclude some of the meals, please get in touch and we can discuss options.​</p>
            </div>
          </div>
        </div>

        <div class="card">
          <div class="card-header header-faq" id="headingFive-fourth">
            <h2 class="mb-0">
              <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse"
                data-target="#collapseFive-fourth" aria-expanded="false" aria-controls="collapseFive-fourth">
                I’m not interested in losing weight, is there an appropriate plan for me?
              </button>
            </h2>
          </div>
          <div id="collapseFive-fourth" class="collapse" aria-labelledby="headingFive-fourth"
            data-parent="#accordionfaq-four">
            <div class="card-body">
              <p>We have different plans to choose from depending on your goals and preferences. Whether for weight loss, weight maintenance or weight gain, we have a plan to suits you!</p>
            </div>
          </div>
        </div>


        <div class="card">
          <div class="card-header header-faq" id="headingSix-fourth">
            <h2 class="mb-0">
              <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse"
                data-target="#collapseSix-fourth" aria-expanded="false" aria-controls="collapseSix-fourth">
                I want to maintain my weight; do you have a plan for me?​
              </button>
            </h2>
          </div>
          <div id="collapseSix-fourth" class="collapse" aria-labelledby="headingSix-fourth"
            data-parent="#accordionfaq-four">
            <div class="card-body">
              <p>Absolutely! We have two plans to choose from Lean Gluten Free and Lean Paleo. Both of which have a daily calorie intake ranges between 1400 and 1800, which is just enough to maintain a healthy lifestyle.</p>
            </div>
          </div>
        </div>


        <div class="card">
          <div class="card-header header-faq" id="headingSeven-fourth">
            <h2 class="mb-0">
              <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse"
                data-target="#collapseSeven-fourth" aria-expanded="false" aria-controls="collapseSeven-fourth">
                What if I do heavy exercise/training and want to gain muscle? ​
              </button>
            </h2>
          </div>
          <div id="collapseSeven-fourth" class="collapse" aria-labelledby="headingSeven-fourth"
            data-parent="#accordionfaq-four">
            <div class="card-body">
              <p>Our "Athlete Paleo" plan is the best plan for you. This plan will help you; stimulate recuperation, absorb nutrients and give you a higher immune function.  With a daily calorie intake that ranges from 1800 – 2300, you’ll have enough energy to juice up your training. </p>
            </div>
          </div>
        </div>

        <div class="card">
          <div class="card-header header-faq" id="headingEight-fourth">
            <h2 class="mb-0">
              <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse"
                data-target="#collapseEight-fourth" aria-expanded="false" aria-controls="collapseEight-fourth">
                How Many meals will I receive? 
              </button>
            </h2>
          </div>
          <div id="collapseEight-fourth" class="collapse" aria-labelledby="headingEight-fourth"
            data-parent="#accordionfaq-four">
            <div class="card-body">
              <p>You will receive 3 meals and 1 snack. Breakfast, Lunch, Dinner & 1 snack.​</p>
            </div>
          </div>
        </div>



        <div class="card">
          <div class="card-header header-faq" id="headingNine-fourth">
            <h2 class="mb-0">
              <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse"
                data-target="#collapseNine-fourth" aria-expanded="false" aria-controls="collapseNine-fourth">
                What does Paleo mean? ​ 
              </button>
            </h2>
          </div>
          <div id="collapseNine-fourth" class="collapse" aria-labelledby="headingNine-fourth"
            data-parent="#accordionfaq-four">
            <div class="card-body">
              <p>Paleo attempts to model what would have been eaten by humans during the Paleolithic era, also known as the prehistoric stone age of human development. This diet includes meats, fish, fruits, vegetables, nuts and seeds. foods that presumably would have been obtained through hunting and gathering. It omits legumes, grains, dairy, refined sugar, low–calorie sweeteners and “processed” foods. ​</p>
            </div>
          </div>
        </div>




      </div>
    </div>
  </div>




  <div class="row no-gutters justify-content-center ">
    <div class="col-sm-10  ">
      <h4 class="orange-txt mb-2  mt-5 text-uppercase">Payments & Discounts​</h4>

      <div class="accordion" id="accordionfaq-five">
        <div class="card">
          <div class="card-header header-faq" id="headingOne-fifth">
            <h2 class="mb-0">
              <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse"
                data-target="#collapseOne-fifth" aria-expanded="false" aria-controls="collapseOne-fifth">
                How much will my plan cost? 
              </button>
            </h2>
          </div>

          <div id="collapseOne-fifth" class="collapse" aria-labelledby="headingOne-fifth"
            data-parent="#accordionfaq-five">
            <div class="card-body">
              <p>Utrition Plans start from AED 3150, All our plans are tailored based on client intolerances, health condition, allergies and set goals so plan cost will depend on calorie count & Intolerances.​

Currently we are offering 10-15% discount for Golds Gym Members based on membership and for more offers and discount contact Utrition support team @ +971 526920873</p>
            </div>
          </div>
        </div>
        <div class="card">
          <div class="card-header" id="headingTwo-fifth">
            <h2 class="mb-0">
              <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse"
                data-target="#collapseTwo-fifth" aria-expanded="false" aria-controls="collapseTwo-fifth">
                Why do I need to pay an AED 200 deposit for the ice and chiller bag? ​
              </button>
            </h2>
          </div>
          <div id="collapseTwo-fifth" class="collapse" aria-labelledby="headingTwo-fifth"
            data-parent="#accordionfaq-five">
            <div class="card-body">
              <p>We ask for AED 200 as a refundable deposit for a Utrition a chiller bag as experience has shown that our customers love our bags so much, that they’re often reluctant to return them!​</p>
            </div>
          </div>
        </div>
        <div class="card">
          <div class="card-header header-faq" id="headingThree-fifth">
            <h2 class="mb-0">
              <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse"
                data-target="#collapseThree-fifth" aria-expanded="false" aria-controls="collapseThree-fifth">
                How can I pay?
              </button>
            </h2>
          </div>
          <div id="collapseThree-fifth" class="collapse" aria-labelledby="headingThree-fifth"
            data-parent="#accordionfaq-five">
            <div class="card-body">
              <p>We accept cash and cards throughout all the Golds Gym in the UAE. You can pay online with your credit or debit card through New Age Fitness Online Portal or if you prefer, you can drop off payment at our head office in Dubai Outlet Mall. ​</p>
            </div>
          </div>
        </div>

        <div class="card">
          <div class="card-header header-faq" id="headingFour-fifth">
            <h2 class="mb-0">
              <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse"
                data-target="#collapseFour-fifth" aria-expanded="false" aria-controls="collapseFour-fifth">
                Do you offer discounts? 
              </button>
            </h2>
          </div>
          <div id="collapseFour-fifth" class="collapse" aria-labelledby="headingFour-fifth"
            data-parent="#accordionfaq-five">
            <div class="card-body">
              <p>We do have several discounts on offer (Terms and Conditions apply): ​</p>
              <ul>
                 <li>
                 <p>We offer 10-15% discount for Gold Gym Members based on membership type.​</p>
                 </li>   

                 <li>
                 <p>Fit Family (10% off); for families to enjoy the plan together​</p>
                 </li>   
                 <li>
                 <p>Refer a friend (one day Detox Plan for each referral upon renewal): as a thank you for spreading the word about Utrition​​</p>
                 </li>   
                 <li>
                 <p>Partner in health (10% off for your first month): for companies partnered with Utrition (contact your company HR for more information or if you would like your company to be one of our partners in health).</p>
                 </li>   
              </ul>  
              <p ><b>For more customized offers reach our ​

support team @ +971 526920873.​</b></p>
                   







            </div>
          </div>
        </div>

    


      



   


     



      </div>
    </div>
  </div>
  

  <!--footer-->
  <?php include 'footer.php';?>
  <!--//footer-->
  <!-- move top icon -->
  <a href="#home" class="move-top text-center">
    <span class="fas fa-level-up-alt" aria-hidden="true"></span>
  </a>
  <!-- //move top icon -->
  <a  href="contact.php" class="nav-link mt-2 btn help-fly-btn px-1  ">
        <span  class="fa fa-sign-in  mr-2"></span>need help?</a> 
  </div>
</body>

</html>