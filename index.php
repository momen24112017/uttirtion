<!DOCTYPE html>
<html lang="en">

<head>
    <title>u trition</title>
    <!-- Meta tag Keywords -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="UTF-8" />
    <meta name="keywords" />

    <!--// Meta tag Keywords -->

    <!-- Custom-Files -->
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/fontawesome-free-5.13.1-web/css/all.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" />
    <link rel="stylesheet" href="css/style.css" type="text/css" media="all" />
    <link rel="icon" type="image/png" sizes="96x96" href="images/favicon-32x32.png">
    <!-- Style-CSS -->
    <!-- //Custom-Files -->
    <!--include on scroll -->
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <!-- Web-Fonts -->
    <link
        href="//fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i&amp;subset=latin-ext"
        rel="stylesheet">
    <link
        href="//fonts.googleapis.com/css?family=Barlow+Semi+Condensed:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">
    <!-- //Web-Fonts -->
    <script src="js/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
    <!--include angular-->
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.0/angular.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.4.0/angular-animate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angularjs-toaster/3.0.0/toaster.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/angularjs-toaster/3.0.0/toaster.min.css" rel="stylesheet" />
    <script src="./js/index.js"> </script>
    <script src="./js/common.js"> </script>
</head>

<body ng-controller="myCtrl" ng-app="myApp">
    <div class="container-fluid px-0">
        <!--navbar-->
        <?php include 'navbar.php';?>
        <!--//navbar-->
        <!--nw carousel -->
        <div id="carouselExampleIndicators" class="carousel slide carousel-fade " data-ride="carousel">
            <ol class="carousel-indicators intro-car-indicator">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner " id="home">

                <div class="carousel-item car-item-style car-1-slide-bg active ">

                    <div class="overlay-on-img abs">

                        <p class="topText white-txt animate__animated animate__fadeInUp">Wide Variety of Meals and
                            Snacks.</p>
                        <a href="#meal_plans">
                            <button type="button" class="btn mt-5 white-txt top-btn btn-on-car">Select your Plan<span
                                    class="fa fa-caret-right ml-1" aria-hidden="true"></span></button>
                        </a>
                    </div>

                </div>
                <div class="carousel-item car-item-style  car-2-slide-bg">

                    <div class="overlay-on-img-sec abs">

                        <p class="topText  white-txt animate__animated animate__fadeInUp">Fresh and Organic.</p>
                        <a href="#meal_plans">
                            <button type="button" class="btn mt-5 white-txt top-btn btn-on-car">Select your Plan<span
                                    class="fa fa-caret-right ml-1" aria-hidden="true"></span></button>
                        </a>
                    </div>

                </div>
                <div class="carousel-item car-item-style  car-3-slide-bg">

                    <div class="overlay-on-img-third abs">

                        <p class="topText white-txt animate__animated animate__fadeInUp">Daily Delivered to your
                            Doorstep.</p>
                        <a href="#meal_plans">
                            <button type="button" class="btn mt-5 white-txt top-btn btn-on-car">Select your Plan<span
                                    class="fa fa-caret-right ml-1" aria-hidden="true"></span></button>
                        </a>
                    </div>
                </div>
            </div>

        </div>
        <!--//nw carousel-->

        <!--about-->
        <div class="row no-gutters  my-5 justify-content-center" id="about">
            <div class="col-xl-2 col-lg-2 ">

                <div class=" text-center mr-3">
                    <img alt="our ethics" src="images/new-ethos.png" class="ethics-title">

                </div>
            </div>
            <div class="col-xl-6 col-lg-6  ">
                <p class="px-2">Here at Utrition we believe in the goals of our clients.
                    We are here to accompany our clients on their journey to success in both training and nutrition.
                    Our meal plans are designed to integrate with your training program seamlessly, be it our
                    accuracy on your required macronutrients, reducing inflammation or support your muscle
                    recuperation. Based on your body analysis, our nutritionist & master chef work together to
                    design a complete meal plan that you can indulge in and meet your wellness goals. Setting goals
                    is a start but achieving goals is the real win.
                </p>

            </div>
        </div>
    </div>
    <!--//about-->
    <!--fresh -->
    <div class=" bg-fresh car-item-style">
        <div class="fresh-bg">
            <div class="row justify-content-center no-gutters py-5">
                <div class="car-item-style w-100">
                    <img src="images/plate-only-sm.png" class="rotating plate-rot-img" alt="plate">

                    <div class="col-xl-12 col-lg-12  ">
                        <div class=" text-center ">
                            <h1 class="main-title mb-2 green-txt">
                                <span class="text-uppercase "> fresh organic</span> <span class="orange-txt secfont">
                                </span><span class="text-uppercase orange-txt ">delivered</span>
                            </h1>
                        </div>
                    </div>

                    <div class="text-center col-sm-12   mt-4">
                        <div class="ser-wrap">
                            <p class="mb-2 intro-sm-txt ds-block "><i class="fab fa-envira"></i><strong
                                    class="green-txt mr-1">Freshly</strong></p>
                            <p class="white-txt"> sourced ingredients cooked by our top executive chef
                                ​
                            </p>
                        </div>
                        <div class="ser-wrap">
                            <p class="mb-2 intro-sm-txt ds-block "><i class="fab fa-envira"></i><strong
                                    class="green-txt mr-1">Organic</strong></p>
                            <p class="white-txt"> sourcing, Macro-Balanced, Low-Carb and vegan
                                options.
                            </p>
                        </div>
                        <div class="ser-wrap">
                            <p class="mb-2 intro-sm-txt ds-block"><i class="fas fa-truck"></i></i><strong
                                    class="green-txt mr-1">Delivered</strong></p>
                            <p class="white-txt"> to your doorstep daily with your health and safety
                                at heart.
                            </p>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    </div>

    <!--//fresh-->

    <!--works -->

    <div class="row justify-content-center no-gutters">
        <div class="col-xl-8 col-lg-8 ">
            <div class=" text-center my-5">
                <h1 class="main-title mb-2 green-txt ">
                    <span class="text-uppercase">how it</span> <span class="orange-txt text-uppercase"> works </span>
                </h1>
            </div>
        </div>
    </div>
    <!--works test-->
    <div class="row  no-gutters justify-content-center">
        <div class="col-sm-6 ">
            <div class="text-center  mt-4">
                <div class="wrap-stylecard mb-4">
                    <img data-aos="fade-right" data-aos-duration="1000" data-aos-easing="ease-in-out"
                        data-aos-offset="200" class="works-img" src="images/394960-PCH23B-421-sm.jpg"
                        alt="know your body">
                    <div data-aos="fade" data-aos-duration="1000" data-aos-easing="ease-in-out" data-aos-offset="200"
                        class="style-cardbody ml-3 my-auto">
                        <h5 class="mb-3"><i class="fab fa-envira"></i><strong class="green-txt ">Know your body</strong>
                        </h5>
                        <p class="mb-3 intro-sm-txt">Our Life Fitness Specialist will guide you to understand your body
                            type
                            with our Body Composition Analysis. This will enable our Dietitian to insure you will select
                            the
                            right plan and help manage your progress.
                        </p>
                    </div>
                </div>
                <div class="wrap-stylecard mb-4">

                    <div data-aos="fade" data-aos-duration="1000" data-aos-easing="ease-in-out" data-aos-offset="200"
                        class="style-cardbody ml-3 my-auto">
                        <h5 class="mb-3"><i class="fab fa-envira"></i><strong class="green-txt ">Select your
                                plan</strong>
                        </h5>
                        <p class="mb-3 intro-sm-txt">Our Specialists will recommend the meal plan as per your calorie
                            and macronutrients
                            requirements, Food dislikes/allergies and your special instructions.
                        </p>

                    </div>
                    <img data-aos="fade-left" data-aos-duration="1000" data-aos-easing="ease-in-out"
                        data-aos-offset="200" class="works-img" src="images/526321-PIYHQ6-734-sm.jpg"
                        alt="know your body">
                </div>
                <div class="wrap-stylecard mb-4">
                    <img data-aos="fade-right" data-aos-duration="1000" data-aos-easing="ease-in-out"
                        data-aos-offset="200" class="works-img" src="images/27980-sm.jpg" alt="know your body">
                    <div data-aos="fade" data-aos-duration="1000" data-aos-easing="ease-in-out" data-aos-offset="200"
                        class="style-cardbody ml-3 my-auto">
                        <h5 class="mb-3"><i class="fab fa-envira"></i><strong class="green-txt ">Enjoy your
                                results</strong>
                        </h5>
                        <p class="mb-3 intro-sm-txt">We only use the freshest, highest quality ingredients cooked by our
                            top executive chef.<br /> You will receive
                            constant support throughout the entire period of the plan from your designated Utrition
                            account
                            manager.
                        </p>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!--//works test-->

    <!--plans-->
    <div class="index-plan-bg mt-5">

        <!--meal plans title-->
        <div class="row no-gutters justify-content-center">
            <div class="col-xl-8 col-lg-8 ">
                <div class=" text-center mt-5">
                    <h1 id="meal_plans" class="main-title mb-2 green-txt">
                        <span class=" orange-txt  text-uppercase ">Our </span><span class=" text-uppercase ">meal
                            plans</span>
                    </h1>
                </div>
            </div>
        </div>

        <!--//meal plans title-->

        <div class="row py-5 no-gutters justify-content-center ">

            <!--spinner-->
            <div class="spin-wrap white-txt" ng-show="arrPlan.length==0">
                <div class="spinner-border " role="status">
                </div>
                <span class="pl-3">
                    <h3>Loading...</h3>
                </span>
            </div>

            <!--//spinner-->

            <div class="col-lg-3 mx-2  col-md-4   " ng-repeat="plan in arrPlan ">
                <div class="card text-center mx-auto index-card  ">
                    <a href="plan_detail.php">
                        <div style="overflow:hidden">
                            <img class="card-img-bottom img-responsive" src="{{plan.feature_img}}" alt="Card image cap">
                        </div>
                    </a>

                    <div class="card-body index-c-body ">
                        <div class="wrap-card-txt">
                            <h5 class=" card-title m-0"><a href="plan_detail.php"
                                    ng-style="{ 'color': (plan.color) }">{{plan.name}}</a></h5>

                            <div class="plan-menu-list">
                                <ul class="list-wrap-index-card">
                                    <li  ng-repeat="Feature in plan.feature">
                                      <span>  {{Feature}}</span></li>

                                </ul>
                            </div>
                          
                            <h3 class="orange-txt my-3"><sub>from</sub><sub class="price-unit"> AED</sub> <span>{{plan.price_per_day}}
                                </span><sub>/ Day</sub></h3>

                            <a ng-click="getPlanDetail(plan)" class="btn py-2 btn-card">

                                <span class="white-txt"> View Plan</span>


                            </a>
                        </div>

                    </div>

                </div>
            </div>
        </div>

    </div>


    <!--  //plans-->

    <!--fans-->

    <!--motive-p-->
    <div class="row no-gutters justify-content-center">
        <div class="col-xl-8 col-lg-8 ">
            <div class=" text-center mt-5">
                <h1 id="meal_plans" class="main-title mb-2 green-txt">
                    <span class="text-uppercase">success </span><span class="orange-txt text-uppercase">stories</span>
                </h1>

            </div>
        </div>
    </div>


    </div>

    <div class="row  d-flex no-gutters  pb-5 justify-content-center ">
        <div class="col-sm-4 ">
            <div id="feedBackCar" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <div class="card text-center fans-card mx-auto index-card  ">
                            <a href="plan_detail.html">
                                <img class="card-img-bottom img-responsive" src="images/601053-PMYTRV-353-sm.jpg"
                                    alt="Card assets/image cap">

                            </a>

                            <div class="card-body index-c-body ">
                                <div class="wrap-card-txt">
                                    <h5 class=" card-title m-0"><a href="plan_detail.php">first member</a></h5>
                                    <p>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                    </p>
                                    <p class="mt-3  short-des">Our philosophy of “join one, join them all” gives you
                                        utmost accessibility and sheer convenience. Choose the package that suits your
                                        lifestyle and let the adventure begin. </p>

                                </div>

                            </div>

                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="card text-center mx-auto index-card fans-card ">
                            <a href="plan_detail.html">
                                <img class="card-img-bottom img-responsive" src="images/user-2-edit.jpg"
                                    alt="Card assets/image cap">

                            </a>

                            <div class="card-body index-c-body ">
                                <div class="wrap-card-txt">
                                    <h5 class=" card-title m-0"><a href="plan_detail.php">second member</a></h5>
                                    <p>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                    </p>
                                    <p class="mt-3  short-des">Our philosophy of “join one, join them all” gives you
                                        utmost accessibility and sheer convenience. Choose the package that suits your
                                        lifestyle and let the adventure begin. </p>

                                </div>

                            </div>

                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="card text-center mx-auto  index-card  fans-card">
                            <a href="plan_detail.html">
                                <img class="card-img-bottom img-responsive" src="images/user-2-edit.jpg"
                                    alt="Card assets/image cap">

                            </a>

                            <div class="card-body index-c-body ">
                                <div class="wrap-card-txt">
                                    <h5 class=" card-title m-0"><a href="plan_detail.php">third member</a></h5>
                                    <p>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                    </p>
                                    <p class="mt-3  short-des">Our philosophy of “join one, join them all” gives you
                                        utmost accessibility and sheer convenience. Choose the package that suits your
                                        lifestyle and let the adventure begin. </p>

                                </div>

                            </div>

                        </div>
                    </div>
                </div>
                <a class="carousel-control-prev" href="#feedBackCar" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#feedBackCar" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>


    </div>

    <!--motive p-->
    <!--//fans-->

    <!--footer-->
    <?php include 'footer.php';?>
    <!--//footer-->
    <!-- move top icon -->
    <a href=" index.php#home" class="move-top text-center">
        <span class="fas fa-level-up-alt" aria-hidden="true"></span>
    </a>
    <a href="contact.php" class="nav-link mt-2 btn help-fly-btn px-1  ">
        <span class="fa fa-sign-in  mr-2"></span>need help?</a>
    <!-- //move top icon -->



    </div>
    <script>
        AOS.init();
    </script>
</body>

</html>