<!DOCTYPE html>
<html lang="en">

<head>
    <title>u trition</title>
    <!-- Meta tag Keywords -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="UTF-8" />
    <meta name="keywords" />

    <!--// Meta tag Keywords -->

    <!-- Custom-Files -->
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/fontawesome-free-5.13.1-web/css/all.css">
    <link rel="stylesheet" href="css/style.css" type="text/css" media="all" />
    <!-- Style-CSS -->
    <!-- //Custom-Files -->

    <!-- Web-Fonts -->
    <link
        href="//fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i&amp;subset=latin-ext"
        rel="stylesheet">
    <link
        href="//fonts.googleapis.com/css?family=Barlow+Semi+Condensed:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">
    <!-- //Web-Fonts -->
    <script src="js/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
    <!--include angular-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/angularjs-toaster/3.0.0/toaster.min.css" rel="stylesheet" />
    <link rel="icon" type="image/png" sizes="96x96" href="images/favicon-32x32.png">

    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.0/angular.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.4.0/angular-animate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angularjs-toaster/3.0.0/toaster.min.js"></script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/angularjs-toaster/3.0.0/toaster.min.css" rel="stylesheet" />
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.0/angular.min.js"></script>
    <script src="./js/manage_plan.js"> </script>
    <script src="./js/common.js"> </script>

</head>

<body ng-controller="myCtrl" ng-app="myApp">
    <toaster-container></toaster-container>
    <div class="container-fluid px-0">
        <!-- top-header -->

        <nav id="home" class="navbar nav-txt-style hide-md navbar-expand-lg navbar-light header-bg">
            <span class=" navbar-text "><i class="fa orange-txt fa-map-marker mr-2"></i>UAE </span>
            <span class="navbar-text ml-3"><i class="fa orange-txt fa-phone mr-2 "></i> (+971) 526920873</span>
            <div class="ml-auto" id="navbarText">
                <a ng-if="User==undefined" href="log_reg.php"
                    class="  btn login-button-2 stand-btn mr-3 text-uppercase">
                    <span class="fa fa-sign-in  mr-2"></span>Login / register</a>
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="profile.php" id="navbarDropdown" role="button"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span ng-if="User!=undefined" class="navbar-text text-capitalize mr-2">
                                <span>
                                    <i class="fas fa-user mr-1"></i>
                                </span> Hello, {{User.name}}
                            </span>
                        </a>
                        <div class="dropdown-menu w-100 text-center" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="profile.php">My Profile</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" ng-click="logout()"><span ng-if="User!=undefined">logout</span></a>


                        </div>
                    </li>
                </ul>
                <!-- <span class="navbar-text text-capitalize mr-2">
                    follow us :
                </span>
                <span class="fa-cont">
                    <span class="s"> <i class="fab fa-twitter mr-2"></i></span>
                    <i class="fab fa-facebook-f mr-2"></i>
                </span> -->
            </div>
        </nav>
        <!-- //top-header -->

        <!--navbar-->
        <nav class="navbar navbar-expand-lg navbar-light nav-bg">
            <a class="navbar-brand" href="index.php">
                <img src="images/logo_crop.jpg" alt="utrition logo" class="nav-logo"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02"
                aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
                <ul class="navbar-nav ml-auto  mt-2 mt-lg-0">
                    <li class="nav-item "><a class="nav-link" href="index.php#meal_plans">meal plans</a></li>
                    <li class="nav-item "><a class="nav-link" href="index.php#about">About Us</a></li>
                    <li class="nav-item active"><a class="nav-link" href="manage_plan.php">my meal plan</a></li>
                    <li class="nav-item "><a class="nav-link" href="gallery.php">gallery</a></li>
                    <li class="nav-item "><a class="nav-link" href="faq.php">faq's</a></li>
                    <li class="nav-item "><a class="nav-link" href="contact.php">contact us</a></li>
                    <li class="nav-item d-lg-none d-xl-none" ng-if="User==undefined"><a class="nav-link"
                            href="log_reg.html">logIn/Register</a>
                    </li>
                </ul>
            </div>
        </nav>
        <!--//navbar-->
        <!--banner-->
        <div class="bg-banner-manage">
            <div class="row  p-0">

                <!--<img src="images/inner-bg.jpg" alt="healthy-food" class="main-banner-2 img-responsive">-->
                <!-- <img src="images/banner-2-test6.jpg" alt="plan img" class=" main-banner-2 img-responsive">-->
                <div class="overlay-on-img-manage">
                    <p class="topText uppercase-text green-txt">Manage your<span class="orange-txt secfont"> Plan
                        </span></p>
                </div>
            </div>

        </div>

        <!--//banner-->
        <!--crumb-->
        <div class="row no-gutters">
            <div class=" px-0 col-sm-12">
                <div class="breadcrumb-agile bg-light py-2">
                    <ol class="breadcrumb bg-light m-0">
                        <li class="breadcrumb-item">
                            <a href="index.php">Home</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">Manage your Plan</li>
                    </ol>
                </div>
            </div>
        </div>
        <!--//crumb-->

        <!--main tabs-->
        <ul class="nav my-4 nav-tabs justify-content-center" id="myTabmenu" role="tablist">
            <!--<li class="nav-item" role="presentation">
    <a class="nav-link active" id="delivery-tab" data-toggle="tab" href="#delivery" role="tab" aria-controls="delivery" aria-selected="true">profile</a>
  </li>-->
            <li class="nav-item" role="presentation">
                <a class="nav-link active" id="menu-tab" data-toggle="tab" href="#menu" role="tab" aria-controls="menu"
                    aria-selected="true">menu</a>
            </li>
            <!--  -->
            <li class="nav-item" role="presentation" ng-if="arrMenuUser.length != 0 && FreezePlan ==null">
                <a class="nav-link" id="freeze-tab" data-toggle="tab" href="#freeze" role="tab" aria-controls="freeze"
                    aria-selected="false">freeze</a>
            </li>
            <li class="nav-item" role="presentation">
                <a class="nav-link" id="renew-tab" data-toggle="tab" href="#renew" role="tab" aria-controls="renew"
                    aria-selected="false">renew</a>
            </li>
            <!--  <li class="nav-item" role="presentation">
    <a class="nav-link" id="track-tab" data-toggle="tab" href="#track" role="tab" aria-controls="track" aria-selected="false">track my progress</a>
  </li>
-->
            <li class="nav-item" role="presentation">
                <a class="nav-link" id="submitplan-tab" data-toggle="tab" href="#submitplan" role="tab"
                    aria-controls="submitplan" aria-selected="false">submit plan</a>
            </li>
        </ul>

        <div class="tab-content" id="myTabmenuContent">
   <!--  <div class="tab-pane fade show active" id="delivery" role="tabpanel" aria-labelledby="delivery-tab">
        
     </div>-->
        <!--menu tab-->
        <div class="tab-pane fade show active" id="menu" role="tabpanel" aria-labelledby="menu-tab">
            <?php include 'monitor_mealplan.php';?>
        </div>
        <!--      freeze tab  -->
        <div ng-if="arrMenuUser.length != 0 && FreezePlan ==null" class="tab-pane fade text-capitalize" id="freeze"
            role="tabpanel" aria-labelledby="freeze-tab">
            <?php include 'manage_plan_freez_tab.php';?>
        </div>


        <!--renew tab-->

        <div class="tab-pane fade" id="renew" role="tabpanel" aria-labelledby="renew-tab">
            <?php include 'manage_plan_renew_tab.php';?>
        </div>

        <!--track tab
   
    <div class="tab-pane fade" id="track" role="tabpanel" aria-labelledby="track-tab">
   
    </div>

    -->
        <!--submit-->


        <div class="tab-pane fade" id="submitplan" role="tabpanel" aria-labelledby="submitplan-tab">
            <?php include 'submit_plan.php';?>
        </div>

        <!--//submit-->
    </div>
    <!--//main tabs -->

    <!--footer-->
    <?php include 'footer.php';?>
    <!--//footer-->

    </div>
</body>

</html>