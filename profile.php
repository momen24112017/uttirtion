<!DOCTYPE html>
<html lang="en">

<head>
  <title>u trition</title>
  <!-- Meta tag Keywords -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta charset="UTF-8" />
  <meta name="keywords" />

  <!--// Meta tag Keywords -->

  <!-- Custom-Files -->
  <link rel="stylesheet" href="css/bootstrap.css">
  <link rel="stylesheet" href="css/fontawesome-free-5.13.1-web/css/all.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" />
  <link rel="stylesheet" href="css/style.css" type="text/css" media="all" />
  <link rel="icon" type="image/png" sizes="96x96" href="images/favicon-32x32.png">
  <!-- Style-CSS -->
  <!-- //Custom-Files -->
  <!--include on scroll -->
  <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
  <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
  <!-- Web-Fonts -->
  <link href="//fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i&amp;subset=latin-ext"
    rel="stylesheet">
  <link
    href="//fonts.googleapis.com/css?family=Barlow+Semi+Condensed:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
    rel="stylesheet">
  <!-- //Web-Fonts -->
  <script src="js/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
    integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
    crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
    integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
    crossorigin="anonymous"></script>
  <!--include angular-->
  <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.0/angular.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.4.0/angular-animate.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/angularjs-toaster/3.0.0/toaster.min.js"></script>

  <link href="https://cdnjs.cloudflare.com/ajax/libs/angularjs-toaster/3.0.0/toaster.min.css" rel="stylesheet" />
  <script src="./js/manage_plan.js"> </script>
  <script src="./js/common.js"> </script>
</head>

<body ng-controller="myCtrl" ng-app="myApp">
  <div class="container-fluid px-0">
    <!--navbar-->
    <?php include 'navbar.php';?>
    <!--//navbar-->
    <!--profile body-->
    <div class="row justify-content-center">
      <div class="col-sm-6">
        <div class="card">
          <h3 class=" pl-3 my-4 green-txt">
            <span class="text-uppercase ">personal information
            </span>
          </h3>
          <ul class="list-group list-group-flush">
            <li class="list-group-item">
              <form>
                <div class="form-group row">
                  <label for="name-n-profile" class="col-sm-2 col-form-label">Name:</label>
                  <div class="col-sm-10">
                    <input type="text" ng-model="User.name" disabled class="form-control-plaintext" id="name-n-profile">
                  </div>
                </div>

                <div class="form-group row">
                  <label for="email-n-profile" class="col-sm-2 col-form-label">Email:</label>
                  <div class="col-sm-10">
                    <input type="email" ng-model="User.email" disabled class="form-control-plaintext"
                      id="email-n-profile">
                  </div>
                </div>

                <div class="form-group row">
                  <label for="phone-n-profile" class="col-sm-2 col-form-label">Phone:</label>
                  <div class="col-sm-10">
                    <input type="text" ng-model="User.phone" class="form-control" id="phone-n-profile">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="pass-n-profile" class="col-sm-2 col-form-label">Password:</label>
                  <div class="col-sm-10">
                    <input type="password" ng-model="User.password" value="" class="form-control" id="pass-n-profile">
                  </div>
                </div>
            </li>

          </ul>
        </div>
      </div>
    </div>

    <div class="row justify-content-center">
      <div class="col-sm-6">
        <div class="card">
          <h3 class=" pl-3 my-4 green-txt">
            <span class="text-uppercase ">delivery information
            </span>
          </h3>
          <ul class="list-group list-group-flush">
            <li class="list-group-item">
              <p>Address</p>
              <form>
                <div class="form-group row" ng-repeat="obj in UserAddress track by $index">
                  <!-- <label for="address-home-n-profile" class="col-sm-2 col-form-label green-txt">{{obj.addres_name}}</label> -->
                  <div class="col-sm-2 col-form-label green-txt">
                    <input type="text" class="form-control" id="address-home-n-profile" ng-model="obj.address_name">
                  </div>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" id="address-sec-n-profile" ng-model="obj.address">
                  </div>
                  <div class="col-sm-2">
                    <a class="promo-code orange-txt float-right" data-toggle="collapse" href="#collapseupdateaddress"
                      role="button" aria-expanded="false" aria-controls="collapseupdateaddress">
                      <i ng-click="addAddress($index)" class="fas fa-plus"></i>
                      <i ng-if="$index>0" ng-click="removeAddress($index)" class="fas fa-minus"></i>
                    </a>
                  </div>
                </div>
                <button type="button" ng-click="updateUserProfile()" class="btn w-100 promo-btn mb-2">
                  <div ng-if="spnupdate" class="spinner-border " role="status">
                    <span class="sr-only">Loading...</span>
                  </div>
                  <span ng-if="!spnupdate">update</span>
                </button>
                <!-- <div class="form-group row">
               <div class="collapse col-sm-12" id="collapseupdateaddress">
                 <div class="card card-body">
                   <div class="form-group  mb-2">
                     <label for="address_work_reviewpg"class="col-sm-2 col-form-label green-txt">work</label>
                     <div class="col-sm-12">
                       <input type="text" class="form-control"  id="address_work_reviewpg" placeholder="street 123 dubai ">
                     </div> 
                   </div>
                   <button type="submit" class="btn w-100 promo-btn mb-2">update</button>
                 </div>
               </div>
             </div>               -->
              </form>
            </li>
          </ul>
        </div>
      </div>
    </div>

    <div class="row justify-content-center">
      <div class="col-sm-6">
        <div class="card">
          <h3 class=" pl-3 my-4 green-txt">
            <span class="text-uppercase ">medical information
            </span>
          </h3>
          <ul class="list-group list-group-flush">
            <li class="list-group-item">
              <form>
                <div class="form-row">
                  <div class="form-group col-md-6 checkboxex-style">
                    <label>Do you have any medical condition:</label><br>
                    <label>medical condition:</label><br>
                    <p>{{medicalMessage}}</p>
                    <div class="ds-block" >
                      <i ng-if="arrUM[0]['a']['Anemia']=='yes'" class="far fa-check-circle"></i>
                      <!-- <i  ng-if="arrUM[0]['a']['Anemia']==null" class="far fa-times-circle"></i> -->
                      <label ng-if="arrUM[0]['a']['Anemia']!=null">Anemia
                        <br></label>
                    </div>
                    <div class="ds-block" >
                      <i ng-if="arrUM[0]['a']['Diabetes']=='yes'" class="far  fa-check-circle"></i>
                      <!-- <i ng-if="arrUM[0]['a']['Diabetes']==null" class="far fa-times-circle"></i> -->
                      <label ng-if="arrUM[0]['a']['Diabetes']!=null">Diabetes <br></label>
                    </div>
                    <div class="ds-block" >
                      <i ng-if="arrUM[0]['a']['BloodPressure']=='yes'" class="far fa-check-circle"></i>
                      <!-- <i ng-if="arrUM[0]['a']['BloodPressure']==null" class="far fa-times-circle"></i> -->
                      <label ng-if="arrUM[0]['a']['BloodPressure']!=null">Blood Pressure (hyper or Hypo) <br></label>
                    </div>
                    <div class="ds-block" >
                      <i ng-if="arrUM[0]['a']['Thyroid']" class="far fa-check-circle"></i>
                      <!-- <i ng-if="arrUM[0]['a']['Thyroid']==null" class="far fa-times-circle"></i> -->
                      <label ng-if="arrUM[0]['a']['Thyroid']!=null">Thyroid (hyper or hypo)
                        <br></label>
                    </div>
                    <div class="ds-block">
                      <i ng-if="arrUM[0]['a']['heart']" class="far fa-check-circle"></i>
                      <!-- <i ng-if="arrUM[0]['a']['heart']==null" class="far fa-times-circle"></i> -->
                      <label ng-if="arrUM[0]['a']['heart']!=null">Heart problems
                        <br></label>
                    </div>
                    <div class="ds-block" >

                      <i ng-if="arrUM[0]['a']['Gastrointestinal']" class="far fa-check-circle"></i>
                      <!-- <i ng-if="arrUM[0]['a']['Gastrointestinal']==null" class="far fa-times-circle"></i> -->
                      <label ng-if="arrUM[0]['a']['Gastrointestinal']!=null">Gastrointestinal Problems
                        <br></label>
                    </div>
                    <div class="ds-block" >
                      <!-- <i  class="far fa-check-circle"></i> <i class="far fa-times-circle"></i> -->
                      <label ng-if="arrUM[0]['a']['otherCondition']!=null">other,please specify

                      </label>
                      <input ng-if="arrUM[0]['a']['otherCondition']!=null" type="text"
                        value="{{arrUM[0]['a']['otherCondition']}}" class="form-control" disabled
                        name="otherConditionProfile">
                    </div>
                  </div>

                </div>


                <div class="form-row">
                  <div class="form-group col-md-6 checkboxex-style">
                    <label>Food causes Allergy:</label><br>
                    <p>{{allergyMessage}}</p>
                    <div class="ds-block" >
                      <i ng-if="arrUM[1]['a']['eggs']=='yes'" class="far fa-check-circle"></i>
                      <!-- <i ng-if="arrUM[1]['a']['eggs']==null" class="far fa-times-circle"></i> -->
                      <label ng-if="arrUM[1]['a']['eggs']!=null">eggs<br></label>
                    </div>
                    <div class="ds-block">
                      <i ng-if="arrUM[1]['a']['Dairy']=='yes'" class="far fa-check-circle"></i>
                      <!-- <i ng-if="arrUM[1]['a']['Dairy']==null" class="far fa-times-circle"></i> -->
                      <label ng-if="arrUM[1]['a']['Dairy']!=null">Dairy
                        <br></label>
                    </div>
                    <div class="ds-block" >
                      <i ng-if="arrUM[1]['a']['Gluten']=='yes'" class="far fa-check-circle"></i>
                      <!-- <i ng-if="arrUM[1]['a']['Gluten']==null" class="far fa-times-circle"></i> -->
                      <label ng-if="arrUM[1]['a']['Gluten']!=null">Gluten
                        <br></label>
                    </div>
                    <div class="ds-block" >
                      <i ng-if="arrUM[1]['a']['nuts']=='yes'" class="far fa-check-circle"></i>
                      <!-- <i ng-if="arrUM[1]['a']['nuts']==null" class="far fa-times-circle"></i> -->
                      <label ng-if="arrUM[1]['a']['nuts']!=null">nuts
                        <br></label>
                    </div>
                    <div class="ds-block" >
                      <i class="far fa-check-circle" ng-if="arrUM[1]['a']['fish']=='yes'"></i>
                      <!-- <i ng-if="arrUM[1]['a']['fish']==null" class="far fa-times-circle"></i> -->
                      <label ng-if="arrUM[1]['a']['fish']!=null">fish
                        <br></label>
                    </div>
                    <div class="ds-block" >
                      <i class="far fa-check-circle" ng-if="arrUM[1]['a']['Seashell']=='yes'"></i>
                      <!-- <i ng-if="arrUM[1]['a']['Seashell']==null" class="far fa-times-circle"></i> -->
                      <label ng-if="arrUM[1]['a']['Seashell']!=null">Seashell
                        <br></label>
                    </div>
                    <div class="ds-block" >
                      <!-- <input class="form-check-input" type="checkbox" id="allergy7" name="allergy7" ng-model="objQuestionnaire.foodAllergy.allergy7" ng-true-value="otherallergy" ng-false-value=""> -->
                      <!-- <i class="far fa-check-circle"></i> <i class="far fa-times-circle"></i> -->
                      <label ng-if="arrUM[1]['a']['otherAllergy']!=null" for="allergy7">other,please specify
                    </div>

                    </label>

                    <input type="text" ng-if="arrUM[1]['a']['otherAllergy']!=null"
                      value="{{arrUM[1]['a']['otherAllergy']}}" class="form-control" disabled
                      name="otherAllergyProfile">

                  </div>

                </div>




              </form>
            </li>

          </ul>
        </div>
      </div>
    </div>
    <!--//profile body-->

    <!--footer-->
    <?php include 'footer.php';?>
    <!--//footer-->
    <!-- move top icon -->
    <a href=" index.php#home" class="move-top text-center">
      <span class="fas fa-level-up-alt" aria-hidden="true"></span>
    </a>
    <a href="contact.php" class="nav-link mt-2 btn help-fly-btn px-1  ">
      <span class="fa fa-sign-in  mr-2"></span>need help?</a>
    <!-- //move top icon -->



  </div>

</body>

</html>