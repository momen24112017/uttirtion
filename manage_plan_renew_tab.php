<form>  

     <div class="row justify-content-center mx-0"> 
     <div class="spin-wrap green-txt" ng-show="showLoadingSpinForPlan">
                    <div class="spinner-border " role="status">
                    </div>
                    <span class="pl-3">
                        <h3>Loading...</h3>
                    </span>
                </div>
        <div class="col-sm-6">
          <label for="plan-select">choose your plan</label>
          <select id="plan-select" class="form-control" ng-model="planDetails">
          <option   value="">select plan</option>
          <option ng-repeat="obj in arrPlan"  value="{{obj}}">{{obj.name}}</option>
         
          </select>
        </div>
     </div>
     <div class="row justify-content-center mx-0"> 
        <div class="col-sm-6 d-flex justify-content-center">
          <button type="button" ng-click="renewPlan()" class="btn my-4 stand-btn">Renew</button>
        </div>
     </div>
    
</form>