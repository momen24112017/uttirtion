<!--save btn-->
<div class="row no-gutters justify-content-center">
   <div class="col-sm-9 text-right">  
     <!-- <button type="button" class="btn text-capitalize   btn-accordion-save" data-toggle="modal" data-target="#exampleModal">
      save
     </button> -->
   </div> 
</div>
<!--// save btn-->
<div class="row py-5 no-gutters justify-content-center " ng-show="showLoadingSpinForSbMenu">

                <!--spinner-->
                <div class="spin-wrap green-txt" >
                    <div class="spinner-border " role="status">
                    </div>
                    <span class="pl-3">
                        <h3>Loading...</h3>
                    </span>
                </div>
</div>
<div ng-if="FreezePlan == null" class="row mt-2 no-gutters justify-content-center">
    <div class="col-sm-9">

 
        <nav >
            <div class="nav nav-tabs" id="nav-tabweeks" role="tablist" >
              <a class="nav-item nav-link week-item " id="nav-weekone-tab" ng-repeat="(key,objWeek) in arrMenuUser track by $index" ng-class="{active: $index == 0}" data-toggle="tab" href="#{{key}}" role="tab" aria-controls="nav-weekone" aria-selected="true"> {{key}}</a>
              
            </div>
          </nav>
    </div>
</div>
<div class="row mt-5 no-gutters justify-content-center">
  <div class="col-sm-9">     
          <div class="tab-content" id="nav-tabweeksContent">
         
        <!--week 1--> 
      <!--modal freeze-->
      <!-- Button trigger modal -->
      <!---->
<button   ng-if="arrMenuUser.length != 0 && FreezePlan ==null" type="button" class="btn text-capitalize btn-primary" data-toggle="modal" data-target="#freezeModal">
 Freeze
</button>

<!-- Modal -->
<div class="modal fade" id="freezeModal" tabindex="-1" role="dialog" aria-labelledby="freezeModalTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title " id="ModalLongTitle">freeze your plan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <form method="post" name="form" ng-class="{true: 'error'}[form.$invalid]">
        <div class="row justify-content-center my-4 mx-0">
          <div class="col-sm-6">
             <div class="form-group">
              <label for="startfreezemodal">from</label>
              <input type="date" name="from" required=""
            ng-model="objFreezePlan.from" min="{{minDate}}" max="{{maxDate}}">
             </div>
         </div>
         <div class="col-sm-6">
           <div class="form-group">
            <label for="endfreezemodal">to</label>
            <input type="date" name="to" required=""
            ng-model="objFreezePlan.to" min="{{minDate}}" max="{{maxDate}}">
           </div>
         </div>
       </div>
       <div class="row justify-content-center mx-0">
        <button type="submit" class="btn btn-primary" ng-click="freezePlan()" ng-disabled="!form.$valid">

          <div ng-if="FreezeSpn == false">Freeze</div>

          <div ng-if="FreezeSpn == true" class="text-center">
              <div class="spinner-border" role="status">
                  <span class="sr-only">Loading...</span>
              </div>
          </div>

        </button>
       </div>      
</form>
      </div>
    
    </div>
  </div>
</div>
      <!--//modal freeze-->

        <div class="row  no-gutters justify-content-center" ng-if="arrMenuUser.length==0"><b>Hello there is no menu to view</b></div>
        <div class="row  no-gutters justify-content-center" ng-if="arrMenuUser.length!=0 && FreezePlan!=null"><b>Hello you freeze your plan</b></div>
        <div ng-if="FreezePlan == null" class="tab-pane fade show " ng-repeat="(key,arrWeek) in arrMenuUser track by $index"  ng-class="{active: $index == 0}" id="{{key}}"  role="tabpanel" aria-labelledby="nav-weekone-tab">
          <div ng-repeat="week in arrWeek track by $index">
            <?php include 'week_one_tab.php';?>                  
          </div>  
        </div>
    
      </div>
  </div>
</div>      